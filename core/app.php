<?
$js = [
    '<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>',
    '<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>',
    '<script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>',
    '<script src="/js/jquery.selectric.min.js"></script>',
    '<script src="https://cdnjs.cloudflare.com/ajax/libs/rangeslider.js/2.3.2/rangeslider.min.js"></script>',
    '<script type="text/javascript" defer src="/js/script.js?v='.rand(11111,99999).'"></script>',
];
/*if ( strpos($_SERVER["HTTP_HOST"],'.local') !== false )
{
    $js[0] = '<script type="text/javascript" src="//code.jquery.com/jquery-3.3.1.min.js"></script>';
}*/

function addHTML( $arr )
{
    foreach ( $arr as $item ) echo $item;
}

function root ()
{
    return $_SERVER['DOCUMENT_ROOT'] . '/';
}

function inc( $path )
{
    $path = root() . "$path";

    include $path;
}

function inc_block( $path )
{
    inc("blocks/$path");
}
function inc_content( $path )
{
    inc("blocks/content/$path");
}

function inc_page( $path )
{
    inc("blocks/_page/$path");
}