<?php

$banners = [];

$banners[] = [
    'text'      => [
        'title'         => "<b>Сертифицированное</b> производство",
        'description'   => "Блочно-модульные контейнеры, автоматы ввода резерва, электрощитовое оборудование.",
    ],
    'img'       => [
        'item'          => "img/banner/item/1.png",
        'background'    => "img/banner/background/1.png",
    ]
];

$banners[] = [
    'text'      => [
        'title'         => "<b>Более 20 000 успешно</b> реализованных проектов",
        'description'   => "В каждом из 85 субъектов РФ, а также в ближнем зарубежье.",
    ],
    'img'       => [
        'item'          => "img/banner/item/2.png",
        'background'    => "img/banner/background/2.png",
    ]
];

$banners[] = [
    'text'      => [
        'title'         => "<b>Инженерные решения</b> полного цикла",
        'description'   => "От предпроекта до пуско-наладки и постгарантийного обслуживания по всей России.",
    ],
    'img'       => [
        'item'          => "img/banner/item/3.png",
        'background'    => "img/banner/background/3.png",
    ]
];

$banners[] = [
    'text'      => [
        'title'         => "<b>12 лет на рынке</b> энергообеспечения",
        'description'   => "Более 100 специалистов в штате. Средний опыт сотрудников в отрасли более 5 лет",
    ],
    'img'       => [
        'item'          => "img/banner/item/4.png",
        'background'    => "img/banner/background/4.png",
    ]
];



