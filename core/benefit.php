<?php

$benefit = [];

$benefit[] = [
    'title' => "Централизованные поставки",
    'list' => [
        'Возможность приобретения оборудования, расходных материалов, сопутствующего оборудования у одного поставщика.',
        'Снижение логистических затрат.',
        'Сокращение сроков поставки оборудования и комплектации производства оборудованием от разных производителей.',
    ]
];

$benefit[] = [
    'title' => "Предоставление персонального менеджера",
    'list' => [
        'Упрощение коммуникации между нами и нашими партнерами.',
        'Ускорение процесса заказа необходимого оборудования.',
        'Контроль произведённых оплат и отгрузка товара без задержек.',
    ]
];

$benefit[] = [
    'title' => "Гибкий индивидуальный подход к каждому партнеру",
    'list' => [
        'Мы всегда готовы пойти навстречу партнерам во многих вопросах.',
        'Пересмотр системы скидок не только при увеличении объемов закупки, но и на отдельно взятые проекты.',
        'Предоставление дополнительных мотивационных программ.',
    ]
];

$benefit[] = [
    'title' => "Всесторонняя сервисная поддержка",
    'list' => [
        'По вопросам подбора, наличия и комплектации оборудования, сопутствующих товаров и расходных материалов.',
        'По вопросам, касающимся технических аспектов по подключению, настройки, обслуживания поставленного оборудования.',
        'Специалисты нашей инженерной службы всегда готовы выехать на объект независимо от сложности работ и места нахождения оборудования.',
        'При возникновении непредвиденных ситуаций, специалисты компании в кратчайшие сроки предоставят необходимую информацию и поставят необходимые запчасти и расходные материалы.',
    ]
];

$benefit[] = [
    'title' => "Бесплатная телефонная поддержка наших дилеров",
];