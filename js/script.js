
let _, w, $w, d, $d, sys;

w = window;
d = document;

sys = {
    init : function() {

        String.prototype.dotted = function () {

            return '.' + this;
        };

        String.prototype.mod = function (txt) {
            
            return this + '.' + txt;
        };

    },

    /**
     *
     * @param get_as_float
     * @returns {*}
     */
    micro_time: function (get_as_float)
    {
        let now = new Date().getTime() / 1000,
            s = parseInt( now );

        return (get_as_float) ? now : (Math.round((now - s) * 1000) / 1000) + ' ' + s;
    },

    is :
        {
            // вывод в консоль - Логов ( только для .local )
            log : false,

            debug : false,

            last : true,

            localhost: function ()
            {
                return (w.location.host.indexOf('.local') !== -1);
            },
        },

    localhost :
        {
            log : function (text)
            {
                if ( sys.is.localhost() && sys.is.log )
                {
                    console.log('localhost LOG:', sys.micro_time(), ' / ', text);
                }
            }
        },

    count : {

        /**
         *
         * @param current
         * @param max
         * @returns {number}
         */
        next : function ( current, max ) {

            current++;

            if ( current > max )
            {
                current = 1;
            }

            return current;
        },

        /**
         *
         * @param current
         * @param max
         * @returns {*}
         */
        prev : function ( current, max ) {

            current--;

            if ( current < 0 )
            {
                current = max;
            }

            return current;
        }
    }
};





sys.localhost.log( 'JS Init' );





if ( $ !== undefined )
{
    /* D O C U M E N T / R E A D Y */

    sys.init();

    (function()
    {
        console.log("jQuery Start");

        /* C O M M O N    V A R I A B L E S */
        $w = $(w);
        $d = $(d);

        let $body = $('BODY'),

            click = 'click',

            open = '__open',
            active = '__active';



        /* S Y S T E M : Start */
        _ = {

            height  : 0,
            width   : 0,

            request : null,

            open    : null,

            attr    : 'data-js',

            init    : function()
            {
                _.common.load( 'common', _.common );
                _.common.load( 'blocks', _.block );
                _.common.load( 'page', _.page );

                this.width = $d.width();

                this.common.mark();
            },

            is      : {

                setup : {
                    full    : 1366,
                    desktop : 1126,
                    tablet  : 768,
                },

                /**
                 *
                 * @returns {boolean}
                 */
                full    : function ()
                {
                    return ( _.width >= this.setup.full );
                },

                /**
                 *
                 * @returns {boolean}
                 */
                desktop : function ()
                {
                    return ( _.width >= this.setup.desktop );
                },

                /**
                 *
                 * @returns {boolean}
                 */
                tablet  : function ()
                {
                    return ( _.width <= this.setup.desktop && _.width >= this.setup.tablet );
                },

                /**
                 *
                 * @returns {boolean}
                 */
                phone   : function ()
                {
                    return ( _.width <= this.setup.tablet );
                },

                /**
                 *
                 * @returns {*}
                 */
                localhost: function ()
                {
                    return sys.is.localhost();
                },

            },

            history : {

                limit : 300,

                list : [],

                last : null,

                /**
                 *
                 * @param item
                 */
                add : function (item) {

                    this.last = item;

                    if ( sys.is.debug )
                    {
                        if ( this.list.length < _.history.limit )
                        {
                            this.list.push([ sys.micro_time(), item ]);
                        }

                        console.log( item );
                    }

                },

                get : {

                    /**
                     *
                     * @param offset
                     * @returns {*}
                     */
                    offset: function ( offset ) {

                        let index = _.history.history.length - offset;

                        return _.history.history[ index  ];
                    },

                    /**
                     *
                     * @param index
                     * @returns {*}
                     */
                    index: function (index) {

                        return _.history.history[ index ];
                    },

                    last : function () {

                        let length = _.history.history.length;

                        return _.history.history[ length ];
                    }
                }
            },

            /**
             *
             * @param params
             * @returns  $.ajax()
             */
            ajax    : function( params )
            {
                if ( _.request !== null && _.request.abort !== null ) _.request.abort();

                let options = {
                    success     : function( resp )
                    {
                        console.log( "AJAX success: ", params, resp );
                    },
                    error       : function( resp )
                    {
                        console.log( "AJAX error:", params, resp );
                    },
                    complete    : function( )
                    {
                        _.request = null;
                    }
                };

                if ( params.success === undefined ) params.success = options.success;
                if ( params.error === undefined ) params.error = options.error;
                if ( params.complete === undefined ) params.complete = options.complete;

                _.request = $.ajax( params );

                return _.request;
            },

            // show function Tree
            /**
             *
             * @param id
             */
            help    : function( id )
            {
                let helper = function( obj, item, level )
                {
                    console.log( level + ' ' + item + ':' );

                    $.each( obj, function ( key, obj )
                    {
                        if ( typeof obj === "object" && obj.length === undefined ) {

                            helper( obj, key, level + '--' );

                        } else {

                            if ( typeof obj === "function" )
                            {
                                console.log( level + '--- '+ key +'()');

                            } else {

                                console.log( level + '--- '+ key );
                            }

                        }
                    });
                };

                if ( id === undefined )
                {
                    helper( _.common, 'common', '-' );
                    helper( _.page, 'page', '-' );

                } else {

                    if ( _.common[ id ] !== undefined )
                    {
                        console.log(_.common[ id ]);

                        helper( _.common[ id ], id, '-' )
                    }
                }

            },

            common : {

                /**
                 *
                 * @param id
                 * @param arr
                 */
                load : function( id, arr )
                {
                    let dif, point = sys.micro_time(true);

                    if ( arr.init !== undefined )
                    {
                        arr.init();

                        dif = ( sys.micro_time(true) - point );

                        sys.localhost.log( id + ' finish: root.init() | длительность загрузки: ' + dif );

                        point = sys.micro_time(true);
                    }


                    if ( arr.bind !== undefined )
                    {
                        arr.bind();

                        dif = ( sys.micro_time(true) - point );

                        sys.localhost.log( id + ' finish: root.bind() | длительность загрузки: ' + dif );

                        point = sys.micro_time(true);
                    }


                    $.each( arr, function ( index, obj )
                    {

                        if ( obj.init !== undefined )
                        {
                            obj.init();

                            dif = ( sys.micro_time(true) - point );

                            sys.localhost.log( id + ' finish: ' + index + '.init() | длительность загрузки: ' + dif );
                        }


                        point = sys.micro_time(true);


                        if ( obj.bind !== undefined )
                        {
                            obj.bind();

                            dif = ( sys.micro_time(true) - point );

                            sys.localhost.log( id + ' finish: ' + index + '.bind() | длительность загрузки: ' + dif );
                        }


                        point = sys.micro_time(true);

                    });
                },

                bind : function ()
                {
                    let attr = "[" + _.attr + "]";

                    $(document).on( click, attr, function (e)
                    {
                        e.preventDefault();

                        let $this = $(this),
                            path = '_.' + $this.attr( _.attr ) + '( $this, e )';

                        _.history.add( { attr, click, $this, e } );

                        eval( path );

                        return 0;
                    });
                },

                mark : function ()
                {
                    $body[0].innerHTML += '<noindex><!-- Front-End Development:  Андрей ( and_y87 ) Кидин : kidin_a [dog] list.ru --></noindex>';
                }
            },

            block   : {},

            page    : {},

            library : {},

            on      : {

                /**
                 *
                 * @param event
                 * @param selector
                 * @param func
                 */
                action : function ( event, selector, func )
                {
                    $d.on( event, selector, function (e)
                    {
                        let $this = $(this);

                        _.history.add( { selector, event, $this, e } );

                        sys.localhost.log( [ 'on.click()', $this, e] );

                        e.preventDefault();

                        return  func( $this, e );
                    });
                },
                click : function ( selector, func )
                {
                    return this.action( click, selector, func );
                }
            }
        };



        let page = _.page,
            block = _.block;

        /* S Y S T E M : End */




        block.mobile = {

          toggle : function ()
          {
              $body.toggleClass('__menu');
          }

        };

        /* C O M M O N / B L O C K */

        block.overlay = {

            selector : {
                root    : '.block__overlay',
                mask    : '.b_overlay--mask',
                layout  : '.b_overlay--layout'
            },

            bind : function()
            {
                let self = block.overlay,

                    close   = function() {

                        block.sub_menu.close();
                        block.navigator.close();
                        block.sheet.close();

                        self.mask.hide();
                    };

                //.b_overlay--mask
                _.on.action( 'mouseenter', self.selector.mask, close );

                //.b_overlay--mask
                _.on.click( self.selector.mask, close );
            },

            mask : {

                mod : '__mask',

                show : function () {

                    let self = block.overlay;

                    $( self.selector.root ).addClass( self.mask.mod );
                },

                hide : function () {

                    let self = block.overlay;

                    $( self.selector.root ).removeClass( self.mask.mod );
                }
            }
        };



        block.banner = {

            // Время показа таймера (учитывая время на анимацию когда он выезжает)
            speed : 10000,

            selector : {
                root    : '#banner',
                item    : '.b_banner--item',
                frame   : '.b_banner--frame',
                active  : '.js_banner--active',
                wrap    : '.b_banner--wrapper',
                timeline: '.b_banner--timeline',
                counter : '.b_banner--counter B',
            },

            current : 0,

            max     : 0,

            timer   : null,

            hover : false,

            init : function ()
            {
                let self = this;

                self.max = $(self.selector.item).length;

                setTimeout(function ()
                {
                    self.timeLineRestart();

                    self.setActive(1 );
                }, 500)
            },

            bind : function()
            {
                let self  = block.banner;

                //.js_banner--active
                _.on.click( self.selector.active, function ( $this )
                {
                    let $item   = $( self.selector.item.mod( active ) ),
                        $line   = $item.find( self.selector.timeline );

                    $line.stop().css({
                        width   : '0%',
                        bottom  : 0
                    });

                    self.setActive( $this.index() + 1 );

                    self.hover = false;
                });

                //#banner
                _.on.action('mouseenter', self.selector.root, function ()
                {
                    self.hover = true;

                    clearInterval( self.timer );

                    $( block.banner.selector.timeline )
                        .stop()
                        .animate({
                            bottom : '-5px'
                        },1000);
                });

                //#banner
                _.on.action('mouseleave', self.selector.root, function ()
                {
                    if ( !self.hover ) return false;

                    self.timerInit();
                    $( block.banner.selector.timeline ).stop();

                    block.banner.timeLineRestart();

                    self.hover = false;
                });

                //#banner
                _.on.action('dblclick', self.selector.root, function ()
                {
                    self.modify();
                });

                this.timerInit();
            },

            timerInit : function()
            {
                let self = block.banner;

                if ( self.timer !== null )
                {
                    clearTimeout( self.timer );
                }

                self.timer = setTimeout(function ()
                {
                    self.setActive( self.next() );

                }, self.speed );
            },

            timeLineRestart : function() {

                let self        = block.banner,
                    selector    = self.selector,
                    item        = selector.item.mod( active),
                    $timeLine   = $(item).find( selector.timeline );

                $timeLine.css({
                    width   : 0,
                    bottom  : 0
                });

                $timeLine.animate({
                    width : '100%'
                }, self.speed, function () {

                    $timeLine.animate({
                        bottom : '-5px'
                    },1000, function () {
                        $timeLine.css('width', '0%');
                    });
                } );
            },

            modify : function()
            {
                $( block.banner.selector.wrap ).toggleClass('__small');
            },

            setActive : function( current )
            {
                this.current = current;

                let selector = this.selector;

                $( selector.counter ).text( current );

                let index = (current - 1);

                $( selector.item.mod(active) ).removeClass( active );

                $( selector.item ).eq( index ).addClass( active );

                $( selector.frame.mod(active) ).removeClass( active );

                $( selector.frame ).eq( index ).addClass( active );

                block.banner.timeLineRestart();

                block.banner.timerInit();
            },

            next : function ()
            {
                return sys.count.next( this.current, this.max );
            },

            prev : function ()
            {
                return sys.count.prev( this.current, this.max );
            }
        };



        block.nav = {

            selector : {
                item : '.b_nav--item',
            },

            bind : function ()
            {
                let self = block.nav,
                    selector = self.selector.item.mod('__dropdown');

                //.b_nav--item.__dropdown
                _.on.action('mouseenter', selector, function ($this)
                {
                    self.open( $this );
                });
            },

            open : function( $this )
            {
                if ( !$this.hasClass( open ) )
                {
                    _.open = $this;

                    $this.addClass( open );

                    block.overlay.mask.show();
                }
            },

            close : function()
            {
                block.navigator.close();
            },
        };

        block.navigator = {

            selector : {
                root    : '.block__navigator',
                wrapper : '.b_navigator--wrapper'
            },

            bind : function()
            {
                //.b_navigator--wrapper
                _.on.action('mouseleave', block.navigator.selector.wrapper, function ()
                {
                    block.navigator.close();
                });
            },

            close : function () {

                let selector = block.nav.selector.item.mod('__dropdown');

                $( selector ).removeClass( open );

                block.overlay.mask.hide();

                $body.removeClass('__menu');

                _.open = null;
            }
        };

        block.search = {

            selector: {
                root    : '#search',
                input   : '.b_search--input'
            },

            response : [],

            ajax : null,

            init : function() {

                block.search.getClearButton().hide();
            },

            bind : function () {

                let self = block.search;

                _.on.action('focus click', self.selector.input, function ()
                {
                    if ( self.response.length )
                    {
                        block.sheet.fill( $(self.selector.root), self.response );
                    }
                });

                _.on.action('keyup', self.selector.input, function ($this)
                {
                    let text = $this.val();

                    if ( text.length )
                    {
                        block.search.getClearButton().show();
                    }

                    setTimeout(function ()
                    {
                        if ( self.ajax !== null )
                        {
                            self.ajax.abort();
                        }

                        self.response = [];

                        self.ajax = self.request( text );

                    }, 500 );
                });
            },

            request : function ( text ) {

                let self = block.search,
                    ajax = {
                    url : '/ajax/quick_search.php',
                    data : {
                        q : $.trim(text)
                    },
                    success : function ( resp ) {

                        console.log('quick_search success', resp);

                        self.response = resp.list;

                        let $container = $(self.selector.root);

                        block.sheet.fill( $container, resp.list );
                    },

                    error : function (resp) {

                        console.log('quick_search error', resp);
                    },

                    complete : function (resp) {

                        self.ajax = null;
                    }
                };

                return _.ajax( ajax );
            },

            clear : function() {

                $( block.search.selector.input).val('');

                block.search.getClearButton().hide();

                block.sheet.close();
            },

            getClearButton : function() {

                return $(block.search.selector.root).find('.__clear');
            },

        };

        block.sheet = {

            selector : {
                root : '.block__sheet',
                body : '.b_sheet--body'
            },

            init : function()
            {
                this.close();
            },

            fill : function ( $wrapper, list )
            {
                let self = this,
                    $container = $wrapper.find( self.selector.body );

                $container.empty();

                $.each( list, function (index, item)
                {
                    let template = $('#template__sheet').html();

                    template = template
                        .replace('#href#', item['href'] )
                        .replace('#img#', item['img'] )
                        .replace('#type#', item['tpe'] )
                        .replace('#model#', item['model'] );

                    $container.append( $(template) )
                });

                $wrapper.find( self.selector.root ).show();

                block.overlay.mask.show();
            },

            clear : function() {

                let selector = block.sheet.selector;

                $(block.search.selector.root).find( selector.root ).find( selector.body ).empty();
            },

            close : function ()
            {
                let sheet = block.sheet,
                    search = block.search,
                    overlay = block.overlay;

                $(search.selector.root).find( sheet.selector.root ).hide();

                $body.focus();

                if ( $(overlay.selector.mask).is(":visible") )
                {
                    overlay.mask.hide();
                }
            }
        };

        block.specification = {

            selector : {
                label : '.b_specification--label'
            }
        };


        block.filter = {

            selector : {
                button : '.b_filter--button'
            },

            text : {
                extend : "Расширенный поиск",
                simple : "Простой поиск"
            },

            init : function ()
            {
                let specification = block.specification,
                    label_extend = specification.selector.label.mod('__extend');

                $( label_extend ).hide();
            },

            bind : function ()
            {
                let selector = this.selector;

                //.b_filter--button.__tag
                _.on.click( selector.button.mod('__tag'), function ( $this )
                {
                    var index = $this.attr('data-manufacture-index');
                    var element = $('.b_manufactures__item')[index];
                    
                    $(element).find('input').prop('checked', false)
                    $this.remove();
                });

                _.on.click('.js_filter-extend-toggle', function ( $this )
                {
                    let $item = $('.b_specification--label.__extend'),
                        visible = $item.first().is(':visible'),
                        text    = block.filter.text[ ( ( visible ) ? 'extend' : 'simple' ) ];

                    if($item.hasClass('hidden')){
                        $item.toggle();
                        $item.toggleClass('hidden');
                    }
                    else{
                        $item.toggleClass('hidden');
                        setTimeout(function(){
                            $item.toggle();
                        }, 500);
                    }
                    

                    $this.text( text );
                });
            }
        };


        block.content = {
            selector : {
                tab     : '.b_content--tab'
            }
        };

        block.tabs = {

            selector : {
                root    : '.block__tabs',
                layout  : '.b_tabs--layout',
                item    : '.b_tabs--content',
                link    : '.b_tabs--link',
                content : '.b_tabs--item',
            },

            bind : function()
            {
                //.__tab .b_filter--button
                _.on.click('.__tab '+  block.content.selector.tab, function ( $this, e )
                {
                    let target  = $this.attr('href').replace('#','.'),
                        $block  = $this.parents('.__tab');

                    $block.removeClass( active );
                    $block.parent().find(target).addClass( active );
                });

                _.on.click('.js_tabs--active', function ( $this )
                {
                    block.tabs.toggle( $this );
                });
            },

            toggle : function ( $this )
            {
                let selector= this.selector,
                    $block  = $this.parents( selector.root ),
                    $layout = $block.find( selector.layout ),
                    id      = $this.find('A').attr('href');

                $block.find( selector.content + ',' + selector.item ).removeClass( active );

                $this.addClass( active );
                $layout.find(id).addClass( active );
            }
        };



        block.choose = {

            selector : {
                wrapper : '.b_choose--wrapper'
            },

            toggle : function ( )
            {
                $( this.selector.wrapper ).toggleClass( active );
            }
        };



        block.showcase = {

            selector : {
                root        : '.block__showcase',
                wrapper     : '.b_showcase--wrapper',
                items       : '.b_showcase--items',
                data_hover  : '[data-showcase-hover]'
            },

            bind : function()
            {
                let selector = this.selector;

                //[data-showcase-hover]
                _.on.action('mouseenter', selector.data_hover, block.showcase.column.mouseenter);

                //[data-showcase-hover]
                _.on.action('mouseout', selector.data_hover, block.showcase.column.mouseout);
            },

            column : {

                items   : 'b_showcase--items',

                mouseenter : function ($this)
                {
                    let cls = '__glow-' + $this.attr('data-showcase-hover');

                    cls     = block.showcase.column.items + ' ' + cls;

                    block.showcase.column.setup( cls );
                },

                mouseout : function ()
                {
                    let cls = block.showcase.column.items;

                    block.showcase.column.setup( cls );
                },

                setup : function ( cls )
                {
                    $(block.showcase.selector.items).attr('class', cls )
                }
            },

            resize : function ( )
            {
                let $obj = $(block.showcase.selector.root).find( block.slider.selector.root );

                if ( $(document).width() < 1110 )
                {
                    $obj.removeClass('__table').addClass( active );

                } else {

                    $obj.removeClass( active ).addClass('__table');
                }
            },

            // data-js="common.showcase.toggle"
            toggle : function ( $this )
            {
                let $target  = $this.parents( block.showcase.selector.wrapper );

                $target.toggleClass( active );
            }
        };



        block.youtube = {

            init : function() {

                setTimeout( block.youtube.resize, 500);
            },

            resize : function ()
            {
                let $item = $('.video_resize');

                if ( $item.length )
                {
                    $item.each( function ( a, $item )
                    {
                        $item   = $($item);

                        let size    = '',
                            regExp  = new RegExp('([0-9]{2,5})+([x])+([0-9]{2,5})'),
                            cls     = $item.attr('class').split(' ');

                        $.each( cls, function ( b, item )
                        {
                            if ( regExp.test(item) )
                            {
                                size = item;
                            }
                        });

                        $item.css({
                            width : 0,
                            height : 0
                        });

                        let data    = size.split('x'),

                            width   = data[0],
                            height  = data[1],

                            scale   = ( $item.parent().width() / width ),

                            frame   = {
                                width   : width * scale,
                                height  : height * scale
                            };

                        $item.css( frame )
                    });
                }
            }
        };



        block.superiority = {

            selector : {
                root    : '.block__superiority'
            },

            bind : function ()
            {
                //.block__superiority .js_superiority--toggle
                _.on.click(block.superiority.selector.root + ' .js_superiority--toggle', function ( $this )
                {
                    block.superiority.toggle( $this );
                });
            },

            toggle : function ( $this )
            {
                $this.parent().toggleClass( active );
            }
        };



        block.selection = {

            // data-js="common.selection.toggle"
            toggle : function ( $this )
            {
                $this.parent().toggleClass( active );
            }
        };



        block.question = {};



        block.spoiler = {

            bind : function ()
            {
                _.on.click('.js_spoiler--toggle', function ( $this )
                {
                    block.spoiler.toggle( $this );
                });
            },

            toggle : function ( $this )
            {
                if ( _.is.desktop() ) return false;

                let $target = $this.parents('.b_spoiler--item');

                if ( $target.length )
                {
                    $target.toggleClass( active );
                    console.log('$target не обнаружен. (смотри JS файл строка ниже)')

                } else {

                    /* необходимая струкрура:
                          .block__spoiler
                              .b_spoiler--wrapper
                                  .b_spoiler--header js_spoiler--toggle
                                      .text-20_25
                                      .b_spoiler--ui
                                  .b_spoiler--content
                      */
                }
            }
        };





        /* A - S I D E */


        block.menu = {

            selector : {
                item : '.b_menu--item',
            },

            bind : function ()
            {
                _.on.click('.js_menu--toggle', function ( $this )
                {
                    block.menu.toggle( $this );
                });

                //.b_menu--item
                _.on.action('mouseenter', block.menu.selector.item, function($this) {

                    if ( $body.hasClass('__menu') ) return;

                    block.sub_menu.open( $this );
                });

                //.b_menu--item
                _.on.action('mouseleave', block.menu.selector.item, function () {

                    if ( _.is.desktop() )
                    {
                        block.sub_menu.close('menu');
                    }
                });
            },

            toggle : function ( $this )
            {
                $this.parent().toggleClass( active );
            }

        };



        block.sub_menu = {

            selector : {
                root    : '.block__submenu',
                list    : '.b_submenu--list'
            },

            open : function( $this ) {

                let $submenu = $this.find( this.selector.root),
                    $list = $submenu.find( this.selector.list );

                if ( $submenu.length )
                {
                    block.sub_menu.close();

                    _.open = $this;

                    $this.addClass( open );

                    $('.b_overlay--layout.__left').addClass( open );

                    $list.css('top', -20);

                    let offset = 0,
                        A = $w.height(),
                        B = $this.height(),
                        C = $this.offset().top,
                        D = A - ( C + B ),
                        W = $list.height(),
                        E = $list.offset().top,
                        F = ( E + W ) - A,
                        G = C - E,
                        S = $w.scrollTop();

                    if ( ( S + A - 60 ) < ( E + W ) && G < 0 )
                    {
                        offset = ( -30 - W - G ) + D + S;

                        $list.css({
                            top : offset
                        });
                    }

                    block.overlay.mask.show();
                }
            },

            close : function () {

                let $obj = $( block.overlay.selector.root );

                if ( !$obj.hasClass(block.overlay.mask.mod) ) return false;

                $( block.menu.selector.item ).removeClass( open );

                $( block.overlay.selector.layout.mod('__left') ).removeClass( open );

                block.overlay.mask.hide();

                _.open = null;
            }
        };





        block.informer = {

            // data-js="common.informer.click"
            click : function ( $this, e )
            {
                console.log({
                    EVENT : e
                });
            }
        };



        block.slider  = {

            selector : {
                root    : '.block__slider'
            }

        };




        block.respond  = {

            selector : {
                spoiler : '.b_respond--spoiler'
            },

            toggle: function ( $this )
            {
                let $item = $this.parents( this.selector.spoiler );

                $item.toggleClass(active);

                $this.text( ( $item.hasClass(active) ) ? 'Свернуть отзыв' : 'Посмотреть весь отзыв' );

            }
        };



        block.catalog = {

            selector   : {
                root    : '.block__catalog',
                item    : '.b_catalog--item'
            },

            bind    : function(e)
            {
                let self = this;

                //.block__catalog .b_tabs--link
                _.on.click( self.selector.root + ' ' + block.tabs.selector.link, function ($this)
                {
                    let wrap    = self.selector.item,
                        $item   = $this.parents(wrap);

                    if ( !$item.hasClass(open) )
                    {
                        let $old = $( wrap + '.' + open );

                        if ( $old.length )
                        {
                            $old.find( '.' + active ).removeClass( active );
                            $old.removeClass( open );
                        }

                        $item.addClass( open );

                    }
                });

            }
        };

        block.job = {

            selector : {
                item : '.b_job--item'
            },

            toggle : function ( $this ) {

                let $parent = $this.parents( block.job.selector.item );

                $parent.toggleClass( open );

                return false;
            }

        };




        /* F O O T E R */

        block.footer = {

            bind : function ()
            {
                _.on.click('.js_footer--toggle', function ( $this )
                {
                    block.footer.domains.toggle( $this );
                });
            },

            domains :
                {
                    toggle : function ( $this )
                    {
                        $this.parents('.__company')
                            .find('.__domains')
                            .find('.__toggle')
                            .toggle();
                    }
                }
        };






        /* P A G E S */
        page.main = {};

        page.category = {};

        page.product = {};

        page.print = {

            print : function () {
                // если надо метрику клика на кнопку распечатать её вставлять сюда

                window.print();
            },

            send : function () {

                let mail = prompt('Введите e-mail');

                if ( mail )
                {
                    _.ajax({
                        url : '/send/print',
                        data : {
                            mail : mail
                        },
                        success : function (resp) {

                        }
                    });
                } else {
                    alert( 'Email пустой' );
                }
            }
        };






        $(window).ready(function ()
        {
            _.init();

            /* R E S I Z E : Start */
            let resize = function()
            {
                _.width = $(w).width();
                _.height = $(w).height();

                block.showcase.resize();
                block.youtube.resize();
            };

            $w.resize(resize);

            resize();
            /* R E S I Z E : End*/

            sys.localhost.log( 'JS Loaded' );
            sys.localhost.log( 'Отключить вывод логов sys.is.log = false' );

            // что бы IDE не подсвечивала как не используемое
            let ide_glow_down = false;

            if ( ide_glow_down )
            {
                if ( _.is.phone() ) _.help();
            }

        });
              
    })();

    $(document).ready(function(){
        // Use plugin "selectric"
        $('.select').selectric();

        // Rewards slider
        $('.js-rewards-slider').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 3
                    }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1
                  }
                }
              ]
        });
        // Slider trust
        $('.js-slider-trust').slick({
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            responsive: [
                {
                    breakpoint: 1200,
                    settings: {
                      slidesToShow: 4
                    }
                },
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 3
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 2
                  }
                },
                {
                    breakpoint: 350,
                    settings: {
                      slidesToShow: 1
                    }
                }
            ]
        });
        // Slider projects
        $('.js-slider-projects').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 1,
            arrows: true,
            dots: true,
            responsive: [
                {
                  breakpoint: 768,
                  settings: {
                    slidesToShow: 2
                  }
                },
                {
                  breakpoint: 480,
                  settings: {
                    slidesToShow: 1
                  }
                }
            ]
        });
        // Slider product sales
        function sliderProductSales(element){
            $(element).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 1,
                arrows: true,
                dots: true,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                        slidesToShow: 2
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {
                        slidesToShow: 1
                        }
                    }
                ]
            });
        }
        sliderProductSales('.js-slider-product-sales');
        sliderProductSales('.js-slider-product-sales1');
        
        
        // Switch headers
        $('.js-switch-header__get').on('click', function(e){
            e.preventDefault();
            var secondElementText = $(this).text();
            var firstElementText = $(this).siblings('.js-switch-header__put').text();

            $(this).text(firstElementText);
            $(this).siblings('.js-switch-header__put').text(secondElementText);

            function switchSlider (){
                if($('#slider_product_sales1').hasClass('__hidden')){
                    console.info('1');
                    $('#slider_product_sales2').addClass('__hidden');
                    $('#slider_product_sales1').removeClass('__hidden');
                    /* $(".js-slider-product-sales").slick('slickSetOption', 'adaptiveHeight', true, true); */
                    return
                }
                
                if($('#slider_product_sales2').hasClass('__hidden')){
                    console.info('2');
                    $('#slider_product_sales1').addClass('__hidden');
                    $('#slider_product_sales2').removeClass('__hidden');
                    sliderProductSales('.js-slider-product-sales2');
                    /* $(".js-slider-product-sales").slick('slickSetOption', 'adaptiveHeight', true, true); */
                    return
                }
            }
            switchSlider();
        });

        // Validation of numeric values
        $('.js-validation-numeric-values').on('input', function(e){
            var reg = /^\d+$/;
            var value = $(this).val();

            if(!value.match(reg)) $(this).val(value.substring(0, value.length - 1));            
        });


        // Switch big image in product gallery
        $('.js-gallery-link').on('click', function(e){
            e.preventDefault();
            var srcImage = $(this).find('img').attr('src');

            $('.js-gallery-preview').attr('href', srcImage);
            $('.js-gallery-preview').find('img').attr('src', srcImage)
        });
        // Slider gallery in product
        $('.js-slider-gallery').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                    slidesToShow: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                    slidesToShow: 1
                    }
                }
            ]
        });

        // Fancybox for gallery in product page
        $('.js-gallery-fancybox').fancybox();

        //MODALS
            // Modals__open
            $('.js-open-modal__call-order').on('click', function(e){
                openModal(e,'.modal-call-order');
            });
            $('.js-open-modal__ordering').on('click', function(e){
                openModal(e,'.modal-ordering');
                $(".js_delivery-list--slider-for").slick('slickSetOption', 'adaptiveHeight', true, true);
                $(".js_delivery-list--slider-nav").slick('slickSetOption', 'adaptiveHeight', true, true);
            });

            // Modals__close
            $('.js-modal-close').on('click', function(){
                $('.js-modal-switch').addClass('hidden');
                $('body').css('overflow', 'auto');
            });

            // Remove class hidden and scroll off
            function openModal(e, modal) {
                e.preventDefault();
                $(modal).removeClass('hidden');
                $('body').css('overflow', 'hidden');
            };

            // Ordering calculator
                // Input count product
                $('.js_field-count--value').on('change', function(e){
                    e.preventDefault();

                    var unit = $(this).attr('data-units');
                    var $count = $(this).val().replace(unit, '').trim();
                    var $productPrice = $('.js_product-price--value').text().replace(/ /g, '');

                    if($count === '0'){
                        $(this).val('1');
                        return
                    }

                    calcTotalPrice(+$productPrice, $count, unit);
                });

                // Click btn plus or minus
                $('.js_btn-count--minus, .js_btn-count--plus').on('click', function(e){
                    e.preventDefault();
                    var unit = $('.js_field-count--value').attr('data-units');
                    var $count = $('.js_field-count--value').val().replace(unit, '').trim();
                    var $productPrice = $('.js_product-price--value').text().replace(/ /g, '');

                    // If click on the btn minus
                    if($(this).hasClass('js_btn-count--minus')){

                        if($count > '1'){
                            calcTotalPrice(+$productPrice, --$count, unit);
                        }
                    };

                    // If click on the btn plus
                    if($(this).hasClass('js_btn-count--plus')){
                        calcTotalPrice(+$productPrice, ++$count, unit);
                    };
                });

                // 
                function calcTotalPrice(productPrice, count, unit) {
                    var totalPrice = productPrice * count;
                    var arr = totalPrice.toString().split('').reverse();
                    var newArr = [];
                    var newTotalPrice;
                    
                    $(arr).each(function(i, item){
                        (i%3 === 0) ? newArr.push(item+' ') : newArr.push(item);
                    });
                    
                    newTotalPrice = newArr.reverse().join('');

                    $('.js_field-count--value').val(count + ' ' + unit);
                    $('.js_product-count--value').text(count);
                    $('.js_product-price--total-value').text(newTotalPrice);
                };
            // Finish Ordering calculator

            // Delivery slider syncing in modal ordering
            $('.js_delivery-list--slider-for').slick({
                asNavFor: '.js_delivery-list--slider-nav',
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: false,
                draggable: false,
                touchMove: false,
                swipe: false
            });
            $('.js_delivery-list--slider-nav').slick({
                asNavFor: '.js_delivery-list--slider-for',
                slidesToShow: 3,
                slidesToScroll: 1,
                dots: false,
                centerMode: true,
                centerPadding: 0,
                focusOnSelect: true,
                draggable: false,
                touchMove: false,
                swipe: false
            });

        // Finish MODALS

        // MODALS VALIDATION
        formValidation();
        function formValidation(){
            console.log('Validation start');

            var $parentFormElement;

            // Add class for elements with class required-field
            $('.required-valid').addClass('not-valid')

            // Modals input validation
            $('.required-valid').on('input', function(){

                $parentFormElement = $(this.closest('form'));

                // Empty field check
                if($(this).val().length > 0) {
                    $(this).removeClass('not-valid');
                    modalValidationForm();
                }
                else {
                    $(this).addClass('not-valid');
                    modalValidationForm();
                };

                // Check input number
                if($(this).hasClass('phone-valid')){
                    var reg = /^\+\d+$/;
                    var value = $(this).val();

                    if(value[0] == '+'){
                        if(value.length > 1 && !value.match(reg)){
                            $(this).val(value.substring(0, value.length - 1));
                        }
                    }   
                    else {
                        $(this).val(value.substring(0, value.length - 1)); 
                    }
                    if(value.length > 10){
                        $(this).removeClass('not-valid');
                        modalValidationForm();
                    } 
                    else{
                        $(this).addClass('not-valid');
                        modalValidationForm();
                    }
                }
            });

            function modalValidationForm () {
                var $el = $($parentFormElement).find('.js-btn-valid');
                var $length = $parentFormElement.find('.not-valid').length;

                ($length > 0) ? $el.attr('disabled', true) : $el.attr('disabled', false);
            }
        };
        // Finish MODAL VALIDATION

        // Filter tabs
        $('.js-filter__header__tabs a').on('click', function(e){
            e.preventDefault();
            var $id = $(this).attr('href');
            var $elBodyWrapper = $('.js-filter__tabs__body');

            $('.js-filter__header__tabs').find('li').removeClass('__active');
            $(this).closest('li').addClass('__active');
            
            $elBodyWrapper.children().hide();
            $elBodyWrapper.find($id).show();
            console.log($elBodyWrapper.find($id));
        });

        // Filter select all manufactures
        $('.js-select-all-manufactures').on('click', function(e){
            e.preventDefault();
            if(!$('.js-all-manufactures').find('.b_filter--checkbox').prop('checked')){
                $('.js_main-manufactures').find('.b_filter--checkbox').prop('checked', true);
                $('.js-all-manufactures').find('.b_filter--checkbox').prop('checked', true);
                switchSelectedManufactures('add');
            }
            else{
                $('.js_main-manufactures').find('.b_filter--checkbox').prop('checked', false);
                $('.js-all-manufactures').find('.b_filter--checkbox').prop('checked', false);
                switchSelectedManufactures('remove');
            }
        });
        function switchSelectedManufactures(action){
            if(action === 'add'){
                var length = $('.js-all-manufactures').children().length;

                for(var i = 0; i < length; i++){
                    var item = $('.js-all-manufactures .b_manufactures__item')[i];
                    var text = $(item).text();
                    var template = `
                        <a class="b_filter--button __tag" href="#" data-manufacture-index='${i}'>
                            <i></i>${text}
                        </a>
                    `;
                    $('.js_filter__selected-manufactures').append(template);
                }
            }
            if(action === 'remove'){
                $('.js_filter__selected-manufactures').html('');
            }
        }
        // Finish filter tabs

        // Filter toggle manufactures list
        $('.js_filter--toggle').on('click', function(e){
            e.preventDefault();
            $('.js_list-all-manufactures--show').slideToggle();
        });

        // Click manufacture checkbox 
        $('.b_manufactures__item').on('change', function(){
            var $indexElement = $(this).index();

            if($(this).find('input').prop('checked')){
                var $textElement = $(this).text().trim();
                var $templateElement = `
                    <a class="b_filter--button __tag" href="#" data-manufacture-index='${$indexElement}'>
                        <i></i>${$textElement}
                    </a>`;
                
                $('.js_filter__selected-manufactures').append($templateElement);
            }
            else{
                $(`.b_filter--button[data-manufacture-index="${$indexElement}"]`).remove();
            }
        });
        
        // Range slider
        $('.js_init-rangeslider').on('click', function(){
            $('input[type="range"]').rangeslider({
                polyfill : false,
                onInit : function() {
                    this.output = $( '<div class="range-output" />' ).insertAfter( this.$range ).html( this.$element.val() );
                },
                onSlide : function( position, value ) {
                    this.output.html( value );
                }
            });
        });
        
        
    })
} else {
    console.log('jQuery Not Found!');
}


