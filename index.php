<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Главная</title>

        <? inc_block("head.php")?>
    </head>

    <body class="design-desktop2">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_content("document/comment.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_block("block__category.php"); ?>

                        <? inc_block("block__filter.php"); ?>

                        <? inc_block("block__choose.php"); ?>

                        <div class="block__content" id="content">


                            <? inc_content("previews/factory.php"); ?>


                            <? inc_content("previews/services.php"); ?>



                            <? inc_content("rewards.php"); ?>

                            <? inc_content("trust.php"); ?>

                            <? inc_content("projects.php"); ?>

                            <? inc_content("news.php"); ?>



                            <? inc_content("sales.php"); ?>

                            <? inc_content("leader.php"); ?>



                            <? inc_content("selection.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

             <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>