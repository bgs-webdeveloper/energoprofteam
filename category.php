<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Категория (Список товаров)</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog" >

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("catalog/block__breadcrumb.php"); ?>

                        <h1 class="b_content--header __title">
                            <span class="text-30_50 __header">
                                Газовые генераторы
                            </span>
                        </h1>

                        <? inc_block("block__filter.php"); ?>

                        <? inc_block("block__choose.php"); ?>

                        <div class="block__content" id="content">

                            <div class="b_content--box mod__visible-lg">

                                <div class="text-18_24 __bold __m1">
                                    Газовые электрогенераторы пользуются большим спросом на энергетическом рынке.
                                    <br>Это оборудование во многом превосходит бензиновые и дизельные аналоги:
                                </div>

                                <div class="block__list">
                                    <ul class="b_list--wrapper">
                                        <li class="b_list--item">Возможность длительного хранения топлива без снижения его свойств;</li>
                                        <li class="b_list--item">Возможность работы напрямую от городской газовой магистрали;</li>
                                        <li class="b_list--item">Низкое содержание вредных веществ в выхлопах;</li>
                                        <li class="b_list--item">Длительный срок службы двигателя;</li>
                                        <li class="b_list--item">Низкая цена топлива по сравнению с бензином и дизелем;</li>
                                        <li class="b_list--item">Стабильные рабочие параметры.</li>
                                    </ul>
                                </div>

                                <div class="b_content--line"></div>

                            </div>

                            <? inc_block("block__showcase.php"); ?>

                            <div class="b_content--line __small"></div>

                            <div class="b_content--box mod__hidden-lg">

                                <div class="text-18_24 __header">
                                    Газовые электрогенераторы пользуются большим спросом на энергетическом рынке.
                                    <br>Это оборудование во многом превосходит бензиновые и дизельные аналоги:
                                </div>

                                <div class="block__list">
                                    <ul class="b_list--wrapper __block">
                                        <li class="b_list--item">Возможность длительного хранения топлива без снижения его свойств;</li>
                                        <li class="b_list--item">Низкое содержание вредных веществ в выхлопах;</li>
                                        <li class="b_list--item">Низкая цена топлива по сравнению с бензином и дизелем;</li>
                                        <li class="b_list--item">Возможность работы напрямую от городской газовой магистрали;</li>
                                        <li class="b_list--item">Длительный срок службы двигателя;</li>
                                        <li class="b_list--item">Стабильные рабочие параметры.</li>
                                    </ul>
                                </div>

                                <div class="b_content--line"></div>

                            </div>

                            <div class="b_content--box">

                                <div class="b_content--header">

                                    <div class="text-24_28 __header">
                                        Справка
                                    </div>

                                </div>

                                <div class="b_content--text">
                                    <p>В дизель-генераторах источником механической энергии служит дизельный двигатель. Главное отличие от бензинового в способе воспламенения топлива для получения кинетической энергии и последующего крутящего момента. Этот момент определяет, как конструктивные особенности дизеля, так и достоинства и недостатки самого электрогенератора.</p>
                                    <p>Мощности дизельных генераторов широко варьируются - от 2 до 200 кВт и более. Конструкция и параметры агрегатов, главным образом число оборотов и тип охлаждения, определяют ресурс их работы. Он может меняться в диапазоне от 3000 до 30000 моточасов и более. Таким образом, главное преимущество дизельных генераторов - их способность к длительной работе при оптимальных нагрузках. Эксплуатация при малых нагрузках или на холостом ходу вредит им. При холостых оборотах двигателя может снижаться температура рабочего цикла. Это приводит впоследствии к частичному сгоранию топлива, образованию загрязнений в цилиндрах и форсунках, к нарушениям в смазочной системе.</p>
                                    <p>По числу оборотов двигателя дизельные генераторы разделяют на низкооборотные (1500 об/мин) и высокооборотные (3000 об/мин). Модели с низкими оборотами обладают большим моторесурсом, экономичностью, низким уровнем шума. Основной их недостаток – высокая стоимость. Высокооборотные модели уступают по своим эксплуатационным характеристикам, выигрывая при этом в цене.</p>
                                </div>

                                <div class="b_content--line __small"></div>

                            </div>

                            <? inc_page("catalog/block__projects.php"); ?>

                            <? inc_page("catalog/block__superiority.php"); ?>

                            <div class="b_content--line"></div>

                            <div class="mod__visible-lg">

                                <div class="b_content--grid">

                                    <div class="b_content--column __left">

                                        <div class="b_content--header">
                                            <span class="text-24_28 __header">Полезные статьи</span>
                                        </div>

                                        <? inc_page("catalog/block__articles.php"); ?>

                                    </div>

                                    <div class="b_content--column __right">

                                        <div class="b_content--header">
                                            <span class="text-24_28 __header">Часто задаваемые вопросы</span>
                                        </div>

                                        <? inc_page("catalog/block__faq.php"); ?>

                                    </div>

                                </div>

                                <div class="b_content--line"></div>

                            </div>

                            <? inc_page("catalog/block__rewards.php"); ?>

                            <? inc_page("catalog/block__selection.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>