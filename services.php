<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Список Услуг</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog" >

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("services/block__breadcrumb.php"); ?>

                        <div class="b_content--line __mini __blue mod__hidden-lg"></div>

                        <h1 class="b_content--header">
                            <span class="text-30_50 __header">
                                Услуги компании ЭнергоПроф
                            </span>
                        </h1>

                        <div class="block__content" id="content">

                            <div class="block__services">

                                <p class="b_services--text">
                                    ГК «ЭнергоПроф» дорожит своей репутацией и уделяет пристальное внимание качеству всех проводимых работ. Нам важно, чтобы оборудование, приобретенное Вами, работало безотказно. Всем нашим клиентам гарантированы профессиональный монтаж и последующее сервисное обслуживание. Наш сервис – это ряд услуг, которые доступны как в России, так и в странах СНГ; как частным лицам, так и крупным компаниям.
                                </p>

                                <p class="b_services--text">
                                    ГК «ЭнергоПроф» предлагает своим клиентам следующие услуги:
                                </p>

                                <div class="b_services--list">

                                    <? $names = [
                                        'Монтаж и пусконаладка',
                                        'Проектирование систем электроснабжения',
                                        'Автоматизация станций',
                                        'Монтаж и пусконаладка',
                                        'Шеф-монтаж',
                                        'Техобслуживание дизельных и бензогенераторов',
                                        'Выкуп БУ генераторов',
                                        'Синхронизация генераторов',
                                        'Ремонт бензиновых и дизельных электростанций',
                                        'Аренда генераторов',
                                        'Доставка',
                                    ]; ?>

                                    <? foreach ( $names as $index => $item ) : $x = ($index + 1)?>
                                        <a class="b_services--item" href="#">
                                            <? $fileName = 'item_' . ( ( $x >= 10 ) ? $x : '0'. $x ); ?>
                                            <div class="b_services--head">
                                                <img class="b_services--img"  src="/img/_page/services/<?= $fileName?>.jpg" alt="Картинка"/>
                                            </div>
                                            <div class="b_services--body">
                                                <span class="b_services--title"><?=$item?></span>
                                                <span class="b_services--description">Помимо поставок электрогенерирующего оборудования мы выполняем работы по проектированию резервных и основных системы электропитания.</span>
                                            </div>
                                        </a>
                                    <? endforeach; ?>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>