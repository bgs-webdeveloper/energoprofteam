<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">


    <head>
        <title>Список Новостей</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __cert">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("catalog/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <h1 class="b_content--header">
                                <span class="text-30_50 __header">
                                    Новости
                                </span>
                            </h1>

                            <? inc_page("chronicle/block__chronicle.php"); ?>

                            <? inc_page("chronicle/block__pagination.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">


                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>