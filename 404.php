<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>404</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <div class="block__content" id="content">

                            <div class="block__404">

                                <div class="b_404--wrapper">

                                    <div class="b_404--overlay __main">

                                        <div class="b_404--head">

                                            <h1 class="b_content--header">
                                                <span class="text-40_50 __header">
                                                    Страница <br class="mod__visible-sm">не найдена
                                                </span>
                                            </h1>

                                            <p class="b_404--text">
                                                Вероятно, эта страница осталась в прошлом или никогда <br class="mod__hidden-sm">не существовала.
                                            </p>
                                            <p class="b_404--text">
                                                Вы можете продолжить поиск в нашем
                                                <a class="link __underline text-13_20" href="#">каталоге</a>
                                                или начать
                                                <br class="mod__hidden-sm">с
                                                <a class="link __underline text-13_20" href="/">главной страницы</a>.
                                            </p>
                                        </div>

                                        <div class="b_404--body">
                                            <img class="b_404--img" src="/img/404.png" alt="изображение 404" />
                                        </div>

                                    </div>

                                    <? inc_page("404/form.php"); ?>

                                    <div class="b_content--line __mini"></div>

                                    <div class="b_404--overlay __filter">

                                        <div class="b_404--layout">

                                            <div class="text-22_30 __header">
                                                Для самостоятельного подбора оборудования воспользуйтесь поиском по товарам или фильтром по параметрам
                                            </div>

                                            <div class="b_404--layout">
                                                <? inc_block("block__search.php"); ?>
                                            </div>

                                        </div>

                                        <div class="b_404--layout">
                                            <? inc_block("block__filter.php"); ?>
                                        </div>

                                    </div>

                                    <div class="b_content--line __mini"></div>

                                    <div class="b_404--overlay __history">

                                        <div class="b_404--layout">

                                            <div class="text-30_50 __header">Просмотренные страницы</div>

                                            <a class="b_404--link" href="#">Бензиновый генератор Fubag TI 800</a>
                                            <a class="b_404--link" href="#">Бензиновый генератор Fubag BS 5500</a>
                                            <a class="b_404--link" href="#">Электростанции</a>
                                            <a class="b_404--link" href="#">Каталог</a>
                                            <a class="b_404--link" href="#">Главная</a>

                                        </div>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>