<?php
    include "core/app.php";
    include "core/job.php";
    include "core/why.php";
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Работа у нас</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("services/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <h1 class="b_content--header">
                                <span class="text-30_50 __header">
                                    Работа у нас
                                </span>
                            </h1>

                            <div class="block__job">
                                <div class="b_job--wrapper">
                                    <div class="b_job--head">

                                        <p class="b_job--text">Компания «ЭнергоПроф» входит в один холдинг с компанией Demis Group – маркетинговым digital-агентством № 1 в России. Мы разделяем общие корпоративные принципы и ценности, среди которых отдельно хочется отметить создание мотивирующей атмосферы в коллективе, позволяющей каждому сотруднику реализовать свой профессиональный потенциал. По итогам 2017 года Demis Group заняла 12-е место в рейтинге 100 лучших работодателей России за 2017 год от HeadHunter – и мы не только радуемся за коллег, но и перенимаем их успешный опыт, чтобы стать максимально привлекательным работодателем в своей отрасли.</p>
                                        <p class="b_job--text">«ЭнергоПроф» стремится к созданию корпоративной культуры, в условиях которой выполнение своих трудовых обязанностей вместо рутинного похода на работу 5 раз в неделю превращается в удовольствие от достижения намеченных результатов командой единомышленников!</p>
                                        <p class="b_job--text">Одна из целей компании – построение такого рабочего процесса, в котором у сотрудников развивается чувство сопричастности к общему делу. Каждый сотрудник «ЭнергоПроф» имеет возможность не только быть в курсе всего происходящего в компании, но и активно участвовать в её развитии – все новости о внутренних изменениях, об общих достижениях и успехах отдельных коллег, о планируемых мероприятиях, о деятельности подразделений публикуются в живой ленте на корпоративном портале. На нашем портале (и не только) каждый сотрудник может высказать свою точку зрения и внести свои предложения.</p>
                                        <p class="b_job--text">И, конечно, мы не забываем об отдыхе и развлечениях – для этого мы проводим различные корпоративные мероприятия, в том числе конкурсы, праздники, спортивные турниры.</p>

                                    </div>

                                    <div class="b_job--body">


                                        <? foreach ($job as $item ): ?>

                                            <div class="b_job--item <?=$item['mod']?>">

                                                <div class="b_job--title" data-js="block.job.toggle">
                                                    <?=$item['title']?>
                                                    <span class="b_job--toggle"></span>
                                                    <div class="b_job--price">
                                                        <?=$item['price']?>
                                                    </div>
                                                </div>

                                                <div class="b_job--overlay">

                                                    <? if (!empty($item['business'])): ?>

                                                        <div class="b_content--line __gray __m2"></div>

                                                        <div class="b_job--layout">
                                                            <div class="text-18_24 __header __m1">Обязанности:</div>

                                                            <div class="block__list">

                                                                <ul class="b_list--wrapper __block">
                                                                    <? foreach ($item['business'] as $list_item ): ?>
                                                                        <li class="b_list--item">
                                                                            <?= $list_item; ?>
                                                                        </li>
                                                                    <? endforeach; ?>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    <? endif; ?>

                                                    <? if (!empty($item['required'])): ?>

                                                        <div class="b_content--line __gray __m2"></div>

                                                        <div class="b_job--layout">
                                                            <div class="text-18_24 __header __m1">Требования</div>

                                                            <div class="block__list">

                                                                <ul class="b_list--wrapper __block">
                                                                    <? foreach ($item['required'] as $list_item): ?>
                                                                        <li class="b_list--item">
                                                                            <?= $list_item; ?>
                                                                        </li>
                                                                    <? endforeach; ?>
                                                                </ul>

                                                            </div>

                                                        </div>
                                                    <? endif; ?>

                                                </div>

                                                <? if ( !empty($item['link'])) :?>

                                                    <a class="button" href="<?=$item['link'];?>" target="_blank">
                                                        Откликнуться
                                                    </a>

                                                <? endif; ?>

                                            </div>
                                        <? endforeach; ?>

                                    </div>

                                    <div class="b_job--footer">

                                        <div class="block__youtube">

                                            <div class="b_youtube--wrapper">

                                                <iframe
                                                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                                        allowfullscreen=""
                                                        class="video_resize 825x465"
                                                        frameborder="0"
                                                        width="100%"
                                                        style="width: 825px; height: 465px"
                                                        src="https://www.youtube.com/embed/VNzRTGLd6SM"
                                                ></iframe>

                                            </div>

                                        </div>

                                        <div class="block__why">

                                            <div class="text-30_50 __header">
                                                Почему мы
                                            </div>

                                            <? foreach ( $why as $item ): ?>

                                                <div class="b_why--item">

                                                    <div class="b_why--preview">

                                                        <div class="center">
                                                            <img class="b_why--img" src="<?=$item['svg']?>" />
                                                        </div>

                                                    </div>

                                                    <div class="b_why--body">

                                                        <div class="b_why--header">
                                                            <?=$item['title']?>
                                                        </div>

                                                        <? if (!empty($item['title'])): ?>
                                                            <p class="text-13_20">
                                                                <?= $item['text']; ?>
                                                            </p>
                                                        <? endif; ?>

                                                        <? if (!empty($item['list'])): ?>

                                                            <div class="block__list">

                                                                <ul class="b_list--wrapper __block">
                                                                    <? foreach ($item['list'] as $list_item): ?>
                                                                        <li class="b_list--item">
                                                                            <?= $list_item; ?>
                                                                        </li>
                                                                    <? endforeach; ?>
                                                                </ul>

                                                            </div>

                                                        <? endif; ?>

                                                    </div>

                                                </div>
                                            <? endforeach; ?>

                                        </div>

                                        <!-- xxxxxx -->

                                        <? $list_trio_1 = []; ?>
                                        <? $list_trio_1[] = ['we_a_work.jpg', 'Мы работаем']; ?>
                                        <? $list_trio_1[] = ['we_a_relax.jpg', 'Мы отдыхаем']; ?>
                                        <? $list_trio_1[] = ['reviews.jpg', 'Отзывы сотрудников']; ?>

                                        <div class="block__trio">

                                            <? foreach ( $list_trio_1 as $item ) : ?>
                                                <a class="b_trio--item">
                                                    <div class="b_trio--preview">
                                                        <img class="b_trio--img" src="/img/job/<?= $item[0]?>" alt="компания"/>
                                                    </div>
                                                    <div class="b_trio--link __underline">
                                                        <?= $item[1]?>
                                                    </div>
                                                </a>
                                            <? endforeach;?>

                                        </div>

                                        <div class="b_content--line __gray"></div>

                                        <div class="text-30_50 __header __block __m3">
                                            Наши основные корпоративные принципы
                                        </div>

                                        <? $list_trio_1 = []; ?>
                                        <? $list_trio_1[] = ['justice.svg', 'Честность, справедливость, порядочность', "Мы действуем согласно нашим принципам, верны принятым обязательствам, выполняем данные обещания и соблюдаем договоренности."]; ?>
                                        <? $list_trio_1[] = ['respect.svg', 'Уважение к людям', "Мы верим в равенство возможностей, внимательны к потребностям других. Искренняя улыбка на лице другого человека для нас истинная ценность."]; ?>
                                        <? $list_trio_1[] = ['cooperation.svg', 'Взаимовыгодное сотрудничество', "Мы действуем по принципу «Выиграл-выиграл» - всегда ищем лучшее решение, которое удовлетворяет обе стороны при всех взаимодействиях: компания-сотрудник, коллега-коллега, компания-клиент/партнер."]; ?>

                                        <div class="block__trio">

                                            <? foreach ( $list_trio_1 as $item ) : ?>
                                                <div class="b_trio--item">

                                                    <div class="b_trio--preview __icon">

                                                        <div class="center">
                                                            <img class="b_trio--img" src="/img/job/<?= $item[0]?>" alt="иконка"/>
                                                        </div>

                                                    </div>

                                                    <div class="b_trio--link __m2">
                                                        <?= $item[1]?>
                                                    </div>

                                                    <p class="b_trio--text">
                                                        <?= $item[2]?>
                                                    </p>

                                                </div>
                                            <? endforeach;?>

                                        </div>

                                        <div class="b_content--line __gray"></div>

                                        <div class="block__quote">

                                            <div class="b_quote--wrapper">

                                                <div class="text-30_35 __header">
                                                    Наша настольная книга
                                                </div>

                                                <div class="b_quote--body">

                                                    <div class="b_quote--preview">
                                                        <img class="b_quote--img" src="/img/alan.jpg" alt="Стивен Р. Кови">
                                                    </div>

                                                    <div class="b_quote--text">
                                                        Слишком много праздников, которые длятся слишком долго, слишком много кино, телевизора, видеоигр, слишком много расслабляющего свободного времени, когда человек привыкает идти по пути наименьшего сопротивления.
                                                    </div>

                                                    <div class="b_quote--author">
                                                        Стивен Р. Кови, «7 навыков высокоэффективных людей»
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>