<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">


    <head>
        <title>Нулевой поиск</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <div class="block__content" id="content">
                            <div class="b_content--line __small"></div>

                            <h1 class="b_content--header">
                                <span class="text-40_50 __header">
                                    В каталоге не найдено моделей по Вашему запросу.
                                </span>
                            </h1>

                            <div class="text-18_24">Попробуйте измеить запрос</div>

                            <div class="b_content--box __m3">
                                <? inc_block("block__filter.php"); ?>
                            </div>

                            <div class="b_content--box">

                                <? inc_page("null/form.php"); ?>

                                <div class="b_content--line mod__visible-lg"></div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>