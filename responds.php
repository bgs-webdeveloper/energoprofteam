<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">


    <head>
        <title> Отзывы клиентов</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __cert">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("respond/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <h1 class="text-30_50 __header">
                                Отзывы клиентов
                            </h1>

                            <div class="block__responds">

                                <div class="b_respond--wrapper">

                                    <div class="b_respond--head">

                                        <a class="b_respond--filter" href="#">Нефтегаз</a>
                                        <a class="b_respond--filter" href="#">Горное дело</a>
                                        <a class="b_respond--filter" href="#">Строительство</a>
                                        <a class="b_respond--filter" href="#">Энергетика</a>
                                        <a class="b_respond--filter" href="#">Транспорт и связь</a>
                                        <a class="b_respond--filter" href="#">ЖКХ и социальная сфера</a>
                                        <a class="b_respond--filter" href="#">Сельское хозяйство</a>
                                        <a class="b_respond--filter" href="#">Сфера услуг и ритейл</a>
                                        <a class="b_respond--filter" href="#">Силовые и гомударственные структуры</a>
                                        <a class="b_respond--filter" href="#">Промышленность</a>

                                    </div>


                                    <div class="b_respond--body">
                                        <? for ($i=1; $i <= 5; $i++ ) : ?>

                                                <div class="b_respond--item">

                                                    <a class="b_respond--link" href="/img/respond/respond--doc--01.jpg" target="_blank">
                                                        <img class="b_respond--img" src="/img/respond/respond--doc--01.jpg" alt="Отзыв"/>
                                                    </a>

                                                    <div class="b_respond--title">
                                                        Благодарность от конструкторского бюро Сухого
                                                    </div>

                                                    <p class="b_respond--text">Руководство Филиала в лице главного инженера выражает благодарность коллективу компании ЭнергоПроф за взаимовыгодное сотрудничество, внимательность к деталям и высокий профессионализм.</p>

                                                    <div class="b_respond--spoiler">

                                                        <div class="b_respond--layout">

                                                            <p class="b_respond--text">За всё время взаимодействия с сотрудниками Вашей компании мы неукоснительно уверялись в надёжности и исключительной квалификации выбранного партнёра.</p>

                                                            <p class="b_respond--text">В результате безупречно реализованного проекта мы смогли приобрести оборудование для обеспечения энергоснабжения авиационной техники на аэродроме в Жуковском. Это позволило исключить любые риски, связанные с перебоями в электроснабжении.</p>

                                                            <p class="b_respond--text">Особую признательность выражаем менеджеру отдела продаж Алексееву Алексею за чёткую работу, соблюдение всех договорённостей и личный контроль за ходом проекта на всех его этапах.</p>

                                                        </div>

                                                        <button class="b_respond--button" data-js="block.respond.toggle">
                                                            Посмотреть весь отзыв
                                                        </button>

                                                    </div>

                                                </div>

                                        <? endfor; ?>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">


                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>