<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">


    <head>
        <title>Прогрес</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <h1 class="b_content--header">
                            <span class="text-30_50 __header">
                                Прогресс
                            </span>
                        </h1>

                        <div class="block__content" id="content">

                            <ul>
                                <li><br> <span class="text-14_20">11 : </span> <a class="link" href="/index.php" target="_blank">index</a> Главная</li>
                                <li><br> <span class="text-14_20">12 : </span> <a class="link" href="/category.php" target="_blank">category</a> Категосия / Список товаров</li>
                                <li><br> <span class="text-14_20">13 : </span> <a class="link" href="/product.php" target="_blank">product</a> Карточка Товара</li>
                                <li><br> <span class="text-14_20">16 : </span> <a class="link" href="/contacts.php" target="_blank">contacts</a> Контакты</li>
                                <li><br> <span class="text-14_20">16 : </span> <a class="link" href="/services.php" target="_blank">services</a> Список услуг</li>
                                <li><br> <span class="text-14_20">17 : </span> <a class="link" href="/service.php" target="_blank">service</a> Услуга</li>
                                <li><br> <span class="text-14_20">18 : </span> <a class="link" href="/cert.php" target="_blank">cert</a> Сертификаты и награды</li>
                                <li><br> <span class="text-14_20">18 : </span> <a class="link" href="/responds.php" target="_blank">responds</a> Отзывы</li>
                                <li><br> <span class="text-14_20">19 : </span> <a class="link" href="/catalog.php" target="_blank">catalog</a> Каталог</li>
                                <li><br> <span class="text-14_20">19 : </span> <a class="link" href="/chronicle.php" target="_blank">chronicle</a> Список новостей</li>
                                <li><br> <span class="text-14_20">22 : </span> <a class="link" href="/404.php" target="_blank">404</a> </li>
                                <li><br> <span class="text-14_20">23 : </span> <a class="link" href="/null.php" target="_blank">null</a> Нулевой поиск</li>
                                <li><br> <span class="text-14_20">23 : </span> <a class="link" href="/sales.php" target="_blank">sales</a> Акции и распродажи</li>
                                <li><br> <span class="text-14_20">23 : </span> <a class="link" href="/post.php" target="_blank">post</a> Новость</li>
                                <li><br> <span class="text-14_20">24 : </span> <a class="link" href="/portfolio.php" target="_blank">portfolio</a> Список проектов</li>
                                <li><br> <span class="text-14_20">24 : </span> <a class="link" href="/project.php" target="_blank">project</a> Информация о выполненом проекте</li>
                                <li><br> <span class="text-14_20">25 : </span> <a class="link" href="/dealer.php" target="_blank">dealer</a> Диллерам</li>
                                <li><br> <span class="text-14_20">25 : </span> <a class="link" href="/print.php" target="_blank">print</a> Страница "Пачать"</li>
                                <li><br> <span class="text-14_20">26 : </span> <a class="link" href="/job.php" target="_blank">job</a> Работа у нас</li>
                                <li><br> <span class="text-14_20">27 : </span> <a class="link" href="/article.php" target="_blank">article</a> Статья</li>
                                <li>...</li>
                            </ul>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>