<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">


    <head>
        <title>Страница продукта</title>

        <? inc_block("head.php")?>
    </head>

    <body class="__product">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_content("document/comment.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner--small.php"); ?>

                        <? inc_page("product/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <div class="block__goods">

                                <div class="b_goods--head __title">

                                    <h1 class="b_goods--title">
                                        Газовый генератор E3 POWER GG3300-X
                                    </h1>

                                    <a class="b_goods--link" href="#">
                                        <img class="b_goods--manufacturer" src="/img/manufacturers/europower.png" alt="логотип" />
                                    </a>

                                </div>

                                <div class="b_goods--body">

                                    <? inc_page("product/block__gallery.php"); ?>

                                    <div class="b_goods--overlay">

                                        <div class="b_goods--layout __left">

                                            <? inc_page("product/block__library.php"); ?>

                                        </div>

                                        <div class="b_goods--layout __right">
                                            <? inc_page("product/block__price.php"); ?>

                                            <div class="block__tabs">

                                                <div class="b_tabs--wrapper __product">

                                                    <ul class="b_tabs--header">

                                                        <li class="b_tabs--item js_tabs--active __active">
                                                            <a class="b_tabs--link" href="#table">Характеристики</a>
                                                        </li>

                                                        <li class="b_tabs--item js_tabs--active mod__hidden-sm __inline">
                                                            <a class="b_tabs--link" href="#variants">Варианты исполнения</a>
                                                        </li>

                                                        <li class="b_tabs--item js_tabs--active  mod__visible-lg __inline">
                                                            <a class="b_tabs--link" href="#addon">Доп. товары</a>
                                                        </li>

                                                        <li class="b_tabs--item js_tabs--active">
                                                            <a class="b_tabs--link" href="#description">Описание</a>
                                                        </li>

                                                    </ul>

                                                    <div class="b_tabs--layout">

                                                        <div class="b_tabs--content __active" id="table">
                                                            <? inc_page("product/block__table.php"); ?>
                                                        </div>

                                                        <div class="b_tabs--content" id="variants">
                                                            <? inc_page("product/tab--variants.php"); ?>
                                                        </div>

                                                        <div class="b_tabs--content" id="addon">
                                                            <? inc_page("product/tab--addon.php"); ?>
                                                        </div>

                                                        <div class="b_tabs--content" id="description">
                                                            <? inc_page("product/tab--description.php"); ?>
                                                        </div>

                                                    </div>

                                                </div>

                                            </div>

                                            <? inc_page("product/block__superiority--goods.php"); ?>

                                            <? inc_page("product/block__youtube.php"); ?>

                                            <? inc_page("product/block__rewards.php"); ?>

                                            <? inc_page("product/block__alert.php"); ?>

                                        </div>

                                    </div>

                                </div>


                                <div class="b_goods--footer">

                                    <div class="b_content--line"></div>


                                    <? inc_page("product/block__superiority--services.php"); ?>

                                    <? inc_page("product/block__showcase.php"); ?>

                                    <? inc_page("product/block__showcase--variants.php"); ?>

                                    <div class="block__spoiler mod__hidden-lg">

                                        <div class="b_spoiler--wrapper">

                                            <div class="b_spoiler--item mod__visible-sm">

                                                <div class="b_spoiler--header js_spoiler--toggle">
                                                    <div class="text-20_25 __header">
                                                        Документация
                                                    </div>
                                                    <span class="b_spoiler--ui"></span>
                                                </div>

                                                <div class="b_spoiler--content">

                                                    <? inc_page("product/block__library.php"); ?>

                                                </div>

                                            </div>

                                            <div class="b_spoiler--item">

                                                <div class="b_spoiler--header js_spoiler--toggle">
                                                    <div class="text-20_25 __header">
                                                        Дополнительные товары
                                                    </div>
                                                    <span class="b_spoiler--ui"></span>
                                                </div>

                                                <div class="b_spoiler--content">

                                                    <? inc_page("product/block__addon.php"); ?>

                                                </div>

                                            </div>

                                        </div>

                                    </div>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>