<?php include "core/app.php";

include "core/benefit.php";
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Дилерам</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("services/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <div class="block__benefit">

                                <div class="b_benefit--wrapper">

                                    <h1 class="b_content--header">
                                        <span class="text-30_50 __header">
                                            Дилерам
                                        </span>
                                    </h1>

                                    <p class="text-14_25">
                                        Современный рынок оборудования для снабжения предприятий сжатым воздухом активно развивается, в производстве используются новые технологии, появляется все больше возможностей комплектации и модификации оборудования, улучшается качество сервиса.
                                        <br>
                                        <br>
                                        Стать активным игроком в этой сфере сегодня - эффективное вложение средств в перспективный бизнес без лишних рисков.
                                    </p>


                                    <div class="b_benefit--title">
                                        Преимущества работы с нами
                                    </div>

                                    <div class="b_benefit--list">

                                        <? foreach ( $benefit as $item ) : ?>

                                            <div class="b_benefit--item">

                                                <div class="b_benefit--title">
                                                    <?= $item['title']; ?>
                                                </div>

                                                <? if ( isset($item['list']) ) : ?>

                                                    <div class="block__list">

                                                        <ul class="b_list--wrapper __block">

                                                            <? foreach ($item['list'] as $text ) : ?>
                                                                <li class="b_list--item">
                                                                    <?= $text; ?>
                                                                </li>

                                                            <? endforeach; ?>

                                                        </ul>

                                                    </div>

                                                <? endif; ?>

                                            </div>

                                        <? endforeach; ?>

                                    </div>

                                    <div class="block__table mod__hidden-sm">

                                        <div class="b_table--wrapper">

                                            <div class="b_table--body">

                                                <div class="b_table--overlay">

                                                    <div class="b_table--title __blue">
                                                        Условия сотрудничества
                                                    </div>

                                                    <div class="b_table--row __blue">
                                                        <div class="b_table--key">
                                                            Вид оборудования
                                                        </div>
                                                        <span class="b_table--value">
                                                                Скидка*
                                                            </span>
                                                    </div>

                                                    <div class="b_table--row">
                                                        <div class="b_table--key">
                                                            Поршневые компрессоры
                                                        </div>
                                                        <span class="b_table--value">
                                                            5% - 25%
                                                        </span>
                                                    </div>

                                                    <div class="b_table--row">
                                                        <div class="b_table--key">
                                                            Винтовые компрессоры
                                                        </div>
                                                        <span class="b_table--value">
                                                            7% - 26%
                                                        </span>
                                                    </div>

                                                    <div class="b_table--row">
                                                        <div class="b_table--key">
                                                            Передвижные компрессоры
                                                        </div>
                                                        <span class="b_table--value">
                                                            3% - 9%
                                                        </span>
                                                    </div>

                                                    <div class="b_table--row">
                                                        <div class="b_table--key">
                                                            Осушители воздуха
                                                        </div>
                                                        <span class="b_table--value">
                                                            7% - 27%
                                                        </span>
                                                    </div>

                                                </div>

                                            </div>

                                        </div>

                                        <div class="text-14_25 __pl2 __m1">
                                            * В зависимости от оборудования и объёма продаж.
                                        </div>
                                        
                                    </div>



                                    <div class="b_content--line"></div>

                                    <div class="b_benefit--notice">
                                        Компаниям, имеющим сервисную службу, способную нести гарантийные
                                        <br class="mod__visible-lg">обязательства и осуществлять постгарантийное обслуживание,
                                        <br><b>предоставляются расширенные условия сотрудничества.</b>
                                    </div>

                                    <div class="b_content--line"></div>

                                </div>

                                <div class="block__contacts">

                                    <div class="text-18_24 __header">
                                        Звоните по телефонам
                                    </div>

                                    <ul class="b_contacts--wrapper __dealer">

                                        <li class="b_contacts--item">

                                            <div class="b_contacts--box">
                                                <i class="b_contacts--icon __phone"></i>

                                                <a class="b_contacts--link" href="#">+7 (495) 789-47-76</a>
                                                <a class="b_contacts--link" href="#">
                                                    +7 (800) 555-06-29
                                                    <sub>(бесплатно из любого региона России)</sub>
                                                </a>
                                            </div>

                                        </li>

                                    </ul>

                                </div>

                            </div>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>