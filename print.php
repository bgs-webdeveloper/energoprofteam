<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Страница печати</title>

        <meta charset="UTF-8">

        <link href="/css/styles.css?v=<?=rand(111111,999999)?>" rel="stylesheet" media="all">

        <link rel="icon" type="image/png" href="/favicon/favicon32x32.png" sizes="32x32">
    </head>

    <body>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page __print" id="page">

                <div class="block__content" id="content">

                    <div class="block__print">

                        <div class="b_print--wrapper">

                            <div class="b_print--head">

                                <img class="b_print--logo" src="/img/logo.svg" alt="Logo" />

                                <div class="b_print--overlay">

                                   <div class="b_print--layout">
                                       <div class="b_print--text __geo-point">
                                           Москва, Мироновская, 25
                                       </div>
                                       <div class="b_print--text __clock">
                                           Пн-пт, c 9:00 до 19:00
                                       </div>
                                   </div>

                                   <div class="b_print--layout">
                                       <div class="b_print--text __phone">
                                           +7 (495) 789-47-76
                                       </div>
                                       <div class="b_print--text">
                                           +7 (800) 555-06-29
                                           <sub>(Бесплатно из регионов России)</sub>
                                       </div>
                                   </div>

                                    <div class="b_print--layout">

                                        <div class="b_print--text __share">
                                            sklad-generator.ru
                                        </div>
                                    </div>

                                    <div class="b_print--layout">

                                        <div class="b_print--text __mail">
                                            mail@sklad-generator.ru
                                        </div>
                                    </div>
                                </div>

                            </div>


                            <div class="b_content--line __print"></div>
                            <div class="text-30_50 __header">
                                Газовый генератор E3 POWER GG3300-X
                            </div>
                            <div class="b_content--line __print"></div>


                            <div class="b_print--body">

                                <div class="b_print--preview">

                                    <div class="sticker">Акция</div>

                                    <img class="b_print--product" src="/img/products/e3-power-GG3300-x.png" alt="Товар" />

                                </div>

                                <div class="b_print--tools">

                                    <button class="b_print--button" data-js="page.print.print" type="button">
                                        <i class="print"></i>
                                        На печать
                                    </button>
                                    <button class="b_print--button" data-js="page.print.send" type="button">
                                        <i class="mail"></i>
                                        Отправить на почту
                                    </button>
                                    <a class="b_print--button" href="#download_url">
                                        <i class="download"></i>
                                        Сохранить
                                    </a>

                                </div>

                            </div>


                        </div>

                        <? inc_page("print/block__price.php"); ?>

                        <? inc_page("print/block__table.php"); ?>

                        <? inc_page("product/block__superiority--goods.php"); ?>

                        <? inc_page("print/block__superiority--services.php"); ?>

                    </div>

                </div>

            </div>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
            <script>
                $(document).ready(function () {
                    window.print();
                });
            </script>
        </div>

    </body>

</html>