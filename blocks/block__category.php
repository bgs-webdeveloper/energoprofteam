<? include root() . "core/category.php"; ?>

<div class="block__category mod__hidden-lg">

    <div class="b_content--header __bold">Генераторы в Москве</div>

    <ul class="b_category--wrapper">

        <? foreach ($category as $img => $item):?>
            <li class="b_category--item <?=$item[0]?>">
                <a class="b_category--link" href="#" style="background-image: url('/img/category/<?=$img?>.png')">
                    <span class="b_category--title">
                        <?=$item[1]?>
                    </span>
                </a>
            </li>
        <? endforeach; ?>

    </ul>

</div>
