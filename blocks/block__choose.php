
<div class="block__choose">

    <div class="b_choose--wrapper">

        <div class="b_choose--title" data-js="block.choose.toggle">
            Быстрый подбор мощности
            <i class="b_choose--ui mod__hidden-lg __inline"></i>
        </div>

        <ul class="b_choose--list">

            <li class="b_choose--item">
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>3</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>4</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>5</b> квт </a>
                </div>
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>6</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>7</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>8</b> квт </a>
                </div>
            </li>

            <li class="b_choose--item">
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>9</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>10</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>15</b> квт </a>
                </div>
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>16</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>18</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>20</b> квт </a>
                </div>
            </li>

            <li class="b_choose--item">
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>25</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>30</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>40</b> квт </a>
                </div>
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>50</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>60</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>70</b> квт </a>
                </div>
            </li>

            <li class="b_choose--item">
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>75</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>80</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>100</b> квт </a>
                </div>
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>120</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>150</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>200</b> квт </a>
                </div>
            </li>

            <li class="b_choose--item">
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>250</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>300</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>400</b> квт </a>
                </div>
                <div class="b_choose--layer">
                    <a class="b_choose--link" href="/catalog.php"> <b>500</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>800</b> квт </a>
                    <a class="b_choose--link" href="/catalog.php"> <b>1000</b> квт </a>
                </div>
            </li>

        </ul>

    </div>

</div>