<div class="block__submenu">

    <i class="b_submenu--ui"></i>

    <div class="b_submenu--wrapper">
        <ul class="b_submenu--list">
            <li class="b_submenu--item">
                <a class="b_submenu--link __bold" href="/catalog.php">Все производители</a>
                <a class="b_submenu--link" href="/catalog.php">Aksa</a>
                <a class="b_submenu--link" href="/catalog.php">AMG</a>
                <a class="b_submenu--link" href="/catalog.php">Atlas Copco</a>
                <a class="b_submenu--link" href="/catalog.php">Briggs & Stratton</a>
                <a class="b_submenu--link" href="/catalog.php">Broadcrown</a>
                <a class="b_submenu--link" href="/catalog.php">Caterpillar</a>
                <a class="b_submenu--link" href="/catalog.php">CTG</a>
                <a class="b_submenu--link" href="/catalog.php">Cummins</a>
                <a class="b_submenu--link" href="/catalog.php">Denyo</a>
                <a class="b_submenu--link" href="/catalog.php">E3 POWER</a>
                <a class="b_submenu--link" href="/catalog.php">Eisemann</a>
                <a class="b_submenu--link" href="/catalog.php">Elcos</a>
                <a class="b_submenu--link" href="/catalog.php">Elemax</a>
                <a class="b_submenu--link" href="/catalog.php">Endress</a>
            </li>
            <li class="b_submenu--item">
                <a class="b_submenu--link" href="/catalog.php">Energo</a>
                <a class="b_submenu--link" href="/catalog.php">EuroPower</a>
                <a class="b_submenu--link" href="/catalog.php">FG Wilson</a>
                <a class="b_submenu--link" href="/catalog.php">Firman</a>
                <a class="b_submenu--link" href="/catalog.php">Fogo</a>
                <a class="b_submenu--link" href="/catalog.php">FPT</a>
                <a class="b_submenu--link" href="/catalog.php">Fubag</a>
                <a class="b_submenu--link" href="/catalog.php">Gazvolt</a>
                <a class="b_submenu--link" href="/catalog.php">Geko</a>
                <a class="b_submenu--link" href="/catalog.php">Generac</a>
                <a class="b_submenu--link" href="/catalog.php">Genese</a>
                <a class="b_submenu--link" href="/catalog.php">Gesan</a>
                <a class="b_submenu--link" href="/catalog.php">GMGen</a>
                <a class="b_submenu--link" href="/catalog.php">Henkelhausen</a>
                <a class="b_submenu--link" href="/catalog.php">Hertz</a>
            </li>
            <li class="b_submenu--item">
                <a class="b_submenu--link" href="/catalog.php">Honda</a>
                <a class="b_submenu--link" href="/catalog.php">Honeywell</a>
                <a class="b_submenu--link" href="/catalog.php">Hyundai</a>
                <a class="b_submenu--link" href="/catalog.php">JCB</a>
                <a class="b_submenu--link" href="/catalog.php">Kipor</a>
                <a class="b_submenu--link" href="/catalog.php">Kubota</a>
                <a class="b_submenu--link" href="/catalog.php">Lister Petter</a>
                <a class="b_submenu--link" href="/catalog.php">MingPowers</a>
                <a class="b_submenu--link" href="/catalog.php">Mirkon Energy</a>
                <a class="b_submenu--link" href="/catalog.php">Mitsubishi</a>
                <a class="b_submenu--link" href="/catalog.php">Mitsui Power</a>
                <a class="b_submenu--link" href="/catalog.php">Mosa</a>
                <a class="b_submenu--link" href="/catalog.php">MVAE</a>
                <a class="b_submenu--link" href="/catalog.php">Onis VISA</a>
                <a class="b_submenu--link" href="/catalog.php">PowerLink</a>
            </li>
            <li class="b_submenu--item">
                <a class="b_submenu--link" href="/catalog.php">Pramac</a>
                <a class="b_submenu--link" href="/catalog.php">REG</a>
                <a class="b_submenu--link" href="/catalog.php">RID</a>
                <a class="b_submenu--link" href="/catalog.php">SDMO</a>
                <a class="b_submenu--link" href="/catalog.php">Shindaiwa</a>
                <a class="b_submenu--link" href="/catalog.php">Toyo</a>
                <a class="b_submenu--link" href="/catalog.php">Yanmar</a>
                <a class="b_submenu--link" href="/catalog.php">Азимут</a>
                <a class="b_submenu--link" href="/catalog.php">АМПЕРОС</a>
                <a class="b_submenu--link" href="/catalog.php">Вепрь</a>
                <a class="b_submenu--link" href="/catalog.php">ДИЗЕЛЬ</a>
                <a class="b_submenu--link" href="/catalog.php">Исток</a>
                <a class="b_submenu--link" href="/catalog.php">ПСМ</a>
                <a class="b_submenu--link" href="/catalog.php">Старт</a>
                <a class="b_submenu--link" href="/catalog.php">ТСС</a>
            </li>
            <li class="b_submenu--item">
                <a class="b_submenu--link" href="/catalog.php">ФАС</a>
            </li>
        </ul>
    </div>
</div>