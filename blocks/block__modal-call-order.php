<div class="modal-bg modal-call-order hidden js-modal-switch">
    <div class="modal">
        <div class="modal-close --out-btn js-modal-close">
            <span class="modal-close__line"></span>
            <span class="modal-close__line"></span>
        </div>
        <div class="modal-inner">
            <h2 class="modal__title --center --blue-text">Заказ звонка</h2>
            <hr class="modal__line">
            <p class="modal__description">
                Оставьте свой номер телефона и наш специалист перезвонит Вам в течение 15 минут
            </p>
            <form class="modal__form">
                <input type="text" class="modal__form__field required-valid" placeholder="Ваше имя">
                <input type="text" class="modal__form__field required-valid phone-valid" placeholder="+7 (___)__-__-___">
                <textarea name=""  class="modal__form__field" placeholder="Укажите удобное время звонка или любую другую информацию."></textarea>
                <button class="button js-btn-valid" disabled>Жду звонка</button>
            </form>
        </div>
    </div>
</div>