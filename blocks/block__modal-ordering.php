<div class="modal-bg modal-ordering hidden js-modal-switch">
    <div class="modal">
        <div class="modal-close --inner-btn js-modal-close">
            <span class="modal-close__line"></span>
            <span class="modal-close__line"></span>
        </div>
        <div class="modal-inner">
            <h2 class="modal__title">Оформление заказа</h2>

            <div class="b_product-ordering">
                <div class="b_product-ordering--grid">
                    <div class="b_product-ordering_grid--left">
                        <div class="b_product-ordering--image-wrapper">
                            <img src="/img/products/e3-power-GG3300-x.png" alt="" class="product-image">
                        </div>
                        
                        <div class="b_product-ordering--information">
                            <h3 class="b_product-ordering--title">Газовый генератор E3 POWER GG3300-X</h3>
                            <ul class="b_product-ordering--list">
                                <li class="b_product-ordering--list-item">Топливо:    <span class="b_product-ordering--value">Газ</span></li>
                                <li class="b_product-ordering--list-item">Мощность:   <span class="b_product-ordering--value">2.4 кВт (2.4 кВА)</span></li>
                                <li class="b_product-ordering--list-item">Напряжение: <span class="b_product-ordering--value">230 В</span></li>
                                <li class="b_product-ordering--list-item">Двигатель:  <span class="b_product-ordering--value">Briggs&Stratton Vanguard</span></li>
                            </ul>
                        </div>
                    </div>
                    <div class="b_product-ordering_grid--right">
                        <div class="b_product-ordering--buttons-count">
                            <button class="btn-minus js_btn-count--minus">_</button>
                            <input  class="field-product-count js_field-count--value" data-units="шт." type="text" value="1 шт.">
                            <button class="btn-plus  js_btn-count--plus">+</button>
                        </div>

                        <div class="b_product-ordering--price">
                            <span class="present-price">
                                <b><span class="js_product-price--value">99 000 000 </span><sub>руб.</sub></b>
                            </span>
                            <span class="old-price">120 000 000 <sub>руб.</sub></span>
                        </div>
                    </div>

                </div>
                <hr class="m_ordering-hr">
                <div class="b_ordering-total-price">
                    <span class="b_superiority--text">Итого <span class="js_product-count--value">1</span> товара <b>без учета доставки</b> на сумму</span>
                    <span class="b_ordering-total-price--sum">
                        <b><span class="js_product-price--total-value">99 000 000</span> <sub>руб.</sub></b>
                    </span>
                </div>
            </div>
            
            <div class="modal-ordering__delivery">
                <div class="m_ordering-delivery--tabs-header">
                    <div class="js_delivery-list--slider-nav">
                        <label class="delivery__label">
                            <input type="radio" name="delivery-tabs-header" data-delivery-index="0">
                            Без доставки
                        </label>

                        <label class="delivery__label js_init-rangeslider">
                            <input type="radio" name="delivery-tabs-header" data-delivery-index="1">
                            Доставка по Москве и области
                        </label>

                        <label class="delivery__label">
                            <input type="radio" name="delivery-tabs-header" data-delivery-index="2">
                            Доставка в регионы России
                        </label>
                    </div>
                </div>

                <div class="m_ordering-delivery--tabs-content">
                    <div class="js_delivery-list--slider-for">
                        <div class="b_delivery-content--item">
                            
                        </div>
                        <div class="b_delivery-content--item">
                            <div class="b_delivery-content--item-inner">
                                <p class="b_superiority--text">
                                    Укажите километраж от МКАДа при доставке в область
                                </p>
                                <div class="b_delivery-wrapper-slider">
                                <input
                                    type="range"
                                    min="10"                    
                                    max="1000"                  
                                    step="10"                   
                                    value="300"                 
                                    data-orientation="horizontal"
                                    data-rangeslider
                                    class="rangeslider"
                                >
                                </div>
                                <p class="b_superiority--text">
                                    Расстояние:  <b>63 км </b>
                                </p>
                                <p class="b_superiority--text">
                                    Ориентировочная стоимость доставки: <b>1 000 руб.</b>
                                </p>
                                <p class="b_superiority--text">
                                    Точную стоимость узнавайте у вашего менеджера.
                                </p>
                            </div>
                        </div>
                        <div class="b_delivery-content--item">
                            <div class="b_delivery-content--item-inner">
                                <p class="b_superiority--text --child-select-block-inline">
                                    Выберите в списке регион доставки: 
                                        <select class="select" tabindex="-1">
                                            <option>Нижний новгород</option>
                                            <option>Питер</option>
                                            <option>Таганрог</option>
                                            <option>Другой город</option>
                                        </select>
                                </p>
                                <p class="b_superiority--text">
                                    Ориентировочная стоимость доставки: <b>2 500 руб.</b>
                                </p>
                                <p class="b_superiority--text">
                                    Точную стоимость узнавайте у вашего менеджера.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="modal-ordering__contact-form-wrapper">
                <p class="b_superiority--text">
                    Заполните форму заказа и мы свяжемся с вами в течение 15 минут в рабочее время.<br/> 
                    Если у вас возникли какие-либо вопросы, вы можете позвонить нам по телефону <b>8 (800) 555-06-29</b>
                </p>
                <form class="b_ordering-contact-form">
                    <div class="b_ordering-contact-form-inner--left">
                        <input type="text" class="modal__form__field required-valid" placeholder="Ваше имя*">
                        <input type="text" class="modal__form__field required-valid phone-valid" placeholder="+7 (___)__-__-___*">
                    </div>

                    <div class="b_ordering-contact-form-inner--right">
                        <textarea name=""  class="modal__form__field" placeholder="Комментарий"></textarea>
                    </div>
                    
                    <div class="b_ordering-contact-form-inner--bottom">
                        <p class="b_superiority--text">
                            Нажимая кнопку «Отправить заказ», вы соглашаетесь на обработку 
                            персональных данных и с условиями <a href="#" class="link">Пользовательского соглашения</a>.
                        </p>
                        <button class="button js-btn-valid" disabled>Отправить заказ</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>