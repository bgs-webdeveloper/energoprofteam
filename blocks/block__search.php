
<div class="block__search" id="search" role="search">

    <form class="b_search--wrapper" method="post" action="/quick-search/" >

        <label class="b_search--label">

            <input class="b_search--input" type="text" placeholder="Поиск по каталогу..." name="q">

            <button class="b_search--button __clear"
                    type="button"
                    style="display: none"
                    data-js="block.search.clear">
                <i class="b_search--ico"></i>
            </button>

            <button class="b_search--button __loop" type="submit">
                <i class="b_search--ico"></i>
            </button>

            <? inc_block('sheet.php' )?>

        </label>

    </form>

    <div class="b_search--layout __button">
        <button class="button js-open-modal__call-order">
            <span class="text-14_20 mod__visible-lg">Заказать звонок</span>
            <span class="text-14_20 mod__hidden-lg">+7 (495) 135-45-73</span>
        </button>
    </div>

</div>