<div class="block__showcase">

    <div class="b_showcase--wrapper">

        <div class="b_showcase--head ">

            <div class="b_showcase--overlay __display mod__visible-lg __inline">
                <? inc_block("block__display.php"); ?>
            </div>

            <div class="b_showcase--overlay __sorting mod__hidden-lg __inline">
                <? inc_block("block__sorting.php"); ?>
            </div>

            <div class="b_showcase--overlay __pagination">
                <? inc_block("block__pagination.php"); ?>
            </div>

        </div>

        <div class="b_showcase--body">

            <div class="b_showcase--tools">

                <div class="block__setups mod__visible-lg">

                    <div class="b_setups--wrapper">

                        <div class="b_setups--head">
                            <div class="b_setups--label">Сортировка</div>
                            <a class="b_setups--link __main" data-showcase-hover="head" href="#">Название модели</a>
                        </div>

                        <div class="b_setups--body">
                            <a class="b_setups--link" data-showcase-hover="param-1" href="#">Топливо</a>
                            <a class="b_setups--link __active" data-hover="param-2" href="#">
                                Мощность
                            </a>
                            <a class="b_setups--link" data-showcase-hover="param-3" href="#">Напряжение</a>
                            <a class="b_setups--link" data-showcase-hover="param-4" href="#">Наличие АВР</a>
                            <a class="b_setups--link" data-showcase-hover="param-5" href="#">Исполнение</a>
                        </div>
                        <div class="b_setups--footer">
                            <a class="b_setups--link" data-showcase-hover="footer" href="#">Цена</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_showcase--items">

                <div class="block__slider __table" id="slider_product_sales">

                    <div class="b_slider--wrapper">

                        <ul class="b_slider--list" cost="2" >

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product--sale.php"); ?>

                            </li>

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product--sale.php"); ?>

                            </li>


                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>


                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>


                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product.php"); ?>

                            </li>


                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product--sale.php"); ?>

                            </li>

                            <li class="b_slider--item">

                                <? inc_page("catalog/block__product--sale.php"); ?>

                            </li>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <div class="b_showcase--footer">


            <div class="b_showcase--overlay __display mod__visible-lg __inline">
                <? inc_block("block__display.php"); ?>
            </div>

            <div class="b_showcase--overlay __pagination">
                <? inc_block("block__pagination.php"); ?>
            </div>

        </div>

    </div>

</div>