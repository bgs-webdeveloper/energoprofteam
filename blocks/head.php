<meta charset="UTF-8">

<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
<link rel="stylesheet" href="/css/selectric.css">
<link rel="stylesheet" href="/css/rangeslider.css">
<link href="/css/styles.css?v=<?=rand(111111,999999)?>" rel="stylesheet" media="all">
<link href="/css/slylesBGS.css" rel="stylesheet" media="all">


<link rel="icon" type="image/png" href="/favicon/favicon32x32.png" sizes="32x32">

