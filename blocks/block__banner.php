<? include root() . "core/banners.php"; ?>

<div class="block__banner" id="banner">

    <div class="b_banner--wrapper">

        <? foreach ( $banners as $index => $item ) : ?>

            <div class="b_banner--item">

                <div class="b_banner--text">

                    <div class="b_banner--title">
                        <?=$item['text']['title'];?>
                    </div>

                    <p class="b_banner--description">
                        <?=$item['text']['description'];?>
                    </p>

                </div>

                <img class="b_banner--img" alt="баннер" src="//dev.sklad-generator.ru/<?=$item['img']['item'];?>" />

                <img class="b_banner--background" alt="Фоновое изображение" src="//dev.sklad-generator.ru/<?=$item['img']['background'];?>" />

                <span class="b_banner--timeline" style="width: 0%;"></span>
            </div>

        <? endforeach; ?>

    </div>

    <div class="b_banner--layout __info">

        <div class="b_banner--box __place">
            <span class="b_banner--counter">
                <b>1</b> / <?=count($banners)?>
            </span>
        </div>

        <ul class="b_banner--controller">
            <? for ( $i = count($banners); $i > 0; $i-- ): ?>
                <li class="b_banner--frame js_banner--active" data-num="<?=$i;?>"></li>
            <? endfor; ?>
        </ul>

    </div>

</div>
