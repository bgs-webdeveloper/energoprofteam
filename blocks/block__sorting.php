<div class="block__sorting">

    <div class="b_sorting--wrapper">

        <label class="b_sorting--label">

            <span class="b_sorting--layout">
                <span class="b_sorting--text">
                    Сортировать по:
                </span>
            </span>

            <select class="select __active">
                <option>Напряжению, по возростанию</option>
                <option>Напряжению, по убыванию</option>
            </select>

        </label>

    </div>

</div>