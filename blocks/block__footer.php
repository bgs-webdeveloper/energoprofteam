

<div class="block__footer" id="footer">

    <div class="b_footer--wrapper">

        <div class="b_footer--layout __left">

            <div class="b_footer--overlay __top">

                <div class="b_footer--box __phones mod__visible-lg">
                    <a class="b_footer--link" href="#">+7 (495) 789-47-76</a>
                    <a class="b_footer--link" href="#">+7 (800) 555-06-29</a>
                </div>

                <button class="button">
                    <span class="mod__visible-lg js-open-modal__call-order">Заказать звонок</span>
                    <span class="mod__hidden-lg">+7 (495) 135-45-73</span>
                </button>

            </div>

            <div class="b_footer--overlay __central">

                <div class="b_footer--overlay __bottom">

                    <div class="b_footer--box __cards">

                        <div class="b_footer--link">
                            <img class="b_footer--img" src="/img/footer/live-internet.png" alt="live internet">
                        </div>

                        <div class="b_footer--link">
                            <img class="b_footer--img" src="/img/footer/card--mastercard.png" alt="card mastercard">
                        </div>

                        <div class="b_footer--link">
                            <img class="b_footer--img" src="/img/footer/card--visa.png" alt="card visa">
                        </div>

                    </div>

                    <div class="b_footer--box __site-map">
                        <a class="b_footer--link" href="#">Карта сата</a>
                    </div>

                </div>

                <div class="b_footer--overlay __ofella">

                    <a class="b_footer--link" href="#">
                        Информация, размещенная на сайте, не является публичной офертой
                    </a>

                </div>

            </div>

        </div>

        <div class="b_footer--layout __right">

            <div class="b_footer--overlay __menu">

                <div class="mod__flex">
                    <a class="b_footer--link" href="#">О компании</a>
                    <a class="b_footer--link" href="#">Производство</a>
                    <a class="b_footer--link" href="#">Каталог</a>
                    <a class="b_footer--link" href="#">Услуги</a>
                    <a class="b_footer--link" href="#">Доставка</a>
                    <a class="b_footer--link" href="#">Проекты</a>
                    <a class="b_footer--link" href="#">Информация</a>
                    <a class="b_footer--link" href="#">Дилерам</a>
                    <a class="b_footer--link" href="#">Контакты</a>
                </div>

            </div>

            <div class="b_footer--overlay __company">

                <div class="b_footer--box __info">

                    <div class="b_footer--box __text">
                        <p class="b_footer--text mod__visible-lg">105318, Москва, Мироновская 25<Br>Часы работы: пн-пт с 9.00 до 19.00</p>
                        <p class="b_footer--text">© 2007-2019 <br class="mod__visible-sm">ООО «ГК ЭнергоПроф»</p>
                    </div>

                    <div class="b_footer--box __controller">

                        <a class="link __box __white js_footer--toggle">
                            Все города
                        </a>

                    </div>

                </div>

                <div class="b_footer--box __domains">

                    <div class="mod__flex">
                        <a class="b_footer--link" href="#">Санкт-Петербург</a>
                        <a class="b_footer--link" href="#">Новосибирск</a>
                        <a class="b_footer--link" href="#">Нижний Новгород</a>
                        <a class="b_footer--link" href="#">Самара</a>
                        <a class="b_footer--link" href="#">Казань</a>
                        <a class="b_footer--link" href="#">Уфа</a>
                        <a class="b_footer--link" href="#">Екатеринбург</a>
                        <a class="b_footer--link" href="#">Ростов-на-Дону</a>
                        <a class="b_footer--link" href="#">Краснодар</a>
                        <a class="b_footer--link" href="#">Челябинск</a>

                        <a class="b_footer--link __toggle __sm" href="#">Пермь</a>
                        <a class="b_footer--link __toggle __sm" href="#">Красноярск</a>
                        <a class="b_footer--link __toggle __sm" href="#">Набережные Челны</a>
                        <a class="b_footer--link __toggle __sm" href="#">Новокузнецк</a>
                        <a class="b_footer--link __toggle __sm" href="#">Волгоград</a>
                        <a class="b_footer--link __toggle __sm" href="#">Саратов</a>

                        <a class="b_footer--link __toggle __md" href="#">Тольятти</a>
                        <a class="b_footer--link __toggle __md" href="#">Тюмень</a>
                        <a class="b_footer--link __toggle __md" href="#">Иркутск</a>
                        <a class="b_footer--link __toggle __md" href="#">Омск</a>
                        <a class="b_footer--link __toggle __md" href="#">Воронеж</a>
                        <a class="b_footer--link __toggle __md" href="#">Тула</a>
                    </div>

                </div>

            </div>

    </div>
    <? inc_block("block__modal-call-order.php")?>
    <? inc_block("block__modal-ordering.php")?>
</div>