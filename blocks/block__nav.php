<nav class="block__nav" id="navigation" role="navigation">

    <ul class="b_nav--wrapper __active">

        <li class="b_nav--item __dropdown">
            <a class="b_nav--link" href="#" >
                О компании
            </a>
            <? inc_block("block__navigator.php"); ?>
        </li>

        <li class="b_nav--item __active">
            <a class="b_nav--link" href="#" >
                Производство
            </a>
        </li>

        <li class="b_nav--item">
            <a class="b_nav--link" href="#" >
                Каталог
            </a>
        </li>
        <li class="b_nav--item __dropdown">
            <a class="b_nav--link" href="#" >
                Услуги
            </a>
            <? inc_block("block__navigator.php"); ?>
        </li>

        <li class="b_nav--item">
            <a class="b_nav--link" href="#" >
                Проекты
            </a>
        </li>

        <li class="b_nav--item" style="display: none">
            <a class="b_nav--link" href="#" >
                Информация
            </a>
        </li>

        <li class="b_nav--item" style="display: none">
            <a class="b_nav--link" href="#" >
                Диллерам
            </a>
        </li>

        <li class="b_nav--item">
            <a class="b_nav--link" href="/contacts.php" >
                Контакты
            </a>
        </li>

    </ul>

</nav>