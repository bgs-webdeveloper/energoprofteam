<div class="block__display">

    <label class="b_display--label">

        <span class="b_display--wrapper">

            <span class="b_display--text">
                Отображать по:
            </span>

            <select class="select __small">
                <option value="10">10</option>
                <option value="20">20</option>
                <option value="30">30</option>
            </select>

        </span>

    </label>

    <div class="b_display--label">

        <div class="b_display--wrapper">

            <span class="b_display--text">
                Найдено моделей:
            </span>

            <b class="b_display--text __count">
                13337
            </b>

        </div>

    </div>

</div>