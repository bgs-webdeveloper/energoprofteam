<div class="block__sheet" style="display: none">

    <div class="b_sheet--wrapper __search">

        <div class="b_sheet--body">
            <? for( $i = 4; $i > 0; $i-- ): ?>
                <div class="b_sheet--item">

                    <a class="b_sheet--overlay" href="/product.php">

                        <div class="b_sheet--layout">
                            <img class="b_sheet--img" src="/img/products/e3-power-GG3300-x.png" alt="Привью товара" />
                        </div>

                        <div class="b_sheet--text">
                            <b>Тип оборудования:</b> Бензиновый генератор
                        </div>

                        <div class="b_sheet--text">
                            <b>Модель:</b> Gesan G 5 TF H Электростартер
                        </div>

                    </a>

                </div>
            <? endfor; ?>
        </div>

        <a class="button" href="/category.php">
            Показать все товары
        </a>

    </div>

    <script id="template__sheet" type="text/html" style="display: none" >
        <div class="b_sheet--item">

            <a class="b_sheet--overlay" href="#href#">

                <div class="b_sheet--layout">
                    <img class="b_sheet--img" src="#img#" alt="Привью товара" />
                </div>

                <div class="b_sheet--text">
                    <b>Тип оборудования:</b> #type#
                </div>

                <div class="b_sheet--text">
                    <b>Модель:</b> #model#
                </div>

            </a>

        </div>
    </script>

</div>