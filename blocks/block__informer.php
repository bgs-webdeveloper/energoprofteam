
<div class="block__informer">

    <div class="b_informer--wrapper">

        <div class="b_informer--overlay __down">

            <span class="b_informer--layout __opacity"></span>

            <span class="b_informer--layout __blend-mode"></span>

            <span class="b_informer--img" style="background-image: url('/img/informer/background_info_delivery.png')"></span>

        </div>

        <div class="b_informer--overlay __top">

            <div class="b_informer--title">Оперативная доставка </div>

            <p class="b_informer--text"> По Москве <br>и регионам <br>России </p>

            <img class="b_informer--icon" src="/img/informer/icon/icon_informer_delivery.svg" alt="Иконка" />

        </div>

    </div>

</div>

<div class="block__informer">

    <div class="b_informer--wrapper">

        <div class="b_informer--overlay __down">

            <span class="b_informer--layout __opacity"></span>

            <span class="b_informer--layout __blend-mode"></span>

            <span class="b_informer--img" style="background-image: url('/img/informer/background_info_vacancy_1.png')"></span>

        </div>

        <div class="b_informer--overlay __top">

            <div class="b_informer--title">Вакансия</div>

            <p class="b_informer--text">Инженер по <br>сборке систем <br>вентиляции</p>

            <img class="b_informer--icon" src="/img/informer/icon/icon_informer_vacancy_1.svg" alt="Иконка" />

        </div>

    </div>

</div>

<div class="block__informer">

    <div class="b_informer--wrapper">

        <div class="b_informer--overlay __down">

            <span class="b_informer--layout __opacity"></span>

            <span class="b_informer--layout __blend-mode"></span>

            <span class="b_informer--img" style="background-image: url('/img/informer/background_info_vacancy_2.png')"></span>

        </div>

        <div class="b_informer--overlay __top">

            <div class="b_informer--title">Вакансия</div>

            <p class="b_informer--text">Менеджер по продажам <br>промышленного <br>оборудования (без поиска)</p>

            <img class="b_informer--icon" src="/img/informer/icon/icon_informer_vacancy_2.svg" alt="Иконка" />

        </div>

    </div>

</div>