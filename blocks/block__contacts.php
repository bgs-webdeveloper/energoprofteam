<div class="block__contacts">

    <ul class="b_contacts--wrapper">

        <li class="b_contacts--item mod__hidden-lg __inline">

            <button class="b_overlay--button __burger" data-js="block.mobile.toggle">Меню</button>

            <a class="b_overlay--link" href="#">
                <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
            </a>

        </li>

        <li class="b_contacts--item __links">

            <div class="b_contacts--box">

                <i class="b_contacts--icon __address mod__hidden-sm"></i>

                <a class="b_contacts--link __underline mod__hidden-sm __inline" href="#" >
                    Москва, Мироновская, 25
                </a>

                <a class="b_contacts--link __underline mod__visible-sm __inline" href="#" >
                    <i class="b_contacts--icon __address"></i>
                    Контакты
                </a>

            </div>

            <div class="b_contacts--box mod__hidden-sm __inline">
                <i class="b_contacts--icon __work-time"></i>

                <span class="b_contacts--text">Пн-пт, c 9:00 до 19:00</span>
            </div>

        </li>

        <li class="b_contacts--item mod__hidden-sm __inline">

            <div class="b_contacts--box">
                <i class="b_contacts--icon __mail"></i>

                <a class="b_contacts--link __dotted" href="#" >mail@sklad-generator.ru</a>
            </div>

        </li>

        <li class="b_contacts--item mod__visible-lg __inline">

            <div class="b_contacts--box">
                <i class="b_contacts--icon __phone"></i>

                <a class="b_contacts--link" href="#" >+7 (495) 789-47-76</a>
                <a class="b_contacts--link" href="#" >+7 (800) 555-06-29</a>
            </div>

        </li>

    </ul>

</div>