<span class="block__hint">
    <button class="b_hint--button"></button>
    <span class="b_hint--window">
        <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
    </span>
</span>