<div class="block__filter">

    <div class="b_filter--wrapper">

        <div class="b_filter--header">

            <div class="block__tabs">

                <div class="b_tabs--wrapper">

                    <div class="mod__hidden-lg">

                        <div class="b_tabs--title">Подбор оборудования по параметрам</div>

                        <label class="b_tabs--layout">

                            <select class="b_tabs--select" name="select_filter">
                                <option>Генераторы</option>
                                <option>БУ генераторы</option>
                                <option>Контейнеры</option>
                                <option>Щиты</option>
                                <option>Стабилизаторы</option>
                                <option>ИБП</option>
                            </select>

                            <span class="b_tabs--text">Выберете категорию товара</span>

                        </label>

                    </div>

                    <ul class="b_tabs--header mod__visible-lg js-filter__header__tabs">

                        <li class="b_tabs--item __active">
                            <a class="b_tabs--link" href="#filter__tabs__content1">Генераторы</a>
                        </li>

                        <li class="b_tabs--item">
                            <a class="b_tabs--link" href="#filter__tabs__content2">БУ генераторы</a>
                        </li>

                        <li class="b_tabs--item">
                            <a class="b_tabs--link" href="#filter__tabs__content3">Контейнеры</a>
                        </li>

                        <li class="b_tabs--item">
                            <a class="b_tabs--link" href="#filter__tabs__content4">Щиты</a>
                        </li>

                        <li class="b_tabs--item">
                            <a class="b_tabs--link" href="#filter__tabs__content5">Стабилизаторы</a>
                        </li>
                        <li class="b_tabs--item">
                            <a class="b_tabs--link" href="#filter__tabs__content6">ИБП</a>
                        </li>

                    </ul>
                </div>

            </div>

        </div>

        <div class="b_filter--body__wrapper js-filter__tabs__body">
            
            <!-- Filter tab content 1 -->
            <div class="b_filter--body" id="filter__tabs__content1">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all js-select-all-manufactures" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes js_main-manufactures">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg js_filter__selected-manufactures">
                        
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

                <? inc_block("block__list-all-manufactures.php"); ?>
            </div>

            <!-- Filter tab content 2 -->
            <div class="b_filter--body" id="filter__tabs__content2" style="display: none">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Не Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg">
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksa
                        </a>
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksaaksa
                        </a>
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

            </div>

            <!-- Filter tab content 3 -->
            <div class="b_filter--body" id="filter__tabs__content3" style="display: none">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Супер Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg">
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksa
                        </a>
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksaaksa
                        </a>
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

            </div>

            <!-- Filter tab content 4 -->
            <div class="b_filter--body" id="filter__tabs__content4" style="display: none">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg">
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksa
                        </a>
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksaaksa
                        </a>
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

            </div>

            <!-- Filter tab content 5 -->
            <div class="b_filter--body" id="filter__tabs__content5" style="display: none">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg">
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksa
                        </a>
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksaaksa
                        </a>
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

            </div>

            <!-- Filter tab content 6 -->
            <div class="b_filter--body" id="filter__tabs__content6" style="display: none">

                <div class="b_filter--layout __manufacturers">

                    <div class="b_filter--checkboxes">

                        <label class="b_filter--label">Производитель:</label>

                        <a class="link __select-all" href="#">Выбрать всё</a>

                        <div class="b_filter--layout __classes">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Эконом
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Бизнес
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox" name="xxx">
                                <i></i>
                                Премиум
                            </label>
                        </div>

                        <div class="b_filter--layout __names" style="display: none;">
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                AMG
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Airman
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Aksa
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Ausonia
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Briggs & Stratton
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                E3 POWER
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Elemax
                            </label>
                            <label class="b_filter--label">
                                <input class="b_filter--checkbox" type="checkbox">
                                <i></i>
                                Energo
                            </label>
                        </div>

                    </div>

                    <a class="link __box js_filter--toggle" href="#">Все производители</a>

                    <div class="b_filter--layout __tags mod__visible-lg">
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksa
                        </a>
                        <a class="b_filter--button __tag" href="#">
                            <i></i> Aksaaksa
                        </a>
                    </div>

                </div>

                <div class="b_filter--layout __specification">

                    <div class="block__specification">

                        <ul class="b_specification--wrapper">

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <span class="b_specification--text">
                                        Топливо:

                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>

                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>Бензин</option>
                                            <option>Газ</option>
                                            <option>Дизель</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <span class="b_specification--text">
                                        Исполнение:
                                    </span>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>


                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Наличие АВР:
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                            </li>

                            <li class="b_specification--item">

                                <div class="b_specification--label">

                                    <div class="b_specification--text">
                                        Топливо:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>

                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Исполнение:
                                    </div>

                                    <label class="b_specification--layout">
                                        <select class="select">
                                            <option>не важно</option>
                                            <option>1</option>
                                            <option>2</option>
                                        </select>
                                    </label>
                                </div>

                                <div class="b_specification--label">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                                <div class="b_specification--label __extend hidden">
                                    <div class="b_specification--text">
                                        Мощность:
                                        <sub>кВт</sub>
                                        <span class="block__hint">
                                            <a class="b_hint--button" href="#"></a>
                                            <span class="b_hint--window">
                                                <span class="b_hint--text">Мощность генератора. Оптимальная работа
                                                    генератора происходит при суммарной нагрузке потребителей
                                                    75%-80%.</span>
                                            </span>
                                        </span>
                                    </div>
                                    <label class="b_specification--layout">
                                        <span class="b_specification--grid">
                                            <span class="b_specification--text">
                                                от
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                            <span class="b_specification--text">
                                                до
                                            </span>
                                            <input class="b_specification--input js-validation-numeric-values"
                                                type="text">
                                        </span>
                                    </label>
                                </div>

                            </li>

                        </ul>

                    </div>

                    <div class="b_filter--footer">
                        <a class="link __box __dotted js_filter-extend-toggle" href="#">Расширенный поиск</a>

                        <button class="button">Подобрать</button>
                    </div>

                </div>

            </div>
        </div>

    </div>

</div>