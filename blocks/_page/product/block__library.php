<div class="block__library">

    <div class="b_library--wrapper">

        <div class="b_library--header mod__hidden-sm">

            <div class="b_library--overlay mod__visible-lg">
                <a class="link __plus" href="#">
                    Добавить к сравнению
                </a>
            </div>

            <div class="b_library--overlay mod__hidden-lg">
                <a class="link __calc __underline" href="#">
                    Калькулятор стоимости доставки
                </a>
            </div>

            <div class="b_library--overlay">
                <a class="link __accept" href="#">
                    Гарантия и возврат
                </a>
            </div>

        </div>


        <div class="b_library--body">

            <div class="block__cert">

                <div class="b_cert--wrapper">

                    <div class="b_cert--overlay">

                        <div class="b_cert--list">

                            <div class="b_cert--item">

                                <div class="b_cert--layout __left">
                                    <img class="b_cert--img" src="/img/cert/cert_product.png" alt="сертификат" />
                                </div>

                                <div class="b_cert--layout __right">
                                    <p class="b_cert--text">
                                        Лучшее <br>оборудование <br>и инструменты <br>от лидеров <br>мирового рынка
                                    </p>
                                </div>
                            </div>

                        </div>

                        <div class="b_cert--controller">
                            <div class="b_cert--position">
                                <b>1</b>/2
                            </div>
                            <div class="b_cert--arrows">
                                <button class="b_cert--button __left"></button>
                                <button class="b_cert--button __right"></button>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_library--overlay">

                <div class="b_library--item __pdf">
                    <a class="link __dotted" href="#">Техническая спецификация серии Vega</a>
                </div>

                <div class="b_library--item __pdf">
                    <a class="link __dotted" href="#">Каталог ORTEA серии Vega</a>
                </div>

                <div class="b_library--item __pdf">
                    <a class="link __dotted" href="#">Преимущества серии Vega и Orion</a>
                </div>

                <div class="b_library--item __doc">
                    <a class="link __dotted" href="#">Преимущества серии Vega и Orion</a>
                </div>

            </div>


        </div>


        <div class="b_library--footer">

            <div class="text-16_24 __header">
                Эта модель не подходит?
                <br>Посмотрите похожие:
            </div>

            <br>

            <a class="link text-14_25" href="#">Газовый электрогенератор E3 POWER</a>
            <a class="link text-14_25" href="#">Электрогенератор E3 POWER</a>
        </div>


    </div>


</div>