<div class="block__showcase">

    <div class="b_showcase--wrapper __tab __b_y __active">

        <div class="b_content--header">

            <div class="text-24_28 __header " data-js="block.showcase.toggle">
                Аналоги с наработкой
                <span class="b_showcase--ui mod__hidden-lg __inline"></span>
            </div>

            <a class="b_content--tab mod__visible-lg __inline" href="#__analogs">Аналоги</a>

        </div>

        <div class="b_showcase--body">

            <div class="b_showcase--tools mod__visible-lg">

                <div class="block__setups">

                    <div class="b_setups--wrapper">

                        <div class="b_setups--head">
                            <div class="b_setups--label">Сортировка</div>
                            <a class="b_setups--link __main" data-showcase-hover="head" href="#">Название модели</a>
                        </div>

                        <div class="b_setups--body">
                            <a class="b_setups--link" data-showcase-hover="param-1" href="#">Топливо</a>
                            <a class="b_setups--link __active" data-hover="param-2" href="#">
                                Мощность
                            </a>
                            <a class="b_setups--link" data-showcase-hover="param-3" href="#">Напряжение</a>
                            <a class="b_setups--link" data-showcase-hover="param-4" href="#">Наличие АВР</a>
                            <a class="b_setups--link" data-showcase-hover="param-5" href="#">Исполнение</a>
                        </div>
                        <div class="b_setups--footer">
                            <a class="b_setups--link" data-showcase-hover="footer" href="#">Цена</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_showcase--items">

                <div class="block__slider __table" id="slider_product_sales">

                    <div class="b_slider--wrapper">

                        <ul class="b_slider--list" cost="2" >

                            <? for($i=0; $i < 6; $i ++ ) : ?>
                                <li class="b_slider--item">
                                    <? inc_page("product/block__product--sale.php"); ?>
                                </li>
                            <? endfor; ?>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="b_content--line mod__hidden-lg"></div>

    <div class="b_showcase--wrapper __tab __analogs ">

        <div class="b_content--header">

            <a class="b_content--tab mod__visible-lg __inline" href="#__b_y">
                Аналоги с наработкой
            </a>

            <div class="text-24_28 __header " data-js="block.showcase.toggle">
                Аналоги
                <span class="b_showcase--ui mod__hidden-lg __inline"></span>
            </div>

        </div>

        <div class="b_showcase--body">

            <div class="b_showcase--tools mod__visible-lg">

                <div class="block__setups">

                    <div class="b_setups--wrapper">

                        <div class="b_setups--head">
                            <div class="b_setups--label">Сортировка</div>
                            <a class="b_setups--link __main" data-showcase-hover="head" href="#">Название модели</a>
                        </div>

                        <div class="b_setups--body">
                            <a class="b_setups--link" data-showcase-hover="param-1" href="#">Топливо</a>
                            <a class="b_setups--link __active" data-hover="param-2" href="#">
                                Мощность
                            </a>
                            <a class="b_setups--link" data-showcase-hover="param-3" href="#">Напряжение</a>
                            <a class="b_setups--link" data-showcase-hover="param-4" href="#">Наличие АВР</a>
                            <a class="b_setups--link" data-showcase-hover="param-5" href="#">Исполнение</a>
                        </div>
                        <div class="b_setups--footer">
                            <a class="b_setups--link" data-showcase-hover="footer" href="#">Цена</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_showcase--items">

                <div class="block__slider __table" id="slider_product_sales">

                    <div class="b_slider--wrapper">

                        <ul class="b_slider--list" cost="2" >

                            <? for($i=0; $i < 6; $i ++ ) : ?>
                                <li class="b_slider--item">

                                    <? inc_page("product/block__product--sale.php"); ?>

                                </li>
                            <? endfor; ?>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <div class="b_content--line mod__hi"></div>

    </div>

</div>