<div class="block__price">

    <div class="b_price--wrapper">

        <div class="b_price--text">
            99 000 000
            <sub>руб.</sub>
        </div>

        <a class="button js-open-modal__ordering" href="#">Купить</a>

        <div class="b_price--overlay">
            <img class="b_price--img" src="/img/card_visa.png" alt="способ оплаты карта Mastercard">
            <img class="b_price--img" src="/img/card_mastercard.png" alt="способ оплаты карта VISA">
        </div>

        <div class="b_price--layout">

            <div class="b_price--sale">
                120 000 000 руб.
            </div>

            <div class="b_price--description">
                Цена и пусконаладочные работы в подарок действительны при заказе до 28 февраля!
            </div>

        </div>

        <div class="b_price--discount">

            <button class="button __blue">
                Получить скидку
            </button>

            <div class="b_price--layout __links mod__visible-lg">
                <a class="link __calc" href="#">
                    Калькулятор стоимости доставки
                </a>
            </div>

            <div class="mod__visible-sm">

                <div class="b_price--layout __tools mod__visible-sm">
                    <a class="link __calc" href="#">
                        Калькулятор стоимости доставки
                    </a>
                </div>

                <div class="b_price--layout __tools">
                    <a class="link __accept" href="#">
                        Гарантия и возврат
                    </a>
                </div>

            </div>

        </div>

    </div>

</div>