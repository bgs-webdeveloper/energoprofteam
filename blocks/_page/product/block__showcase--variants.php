<div class="block__showcase mod__visible-sm">

    <div class="b_showcase--wrapper __tab __variants">

        <div class="b_content--header">

            <div class="text-24_28 __header " data-js="block.showcase.toggle">
                Варианты исполнения
                <span class="b_showcase--ui mod__hidden-lg __inline"></span>
            </div>

        </div>

        <div class="b_showcase--body">

            <div class="b_showcase--tools mod__visible-lg">

                <div class="block__setups">

                    <div class="b_setups--wrapper">

                        <div class="b_setups--head">
                            <div class="b_setups--label">Сортировка</div>
                            <a class="b_setups--link __main" data-showcase-hover="head" href="#">Название модели</a>
                        </div>

                        <div class="b_setups--body">
                            <a class="b_setups--link" data-showcase-hover="param-1" href="#">Топливо</a>
                            <a class="b_setups--link __active" data-hover="param-2" href="#">
                                Мощность
                            </a>
                            <a class="b_setups--link" data-showcase-hover="param-3" href="#">Напряжение</a>
                            <a class="b_setups--link" data-showcase-hover="param-4" href="#">Наличие АВР</a>
                            <a class="b_setups--link" data-showcase-hover="param-5" href="#">Исполнение</a>
                        </div>
                        <div class="b_setups--footer">
                            <a class="b_setups--link" data-showcase-hover="footer" href="#">Цена</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_showcase--items">

                <div class="block__slider __table" id="slider_product_sales">

                    <div class="b_slider--wrapper">

                        <ul class="b_slider--list" cost="2" >

                            <? for($i=0; $i < 6; $i ++ ) : ?>
                                <li class="b_slider--item">

                                    <? inc_block("block__product--sale.php"); ?>

                                </li>
                            <? endfor; ?>

                        </ul>

                    </div>

                </div>

            </div>

        </div>

        <div class="b_content--line __small mod__hidden-lg"></div>

    </div>

</div>