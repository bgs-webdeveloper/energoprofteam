<div class="b_addon--item">

    <div class="b_addon--layout">
        <div class="b_addon--pic">
            <img class="b_addon--img" src="/img/products/gen.png" alt="Вариант товара" />
        </div>
    </div>

    <a class="b_addon--link" href="#">
        Pramac S12000 в контейнере
    </a>

    <div class="b_addon--text">
        Пуск — электростартер,	наличие автомата ввода резерва (АВР) - есть,	Исполнение - в контейнере
    </div>

    <div class="b_addon--tools mod__visible-lg">
        <a class="link __cross __dotted" href="#"> Добавить в сравнение </a>
    </div>

    <div class="block__price">

        <div class="b_price--wrapper __small">

            <div class="b_price--text">
                99 000 000
                <sub>руб.</sub>
            </div>

            <a class="button" href="#">Купить</a>

        </div>

    </div>

</div>