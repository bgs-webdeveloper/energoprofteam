<div class="block__table">

    <div class="b_table--wrapper">

        <div class="b_table--body">

            <div class="b_table--overlay">

                <div class="b_table--title">
                    Основные характеристики
                </div>

                <div class="b_table--row __main">

                    <div class="b_table--key">
                        Топливо
                        <span class="b_table--value">
                            Газ
                        </span>
                    </div>

                    <div class="b_table--key">
                        Мощность номинальня
                        <span class="b_table--value">2.4 кВт (2.4 кВА)</span>
                    </div>

                    <div class="b_table--key">
                        Напряжение
                        <span class="b_table--value">230 В</span>
                    </div>

                    <div class="b_table--key">
                        Двигатель
                        <span class="b_table--value">Briggs&Stratton Vanguard</span>
                    </div>
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        Мощность максимальная:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">2.6 кВт</span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Число фаз:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        1
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Пуск:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        ручной
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Наличие автомата ввода резерва (АВР):
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        ручной
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Исполнение:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        открытое
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Функция сварки:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        нет
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Степень защиты:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        IP 23
                    </span>
                </div>
            </div>

            <div class="b_table--overlay">

                <div class="b_table--title">
                    Дополнительные характеристики
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        Частота:
                        <span class="block__hint">
                            <a class="b_hint--button" href="#"></a>
                            <span class="b_hint--window">
                                <span class="b_hint--text">Мощность генератора. Оптимальная работа генератора происходит при суммарной нагрузке потребителей 75%-80%.</span>
                            </span>
                        </span>
                    </div>
                    <span class="b_table--value">
                        50 Гц
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Инверторная модель:
                    </div>
                    <span class="b_table--value">
                        нет
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Тип генератора:
                    </div>
                    <span class="b_table--value">
                        Синхронный
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Расход топлива при 75% нагрузке:
                    </div>
                    <span class="b_table--value">
                        0.7 л/ч
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Объем топливного бака:
                    </div>
                    <span class="b_table--value">
                        78 дБ
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Уровень шума:
                    </div>
                    <span class="b_table--value">
                        2.4 л
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Модель:
                    </div>
                    <span class="b_table--value">
                        EuroPower EP 2800 D
                    </span>
                </div>
            </div>

            <div class="b_table--overlay">

                <div class="b_table--title">
                    Двигатель
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        Производитель двигателя:
                    </div>
                    <span class="b_table--value">
                        Yanmar
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Модель двигателя:
                    </div>
                    <span class="b_table--value">
                        L 48 N
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Генератор:
                    </div>
                    <span class="b_table--value">
                        Mecc Alte S16W-90/CP
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Система охлаждения:
                    </div>
                    <span class="b_table--value">
                        воздушная
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Частота вращения двигателя:
                    </div>
                    <span class="b_table--value">
                        3000 об/мин
                    </span>
                </div>
            </div>

            <div class="b_table--overlay">

                <div class="b_table--title">
                    Гобариты
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        Масса:
                    </div>
                    <span class="b_table--value">
                        54 кг
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Длина:
                    </div>
                    <span class="b_table--value">
                        770 мм
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Ширина:
                    </div>
                    <span class="b_table--value">
                        510 мм
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Высота:
                    </div>
                    <span class="b_table--value">
                        560 мм
                    </span>
                </div>

            </div>

            <div class="b_table--overlay">

                <div class="b_table--title">
                    Производитель
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        Страна происхождения:
                    </div>
                    <span class="b_table--value">
                        Бельгия
                    </span>
                </div>
                <div class="b_table--row">
                    <div class="b_table--key">
                        Гарантия:
                    </div>
                    <span class="b_table--value">
                        2 года
                    </span>
                </div>

                <div class="b_table--row">
                    <div class="b_table--key">
                        <a class="link" href="#">
                            Информация о доставке
                        </a>
                    </div>
                    <span class="b_table--value">
                        &nbsp;
                    </span>
                </div>

            </div>

        </div>

        <div class="b_table--footer">
            <div class="b_table--text">
                Для консультации и заказа оборудования звоните по номеру
                <a class="b_table--text __phone" href="#">+7 (495) 181-12-72</a>
            </div>
        </div>

    </div>

</div>