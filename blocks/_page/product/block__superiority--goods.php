<div class="block__superiority">

    <div class="b_superiority--wrapper __product __active">

        <div class="b_superiority--layout __icons">

            <div class="b_superiority--item">
                <span class="b_superiority--icons" style="background-image: url('/img/svg/product/target.svg')"></span>
                <div class="b_superiority--title">
                    Уникальная точность
                </div>
                <p class="b_superiority--text">
                    Стабилизаторы Vega регулируют напряжение мягко и без скачков с уникальной точностью 0,5%. Свет будет гореть мягко и не моргая.
                </p>
            </div>

            <div class="b_superiority--item">
                <span class="b_superiority--icons" style="background-image: url('/img/svg/product/power.svg')"></span>
                <div class="b_superiority--title">
                    Встроенная молниезащита
                </div>
                <p class="b_superiority--text">
                    В стабилизаторах ORTEA уже встроена защита от перенапряжения, благодаря ей бытовые приборы не сгорят от высоковольтных бросков напряжения.
                </p>
            </div>

            <div class="b_superiority--item">
                <span class="b_superiority--icons" style="background-image: url('/img/svg/product/doc.svg')"></span>
                <div class="b_superiority--title">
                    Страховка от финансовых потерь
                </div>
                <p class="b_superiority--text">
                    Все электрооборудование, подключенное к стабилизаторам ORTEA, застраховано компанией "ИНГОССТРАХ" на 3 000 000 руб.
                </p>
            </div>

            <div class="b_superiority--item">
                <span class="b_superiority--icons" style="background-image: url('/img/svg/product/acept.svg')"></span>
                <div class="b_superiority--title">
                    Гарантия качества
                </div>
                <p class="b_superiority--text">
                    ORTEA предоставляет 2 года полной гарантии и 3 года бесплатного сервиса. При регулярном проведении работ по техническому обслуживанию VEGA дается пожизненная гарантия.
                </p>
            </div>

            <div class="b_superiority--item">
                <span class="b_superiority--icons" style="background-image: url('/img/svg/product/snowfly.svg')"></span>
                <div class="b_superiority--title">
                    Работа на морозе
                </div>
                <p class="b_superiority--text">
                    Стабилизаторы VEGA работают при -25°C благодаря использованию морозостойких компонентов. Это позволит установить их в неотапливаемом помещении.
                </p>
            </div>

        </div>

    </div>

</div>