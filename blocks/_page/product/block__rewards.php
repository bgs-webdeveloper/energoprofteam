<div class="b_content--box mod__visible-sm">

    <div class="b_content--line"></div>

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Сертификаты
        </div>

    </div>

    <div class="block__document">

        <div class="block__slider __active" id="slider_docs_rewards">

            <div class="b_slider--wrapper __docs">

                <div class="b_slider--overlay">

                    <div class="b_slider--layout __arrows">
                        <a class="b_slider--button __left"></a>
                        <a class="b_slider--button __right"></a>
                    </div>

                    <div class="b_slider--layout __frames">
                        <ul class="b_slider--controller">
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame __active">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                        </ul>
                    </div>

                </div>
                <ul class="b_slider--list " cost="3">
                    <li class="b_slider--item">

                        <a class="b_document--item">

                            <span class="b_document--layout">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </span>

                            <span class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</span>
                            <p class="b_document--text">2017</p>

                        </a>

                    </li>
                    <li class="b_slider--item">

                        <a class="b_document--item __land">

                            <span class="b_document--layout">
                                <img class="b_document--img" src="/img/preview/document_land.jpg" alt="Награда">
                            </span>

                            <span class="b_document--name">Мастер продаж. Выдан <br>компании «ЭнергоПроф»</span>
                            <p class="b_document--text">2017</p>

                        </a>

                    </li>
                    <li class="b_slider--item">

                        <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name" href="#">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>

                        </div>

                    </li>
                </ul>

            </div>


        </div>

        <div class="b_content--line __mini"></div>
    </div>

</div>