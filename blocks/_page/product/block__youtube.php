<div class="block__youtube">

    <div class="b_youtube--wrapper">

        <iframe
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                allowfullscreen=""
                class="video_resize 712x534"
                frameborder="0"
                width="100%"
                style="width: 712px; height: 534px"
                src="https://www.youtube.com/embed/VNzRTGLd6SM"
        ></iframe>

    </div>

</div>