<div class="block__breadcrumb mod__visible-lg">

    <div class="b_breadcrumb--wrapper">

        <ul class="b_breadcrumb--list">
            <li class="b_breadcrumb--item">
                <a class="b_breadcrumb--link" href="#">Каталог электростанций</a>
            </li>

            <li class="b_breadcrumb--item">
                <a class="b_breadcrumb--link" href="#">Каталог</a>
            </li>

            <li class="b_breadcrumb--item">
                <a class="b_breadcrumb--link" href="#">Газовые генераторы</a>
            </li>
        </ul>

        <a class="link __print" href="/_print.php">
            Версия для печати
        </a>

    </div>

</div>