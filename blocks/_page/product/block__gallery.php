<div class="block__gallery">

    <div class="b_gallery--wrapper">

        <a class="b_gallery--pic js-gallery-preview js-gallery-fancybox" href="/img/products/e3-power-GG3300-x.png">

            <img class="b_gallery--preview" src="/img/products/e3-power-GG3300-x.png" alt="Товар">

            <span class="b_gallery--sticker">Акция</span>

        </a>

        <div class="b_gallery--overlay">

            <div class="b_gallery--layout gallery-slider js-slider-gallery">
                <div class="b_gallery--item">
                    <a class="b_gallery--link js-gallery-link" href="#">
                        <img class="b_gallery--img" src="/img/products/e3-power-GG3300-x.png" alt="Товар">
                    </a>
                </div>
                <div class="b_gallery--item">
                    <a class="b_gallery--link js-gallery-link" href="#">
                        <img class="b_gallery--img" src="https://sad-ogorod.od.ua/images/shop/product/904/1/700/700/gazovyy-generator-kipor-kne-5500-e3.jpg" alt="Товар">
                    </a>
                </div>
                <div class="b_gallery--item">
                    <a class="b_gallery--link js-gallery-link" href="#">
                        <img class="b_gallery--img" src="https://hotline.ua/img/tx/127/1273625_s265.jpg" alt="Товар">
                    </a>
                </div>
                <div class="b_gallery--item">
                    <a class="b_gallery--link js-gallery-link" href="#">
                        <img class="b_gallery--img" src="/img/products/e3-power-GG3300-x.png" alt="Товар">
                    </a>
                </div>
                <div class="b_gallery--item">
                    <a class="b_gallery--link js-gallery-link" href="#">
                        <img class="b_gallery--img" src="/img/products/e3-power-GG3300-x.png" alt="Товар">
                    </a>
                </div>
            </div>

        </div>

    </div>

</div>