<div class="block__pic">

    <div class="b_pic--wrapper">

        <div class="b_pic--preview">
            <img class="b_pic--img" src="/img/pic/img.png" alt="изображение" />
        </div>

        <div class="b_pic--description">

            <div class="b_pic--text">
                Ошибки, совершенные при попытке самостоятельно установить генератор, могут привести к поломке дорогостоящего оборудования или нарушению техники безопасности.
            </div>

            <div class="b_pic--text mod__visible-lg">
                Также могут быть не учтены важные моменты, обеспечивающие исправную работу генератора: учет и контроль уровня шума, удобство технического обслуживания и ремонта и многое другое.
            </div>
        </div>

    </div>

</div>