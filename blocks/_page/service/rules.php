
<div class="block__rules">

    <div class="b_rules--wrapper">

        <div class="b_rules--head">
            Ознакомьтесь с базовыми правилами по установке генераторов:
        </div>

        <div class="b_rules--body">

            <a class="b_rules--link" href="#">

                <img class="b_rules--img" src="/img/rules/generator_setup_street.png" alt="Привью изображения" />

                Генератор <br>устанавливается <br>на улице (.pdf)
            </a>

            <a class="b_rules--link" href="#">

                <img class="b_rules--img" src="/img/rules/generator_setup_inside.png" alt="Привью изображения" />

                Генератор <br>устанавливается <br>в помещении (.pdf)
            </a>

        </div>

    </div>

</div>