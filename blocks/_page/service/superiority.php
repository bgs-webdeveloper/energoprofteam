<div class="block__superiority">

    <div class="b_superiority--wrapper __active">

        <div class="b_content--header __small">
            <div class="text-24_28 __header">Преимущества наших услуг</div>
        </div>

        <div class="b_superiority--layout __product __icons">

            <div class="mod__grid __trio">

                <div class="m_grid--column">

                    <div class="b_superiority--item">

                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-pro.svg')"></span>

                        <div class="b_superiority--text">
                            Отличные цены обеспечивают комфортные условия для оптовых и розничных клиентов
                        </div>

                    </div>

                </div>

                <div class="m_grid--column">

                    <div class="b_superiority--item">

                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-pro.svg')"></span>

                        <div class="b_superiority--text">
                            Отличные цены обеспечивают комфортные условия для оптовых и розничных клиентов

                        </div>

                    </div>

                </div>

                <div class="m_grid--column">

                    <div class="b_superiority--item">

                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-pro.svg')"></span>

                        <div class="b_superiority--text">
                            Отличные цены обеспечивают комфортные условия для оптовых и розничных клиентов
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="b_content--line"></div>

</div>