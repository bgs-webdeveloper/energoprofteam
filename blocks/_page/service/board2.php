
<div class="block__board mod__visible-sm">

    <div class="b_board--wrapper">

        <div class="b_rules--head">
            Пусконаладочные работы
        </div>

        <div class="b_board--body">

            <div class="b_board--row">
                <div class="b_board--label">
                    Стоимость работ
                </div>
                <div class="b_board--value">
                    определяется в зависимости от стоимости оборудования и составляет не менее 15 000 р.
                </div>
            </div>

            <div class="b_board--row">
                <div class="b_board--label">
                    Поездка на поезде/самолете
                </div>
                <div class="b_board--value">

                    <div class="block__list">
                        <ul class="b_list--wrapper __block">
                            <li class="b_list--item">предварительное подключение и тест электростанции во всех режимах,</li>
                            <li class="b_list--item">проверка всех  функций,</li>
                            <li class="b_list--item">предварительное подключение нагрузок и тест станции под нагрузкой,</li>
                            <li class="b_list--item">настройка блока АВР на необходимые параметры,</li>
                            <li class="b_list--item">составление акта официального ввода в эксплуатацию</li>
                        </ul>
                    </div>

                </div>
            </div>

        </div>

        <div class="b_board--footer">
            Стоимость консультационного выезда вычитается из последующего монтажа и пусконаладки
        </div>

        <div class="b_board--body">

            <div class="b_board--row">

                <div class="b_board--label __full">

                    <a class="link" href="#">
                        Посмотреть полную таблицу
                    </a>

                </div>

            </div>

        </div>

    </div>

</div>