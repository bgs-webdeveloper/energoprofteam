
<div class="mod__hidden-sm">

    <table class="table">

        <colgroup>
            <col width="180">
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
            <col>
        </colgroup>
        <thead>
            <tr>
                <th class="head" colspan="11">Прайс-лист на монтажные работы (не включая материалы)</th>
            </tr>
            <tr class="padding">
                <th rowspan="2" class="blue main">Услуга</th>

                <th colspan="2" class="light">до 10 кВт</th>
                <th colspan="2">10-20 кВт</th>
                <th colspan="2" class="light">20-40 кВт</th>
                <th colspan="2">40-60 кВт</th>
                <th colspan="2" class="light">60-80 кВт</th>
            </tr>
            <tr class="padding">
                <th class="light">стандарт</th>
                <th class="light">премиум</th>
                <th>стандарт</th>
                <th>премиум</th>
                <th class="light">стандарт</th>
                <th class="light">премиум</th>
                <th>стандарт</th>
                <th>премиум</th>
                <th class="light">стандарт</th>
                <th class="light">премиум</th>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td>Прокладка силового кабеля по воздуху или по подготовленной трассе (цена за 3 метра)</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
            </tr>
            <tr>
                <td>Монтаж блока АВР</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
            </tr>
            <tr>
                <td>Устройство заземления станции</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
            </tr>
            <tr>
                <td>Подготовка кабельных трасс, подготовка площадки, земляные работы</td>
                <td class="notice light" colspan="10">
                    Только после предоставления Заказчиком технического задания
                </td>
            </tr>
            <tr>
                <td>Устройство заземления станции</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
                <td> 2500 р.</td>
                <td> 99 000 р.</td>
                <td class="light"> 2500 р.</td>
                <td class="light"> 99 000 р.</td>
            </tr>
        </tbody>

    </table>

</div>