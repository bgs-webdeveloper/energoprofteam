
<div class="block__board">

    <div class="b_board--wrapper">

        <div class="b_board--head">
            Расчет выезда специалиста
        </div>

        <div class="b_board--body">

            <div class="b_board--row">
                <div class="b_board--label">
                    Консультационный выезд по Москве и МО
                </div>
                <div class="b_board--value">
                    от 4500 руб.
                </div>
            </div>

            <div class="b_board--row">
                <div class="b_board--label">
                    Поездка на поезде/самолете
                </div>
                <div class="b_board--value">
                    время в пути: 3 000р/час, но не более 8 часов в сутки + билеты (+20%) + гостиница + командировочные (4 500р/сутки)
                </div>
            </div>

        </div>

        <div class="b_board--footer">
            Стоимость консультационного выезда вычитается из последующего монтажа и пусконаладки
        </div>

    </div>

</div>