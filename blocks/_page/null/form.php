<div class="b_404--overlay __contact">

    <div class="b_404--layout">
        <div class="text-22_30">
            Для получения оперативной консультации свяжитесь с нами, либо оставьте свой номер телефона
        </div>
    </div>

    <div class="b_content--line __mini"></div>

    <div class="b_404--layout __left">

        <div class="b_404--title">Позвоните нам</div>

        <div class="block__contacts">

            <ul class="b_contacts--wrapper __content">

                <li class="b_contacts--item">

                    <div class="b_contacts--box">
                        <i class="b_contacts--icon __phone"></i>

                        <a class="b_contacts--link" href="#">+7 (495) 789-47-76</a>
                        <a class="b_contacts--link" href="#">+7 (800) 555-06-29</a>
                    </div>

                </li>

            </ul>

        </div>

    </div>

    <div class="b_404--layout __right">

        <div class="b_404--title">Напишите нам</div>

        <div class="block__contacts">

            <ul class="b_contacts--wrapper __content">

                <li class="b_contacts--item">

                    <div class="b_contacts--box">
                        <i class="b_contacts--icon __mail"></i>

                        <a class="b_contacts--link __dotted" href="#">mail@sklad-generator.ru</a>
                    </div>

                </li>

            </ul>

        </div>

    </div>

    <div class="b_content--line __mini mod__visible-lg"></div>

    <div class="b_404--layout mod__visible-lg">

        <div class="b_404--title">
            Мы перезвоним в течение 15 минут
        </div>

        <form class="b_4040--form">

            <label class="b_404--label">
                <input class="b_404--input input" type="text" name="name" placeholder="Ваше имя"/>
            </label>

            <label class="b_404--label">
                <input class="b_404--input input" type="text" name="phone" placeholder="+7 (___) ___-__-__"/>
            </label>

            <label class="b_404--label">
                <button class="b_404--button button">Отправить</button>
            </label>

        </form>

    </div>

</div>