<div class="block__price">

    <div class="b_price--wrapper __print">

        <div class="b_price--layout">

            <div class="b_price--text">
                99 000 000
                <sub>руб.</sub>
            </div>

            <div class="b_price--sale">
                120 000 000 руб.
            </div>

        </div>

        <div class="b_price--description">
            Цена и пусконаладочные работы в подарок действительны при заказе до 28 февраля!
        </div>

    </div>

</div>