
<div class="block__catalog">

    <div class="b_catalog--"></div>

    <div class="b_catalog--wrapper">


        <div class="b_catalog--item __open">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/gen.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Электростанции</a>

                <p class="b_catalog--text">
                    Электростанции - это автономные источники электроснабжения, незаменимые в качестве
                    резервного источника питания и при отсутствии соединения с централизованным
                    источником. В каталоге представлены более 7 000 моделей электрогенераторов
                    мощностью от 0,65 до 2440 кВт производителей ТСС, Fubag, SDMO, Pramac, Вепрь и других.
                </p>

            </div>

            <div class="b_catalog--footer">

                <div class="block__tabs">

                    <div class="b_tabs--wrapper __catalog">

                        <ul class="b_tabs--header">

                            <li class="b_tabs--item js_tabs--active __active">
                                <a class="b_tabs--link" href="#by_fule">По топливу</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_power">По мощности</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_type">По типу</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_country">По стране</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_faza">По числу фаз</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_style">По исполнению</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_engine">По двигателю</a>
                            </li>

                        </ul>

                        <div class="b_tabs--layout">

                            <div class="b_tabs--content __active" id="by_fule">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_power">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_type">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_country">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_faza">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_style">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                            <div class="b_tabs--content" id="by_engine">

                                <? inc_page("katalog/b_catalog--layout.php"); ?>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/container.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Контейнеры</a>

                <p class="b_catalog--text">
                    Прочные металлические контейнеры предназначены для защиты энергетического оборудования
                    от механических и погодно-климатических воздействий. Также этот тип корпуса значительно
                    снижает уровень рабочего шума электрогенератора и способен продлить срок службы техники.
                </p>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/avr.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Автоматы ввода резерва</a>

                <p class="b_catalog--text">
                    Автомат ввода резерва включает резервное электропитание при потере или отсутствии
                    централизованного. АВР используют с бензиновыми и дизельными генераторами, у
                    которых есть возможность его подключения. В каталоге представлены автоматы ввода
                    резерва собственного производства под маркой ЭнергоПроф.
                </p>

            </div>

            <div class="b_catalog--footer">

                <div class="block__tabs">

                    <div class="b_tabs--wrapper __catalog">

                        <ul class="b_tabs--header">

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_style">По исполнению</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_target">По назначению</a>
                            </li>

                        </ul>

                        <div class="b_tabs--layout">

                            <div class="b_tabs--content" id="by_style">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                            <div class="b_tabs--content" id="by_target">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/electro-shitovoe.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Электрощитовое оборудование</a>

                <p class="b_catalog--text">
                    В данном разделе представлены электрические щиты бытового и промышленного типа. Они
                    используются для установки автоматических выключателей, УЗО и других
                    электроэлементов. Мы предлагаем только надежные и сертифицированные товары по
                    выгодным ценам.
                </p>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/ibp.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Источники бесперебойного питания</a>

                <p class="b_catalog--text">
                    Источник бесперебойного питания используют при отсутствии стабильного напряжения
                    тока, а также для кратковременного и долговременного электропитания в автономном
                    режиме. В нашем каталоге представлено более 250 моделей с выходной мощностью от 1,5
                    до 600 кВа производства General Electric, Inelt, Inform, Makelsan и Vision.
                </p>

            </div>

            <div class="b_catalog--footer">

                <div class="block__tabs">

                    <div class="b_tabs--wrapper __catalog">

                        <ul class="b_tabs--header">

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_power">По мощности</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_type">По типу подключения</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_faza">По числу фаз</a>
                            </li>

                            <li class="b_tabs--item js_tabs--active">
                                <a class="b_tabs--link" href="#by_style">По типу</a>
                            </li>

                        </ul>

                        <div class="b_tabs--layout">

                            <div class="b_tabs--content" id="by_power">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                            <div class="b_tabs--content" id="by_type">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                            <div class="b_tabs--content" id="by_faza">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                            <div class="b_tabs--content" id="by_style">
                                <? inc_page("katalog/b_catalog--layout.php"); ?>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/stabilizator.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">Стабилизаторы напряжения</a>

                <p class="b_catalog--text">
                    Стабилизатор напряжения устанавливают для генераторов с целью поддержания
                    напряжения сети в пределах нормы. Стабилизаторы защищают оборудование от резких
                    сетевых перепадов и обеспечивают стабильную нагрузку. В нашем каталоге вы найдете
                    более 200 моделей стабилизаторов напряжения марок Lider, Ortea и Штиль.
                </p>

            </div>

        </div>


        <div class="b_catalog--item">

            <a class="b_catalog--head" href="#">
                <img class="b_catalog--img" src="/img/catalog/b_y_gen.png" alt="Привью товара" />
            </a>

            <div class="b_catalog--body">

                <a class="b_catalog--title" href="#">БУ Генераторы</a>

                <p class="b_catalog--text">
                    Стабилизатор напряжения устанавливают для генераторов с целью поддержания
                    напряжения сети в пределах нормы. Стабилизаторы защищают оборудование от резких
                    сетевых перепадов и обеспечивают стабильную нагрузку. В нашем каталоге вы найдете
                    более 200 моделей стабилизаторов напряжения марок Lider, Ortea и Штиль.
                </p>

            </div>

        </div>


    </div>

</div>