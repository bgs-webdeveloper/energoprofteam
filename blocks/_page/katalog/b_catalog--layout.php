<div class="b_catalog--layout">

    <div class="b_catalog--name">Дизельные</div>

    <a class="b_catalog--link" href="/category.php#Инверторные">Инверторные</a>
    <a class="b_catalog--link" href="/category.php#Бытовые">Бытовые</a>
    <a class="b_catalog--link" href="/category.php#Автономные">Автономные</a>
    <a class="b_catalog--link" href="/category.php#Передвижные">Передвижные</a>
    <a class="b_catalog--link" href="/category.php#Стационарные">Стационарные</a>
    <a class="b_catalog--link" href="/category.php#Резервные">Резервные</a>
    <a class="b_catalog--link" href="#">Переносные</a>
    <a class="b_catalog--link" href="#">С автозапуском</a>
    <a class="b_catalog--link" href="#">С автоматикой</a>
    <a class="b_catalog--link" href="#">С электростартером</a>
    <a class="b_catalog--link" href="#">В контейнере</a>
    <a class="b_catalog--link" href="#">Компактные</a>
    <a class="b_catalog--link" href="#">Вепрь АДП</a>
    <a class="b_catalog--link" href="#">Вепрь АДС</a>
    <a class="b_catalog--link" href="#">Малой мощности</a>
    <a class="b_catalog--link" href="#">Для дачи</a>
    <a class="b_catalog--link" href="#">Для дома</a>
    <a class="b_catalog--link" href="#">Генераторы АД</a>
    <a class="b_catalog--link" href="#">Генераторы АД-200</a>
</div>


<div class="b_catalog--layout">

    <div class="b_catalog--name">Бензиновые</div>

    <a class="b_catalog--link" href="#">Стационарные</a>
    <a class="b_catalog--link" href="#">В контейнере</a>
    <a class="b_catalog--link" href="#">Для дачи</a>
    <a class="b_catalog--link" href="#">Для дома</a>
    <a class="b_catalog--link" href="#">Инверторные до 3 кВт</a>
    <a class="b_catalog--link" href="#">Инверторные</a>
    <a class="b_catalog--link" href="#">Бытовые</a>
    <a class="b_catalog--link" href="#">Автономные</a>
    <a class="b_catalog--link" href="#">Переносные</a>
    <a class="b_catalog--link" href="#">В кожухе</a>
    <a class="b_catalog--link" href="#">С автозапуском</a>
    <a class="b_catalog--link" href="#">С автоматикой</a>
    <a class="b_catalog--link" href="#">С электростартером</a>
    <a class="b_catalog--link" href="#">Резервные</a>
    <a class="b_catalog--link" href="#">Kipor</a>
    <a class="b_catalog--link" href="#">Вепрь 6 кВт</a>
    <a class="b_catalog--link" href="#">Вепрь АБП</a>
</div>

<div class="b_catalog--layout">

    <div class="b_catalog--name">Дизельные</div>

    <a class="b_catalog--link" href="#">Бензиновые</a>
    <a class="b_catalog--link" href="#">Дизельные</a>
    <a class="b_catalog--link" href="#">Постоянного тока</a>
    <a class="b_catalog--link" href="#">Инвертор</a>
    <a class="b_catalog--link" href="#">Бытовые инверторы</a>
    <a class="b_catalog--link" href="#">Передвижные</a>
    <a class="b_catalog--link" href="#">Автономные</a>
</div>

<div class="b_catalog--layout">

    <div class="b_catalog--name">Дизельные</div>

    <a class="b_catalog--link" href="#">Бытовые</a>
    <a class="b_catalog--link" href="#">Для дачи</a>
    <a class="b_catalog--link" href="#">Для дома</a>
    <a class="b_catalog--link" href="#">с АВР</a>
    <a class="b_catalog--link" href="#">Промышленные</a>
</div>