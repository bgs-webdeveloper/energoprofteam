<div class="block__office">

    <div class="b_office--wrapper">

        <div class="b_office--item __active">

            <div class="b_office--title">
                Центральный офис в Москве
            </div>

            <div class="b_office--head">

                <div class="b_office--overlay __buttons">

                    <div class="b_office--layout __phone">

                        <div class="b_office--row">
                            <a class="b_office--link" href="#">
                                +7 (495) 789-47-76
                            </a>
                        </div>

                        <div class="b_office--row">
                            <a class="b_office--link __notice" href="#">
                                +7 (800) 555-06-29
                                <sub>
                                    (Бесплатно из любого региона России)
                                </sub>
                            </a>
                        </div>

                        <button class="button mod__hidden-lg">
                            Открыть в картах
                        </button>

                    </div>

                </div>

                <div class="b_office--overlay __text">

                    <div class="b_office--layout __geo-point">
                        <p class="b_office--text">105318, Москва, Мироновская 25</p>
                    </div>

                    <div class="b_office--layout __clock">
                        <p class="b_office--text">Пн-пт с 9:00 до 19:00</p>
                    </div>

                    <div class="b_office--layout __mail">

                        <div class="b_office--row">
                            <a class="b_office--link" href="#">
                                <span class="b_office--ui __dotted">
                                    mail@sklad-generator.ru
                                </span>
                            </a>
                        </div>

                        <div class="b_office--row">
                            <a class="b_office--link " href="#">
                                <span class="b_office--ui __dotted">
                                    gendir@energoprof-group.ru
                                </span>
                            </a>
                            <span class="b_office--text">(по спорным вопросам вы можете обратиться к руководителю компании).</span>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_office--footer mod__hidden-lg">
                <img src="/img/temp/contact_yandex_tablet_office.jpg" alt="Yandex карта" style="width: 100%">
            </div>

        </div>

        <div class="b_office--item">

            <div class="b_office--title">
                Производственные площадки
            </div>

            <div class="b_office--head">

                <div class="b_office--overlay __buttons">

                    <div class="b_office--layout __phone">

                        <div class="b_office--row">
                            <a class="b_office--link __notice" href="#">
                                +7 (800) 555-06-29
                                <sub>
                                    (сервисная поддержка)
                                </sub>
                            </a>
                        </div>

                        <button class="button mod__hidden-lg">
                            Открыть в картах
                        </button>

                    </div>

                </div>

                <div class="b_office--overlay __text">

                    <div class="b_office--layout __geo-point">
                        <p class="b_office--text">
                            Москва, 1-я улица Бухвостова, дом 12/11, к4
                            <br>Мытищи, улица Силикатная, дом 38, стр.2
                        </p>
                    </div>

                    <div class="b_office--layout __clock">
                        <p class="b_office--text">Пн-пт с 9:00 до 19:00</p>
                    </div>

                    <div class="b_office--layout __flip">
                        <p class="b_office--text">
                            Вы можете забрать оборудование самовывозом со склада, предварительно договорившись с персональным менеджером.
                        </p>
                    </div>
                    <div class="b_office--layout">
                        <button class="button __small mod__visible-lg" type="button">
                            Схема проезда
                        </button>
                    </div>

                </div>

            </div>

            <div class="b_office--footer mod__hidden-lg">
                <img src="/img/temp/contact_yandex_tablet_office.jpg" alt="Yandex карта" style="width: 100%">
            </div>

        </div>

        <div class="b_office--map mod__visible-lg">

            <div class="b_office--footer">
                <img src="/img/temp/contact_yandex_tablet_office.jpg" alt="Yandex карта" style="width: 100%">
            </div>
        </div>

    </div>

</div>