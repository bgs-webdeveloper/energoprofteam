<div class="block__filial mod__visible-lg">

    <div class="b_filial--wrapper">

        <div class="b_filial--head">
            <div class="b_filial--title">
                Наши филиалы
            </div>
        </div>

        <div class="b_filial--body">

            <ul class="b_filial--list">

                <? for( $i=0; $i<22; $i++): ?>

                    <li class="b_filial--item">

                        <a class="b_filial--name" href="#">
                            Санкт-Петербург
                        </a>

                        <a class="b_filial--phone" href="tel:+7(812)385-74-98">
                            +7 (812) 385-74-98
                        </a>

                        <a class="b_filial--phone" href="tel:+7(800)100-27-93">
                            +7 (800) 100-27-93
                        </a>

                        <a class="link __dotted" href="#">
                            sales+spb@sklad-generator.ru
                        </a>

                    </li>

                <? endfor; ?>

            </ul>

        </div>

    </div>

</div>