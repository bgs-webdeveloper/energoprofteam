<div class="block__document">

    <div class="b_document--wrapper __trio">

        <? for( $i=0; $i <=3; $i++ ) : ?>

            <div class="b_document--item">

                <a class="b_document--layout" href="#">
                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                </a>

                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                <p class="b_document--text">2017</p>
            </div>

            <div class="b_document--item __land">

                <a class="b_document--layout">
                    <img class="b_document--img" src="/img/preview/document_land.jpg" alt="Награда">
                </a>

                <a class="b_document--name">Мастер продаж. Выдан <br>компании «ЭнергоПроф»</a>
                <p class="b_document--text">2017</p>

            </div>

            <div class="b_document--item">

                <a class="b_document--layout" href="#">
                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                </a>

                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                <p class="b_document--text">2017</p>
            </div>

        <? endfor; ?>
    </div>

</div>