<div class="block__pagination">

    <div class="b_pagination--wrapper">

        <div class="b_pagination--overlay __left">

            <div class="b_pagination--layout">
                <div class="b_pagination--label">Страницы:</div>
                <a class="b_pagination--link __active" href="#">1</a>
                <a class="b_pagination--link" href="#">2</a>
                <a class="b_pagination--link" href="#">3</a>
                <span class="b_pagination--separator">...</span>
                <a class="b_pagination--link" href="#">444</a>
                <a class="b_pagination--link" href="#">445</a>
            </div>

        </div>

    </div>

</div>