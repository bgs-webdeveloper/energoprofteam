
<div class="block__chronicle">

    <div class="b_chronicle--wrapper">

        <article class="b_chronicle--item __glow">

            <a class="b_chronicle--head" href="/article.php">

                <img class="b_chronicle--img" src="/img/chronicle/gen1.png" alt="Привью новости" />

            </a>

            <div class="b_chronicle--body">

                <span class="b_chronicle--data">
                    07 марта 2019
                </span>

                <a class="b_chronicle--title" href="/article.php">
                    Пополнение в ассортименте ДГУ Амперос
                </a>

                <p class="b_chronicle--text">
                    Прочные металлические контейнеры предназначены для защиты энергетического оборудования
                    от механических и погодно-климатических воздействий. Также этот тип корпуса значительно
                    снижает уровень рабочего шума электрогенератора и способен продлить срок службы техники.
                </p>

            </div>

        </article>

        <? for( $i=1; $i < 9; $i++ ): ?>

            <? inc_block("_items/article.php"); ?>

        <? endfor; ?>

    </div>

</div>