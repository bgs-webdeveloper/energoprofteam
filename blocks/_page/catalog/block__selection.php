<div class="b_content--box">

    <div class="block__selection">

        <div class="b_selection--wrapper __catalog">

            <div class="b_selection--item __single">

                <div class="b_selection--layout">

                    <div class="b_selection--head" data-js="block.selection.toggle">

                        <div class="b_selection--header">

                            <div class="text-16_24 __header">
                                Быстрый подбор по типу
                            </div>
                            <i class="b_selection--ui mod__hidden-lg __inline"></i>

                        </div>

                    </div>

                    <div class="b_selection--body">

                        <div class="b_selection--column">
                            <a class="b_selection--link" href="#">Бытовые</a>
                            <a class="b_selection--link" href="#">Для дачи</a>
                        </div>

                        <div class="b_selection--column">
                            <a class="b_selection--link" href="#">Для дома</a>
                            <a class="b_selection--link" href="#">Промышленные</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_selection--item __single">

                <div class="b_selection--layout">

                    <div class="b_selection--head" data-js="block.selection.toggle">

                        <div class="b_selection--header">

                            <div class="text-16_24 __header">
                                Быстрый подбор по стране
                            </div>
                            <i class="b_selection--ui mod__hidden-lg __inline"></i>

                        </div>

                    </div>

                    <div class="b_selection--body">

                        <div class="b_selection--column">
                            <a class="b_selection--link" href="#">Россия</a>
                            <a class="b_selection--link" href="#">Италия</a>
                            <a class="b_selection--link" href="#">Япония</a>
                        </div>

                        <div class="b_selection--column">
                            <a class="b_selection--link" href="#">Китай</a>
                            <a class="b_selection--link" href="#">Испания</a>
                            <a class="b_selection--link" href="#">Германия</a>
                        </div>

                    </div>

                </div>

            </div>

            <div class="b_selection--item __single">

                <div class="b_selection--layout">

                    <div class="b_selection--head" data-js="block.selection.toggle">

                        <div class="b_selection--header">

                            <div class="text-16_24 __header">
                                Быстрый подбор по исполнению
                            </div>
                            <i class="b_selection--ui mod__hidden-lg __inline"></i>

                        </div>

                    </div>

                    <div class="b_selection--body">

                        <a class="b_selection--link" href="#">Быстрый подбор по исполнению</a>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>