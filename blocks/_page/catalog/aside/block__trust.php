
<div class="block__trust">

    <div class="b_trust--layout">

        <div class="b_trust--header">
            <div class="text-24_28 __header">Нам доверяют</div>
        </div>

        <a class="link __arrow" href="#">Все клиенты</a>
    </div>

    <div class="b_trust--overlay">

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/lukoil.png')"></div>
            <a class="b_trust--link" href="#">Лукойл</a>
        </div>

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/sochi.png')"></div>
            <a class="b_trust--link" href="#">Ок в Сочи</a>
        </div>

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/vtb.png')"></div>
            <a class="b_trust--link" href="#">Банк ВТБ</a>
        </div>

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/mvd.png')"></div>
            <a class="b_trust--link" href="#">МВД России</a>
        </div>

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/sochi.png')"></div>
            <a class="b_trust--link" href="#">Ок в Сочи</a>
        </div>

        <div class="b_trust--item">
            <div class="b_trust--img" style="background-image: url('/img/clients/lukoil.png')"></div>
            <a class="b_trust--link" href="#">Лукойл</a>
        </div>

    </div>

</div>

