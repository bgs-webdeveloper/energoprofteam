
<div class="block__superiority">

    <div class="b_superiority--wrapper __open">
        <!-- Список преимущества не скрывающий ся __open-->

        <div class="b_content--header __small">
            <div class="text-24_28">Преимущества наших услуг:</div>
            <i class="b_superiority--ui"></i>
        </div>

        <div class="b_superiority--layout __icons">

            <div class="mod__grid __dual">

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/svg/superiority/catalog-power.svg')" ></span>
                        <div class="b_superiority--title">
                            Мощность
                        </div>
                        <p class="b_superiority--text">
                            Для того чтобы приобрести оптимальное оборудование следует сложить потребляемую мощность всех подключаемых электрических приборов и добавить еще 25-30% (запас мощности) так как постоянная интенсивная работа нежелательна для генератора
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/svg/superiority/catalog-control.svg')" ></span>
                        <div class="b_superiority--title">
                            Наличие блока АВР и таймера
                        </div>
                        <p class="b_superiority--text">
                            Этот модуль предназначен для контроля состояния городской электрической сети. При возникновении каких-либо проблем с основной системой питания, блок АВР автоматически запустит резервную систему электроснабжения. Также есть возможность включения генератора в определенное время суток.
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/svg/superiority/catalog-bak.svg')" ></span>
                        <div class="b_superiority--title">
                            Объем бака
                        </div>
                        <p class="b_superiority--text">
                            Вместительный газовый баллон позволит генератору работать сутки или более без дозаправки.
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/svg/superiority/catalog-cube.svg')" ></span>
                        <div class="b_superiority--title">
                            Исполнение
                        </div>
                        <p class="b_superiority--text">
                            Вы можете выбрать агрегат открытого типа, станцию в герметичном шумопоглощающем кожухе или прочном металлическом контейнере.
                        </p>
                    </div>

                </div>


            </div>

        </div>

    </div>
</div>