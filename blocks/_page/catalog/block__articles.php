<div class="block__articles">

    <section class="b_articles--wrapper">

        <article class="b_articles--item">

            <img class="b_articles--img" src="/img/news/1.png" alt="изображение" />

            <a class="b_articles--link" href="#">Газовые генераторные установки ФАС</a>

            <p class="b_articles--text">Торговая марка ФАС принадлежит промышленной группе «FAS-Россия», которая была основана в 1995 году.</p>

        </article>

        <article class="b_articles--item">

            <img class="b_articles--img" src="/img/news/2.png" alt="изображение" />

            <a class="b_articles--link" href="#">Как выбрать газовый генератор</a>

            <p class="b_articles--text">Эта статья полезна для тех, кто готовится к покупке газового генератора. Вы узнаете основные критерии выбора электростанции на газовом топливе, правила расчета нагрузки и времени непрерывной работы.</p>

        </article>

        <article class="b_articles--item">

            <img class="b_articles--img" src="/img/news/3.png" alt="изображение" />

            <a class="b_articles--link" href="#">Как работает газовый генератор?</a>

            <p class="b_articles--text">В статье представлена краткая информация о принципе работы газовых генераторов, их использовании и подключении.</p>

        </article>

        <a class="link __arrow" href="#">Все статьи</a>

    </section>

</div>