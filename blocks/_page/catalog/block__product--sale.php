<div class="block__product">

    <div class="b_product--item">

        <div class="b_product--head">

            <div class="b_projects--preview">
                <span class="sticker">Акция</span>
                <a class="b_product--link __clear" href="/product.php">
                    <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                </a>
            </div>

            <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>

            <div class="mod__visible-lg">
                <div class="b_projects--icons">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/faza_1.png">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/sync_sync.png">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/start-electro.png">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/varka-no.png">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/garanty-1.png">
                    <img class="b_projects--ico" alt="иконка" title="подсказка иконки"  src="/img/icons/country-rus-china.png">
                </div>
                <a class="link __cross __dotted" href="#"> Добавить в сравнение </a>
            </div>

        </div>

        <div class="b_product--body">

            <div class="block__params">

                <ul class="b_params--wrapper">

                    <li class="b_params--item">
                        <dl class="b_params--overlay">
                            <dt class="b_params--layout">
                                <span>Топливо</span>
                            </dt>
                            <dd>дизель</dd>
                        </dl>
                    </li>

                    <li class="b_params--item __active">
                        <dl class="b_params--overlay">
                            <dt class="b_params--layout">
                                <span>Мощность</span>
                            </dt>
                            <dd>
                                2 кВт
                                <sub>(2 кВА)</sub>
                            </dd>
                        </dl>
                    </li>

                    <li class="b_params--item">
                        <dl class="b_params--overlay">
                            <dt class="b_params--layout">
                                <span>Напряжение</span>
                            </dt>
                            <dd>230 В</dd>
                        </dl>
                    </li>

                    <li class="b_params--item">
                        <dl class="b_params--overlay">
                            <dt class="b_params--layout">
                                <span>Наличие АВР</span>
                            </dt>
                            <dd>нет</dd>
                        </dl>
                    </li>

                    <li class="b_params--item">
                        <dl class="b_params--overlay">
                            <dt class="b_params--layout">
                                <span>Исполнение</span>
                            </dt>
                            <dd>открытое</dd>
                        </dl>
                    </li>

                </ul>
            </div>
        </div>

        <div class="b_product--footer">
            <div class="b_product--price">124 520 руб.</div>
            <div class="b_product--price __sale">124 520 руб.</div>
            <a class="b_product--link mod__underline" href="/product.php">Купить</a>
            <button class="b_product--button mod__underline">Получить скидку</button>
        </div>

    </div>

</div>