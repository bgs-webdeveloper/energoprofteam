

<div class="b_content--box mod__visible-lg">

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Проекты
        </div>

        <a class="link __arrow" href="#">Все проекты</a>

    </div>

    <div class="block__projects">

        <div class="block__slider" id="slider_projects">

            <div class="b_slider--wrapper">

                <div class="b_slider--overlay">

                    <div class="b_slider--layout __arrows">
                        <a class="b_slider--button __left"></a>
                        <a class="b_slider--button __right"></a>
                    </div>

                    <div class="b_slider--layout __frames">
                        <ul class="b_slider--controller">
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame __active">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                        </ul>
                    </div>

                </div>

                <ul class="b_slider--list" cost="3" >

                    <li class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/vtb.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">Поставка дизельного<br> генератора для банка ВТБ</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __bank"></i>
                                    Банковская сфера
                                </div>

                            </div>

                        </div>

                    </li>

                    <li class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/olymp.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">ДГУ в кожухе для олимпийского комплекса</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __olympic"></i>
                                    Олимпийский комплекс
                                </div>

                            </div>

                        </div>

                    </li>

                    <li class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/olymp.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">ДГУ в кожухе для олимпийского комплекса</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __olympic"></i>
                                    Олимпийский комплекс
                                </div>

                            </div>

                        </div>

                    </li>

                </ul>

            </div>


        </div>
    </div>

    <div class="b_content--line"></div>

</div>

