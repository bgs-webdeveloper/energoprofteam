

<div class="b_content--box">

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Награды
        </div>

        <a class="b_content--tab mod__visible-lg __inline" href="#">Сертификаты</a>

        <a class="link __arrow" href="#">Полный список</a>

    </div>

    <div class="block__document">

        <div class="block__slider" id="slider_docs_rewards">

            <div class="b_slider--wrapper __docs">

                <!-- <? inc_content("slider/overlay.php"); ?> -->

                <ul class="b_slider--list js-rewards-slider" cost="4" >

                    <li class="b_slider--item">

                         <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>
                        </div>

                    </li>

                    <li class="b_slider--item">

                         <a class="b_document--item __land">

                            <span class="b_document--layout">
                                <img class="b_document--img" src="/img/preview/document_land.jpg" alt="Награда">
                            </span>

                            <span class="b_document--name">Мастер продаж. Выдан <br>компании «ЭнергоПроф»</span>
                            <p class="b_document--text">2017</p>

                        </a>

                    </li>

                    <li class="b_slider--item">

                        <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>
                        </div>

                    </li>

                    <li class="b_slider--item">

                        <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>
                        </div>

                    </li>

                    <li class="b_slider--item">

                        <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>
                        </div>

                    </li>
                </ul>

            </div>


        </div>

    </div>

</div>

<div class="b_content--line"></div>


<div class="mod__hidden-lg">

    <div class="b_content--box">

        <div class="b_content--header">

            <div class="text-24_28 __header">
                Сертификаты
            </div>

            <a class="b_content--tab mod__visible-lg __inline" href="#">Награды</a>

            <a class="link __arrow" href="#">Полный список</a>

        </div>

        <div class="block__document">

            <div class="block__slider" id="slider_docs_certificates">

                <div class="b_slider--wrapper __docs">

                    <? /* inc_content("slider/overlay.php"); */ ?>

                    <ul class="b_slider--list js-rewards-slider" >

                        <li class="b_slider--item">

                            <div class="b_document--item">

                                <a class="b_document--layout" href="#">
                                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                                </a>

                                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                                <p class="b_document--text">2017</p>
                            </div>

                        </li>

                        <li class="b_slider--item">

                            <div class="b_document--item">

                                <a class="b_document--layout" href="#">
                                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                                </a>

                                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                                <p class="b_document--text">2017</p>
                            </div>

                        </li>

                        <li class="b_slider--item">

                            <div class="b_document--item">

                                <a class="b_document--layout" href="#">
                                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                                </a>

                                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                                <p class="b_document--text">2017</p>
                            </div>

                        </li>

                        <li class="b_slider--item">

                            <div class="b_document--item">

                                <a class="b_document--layout" href="#">
                                    <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                                </a>

                                <a class="b_document--name">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                                <p class="b_document--text">2017</p>
                            </div>

                        </li>

                    </ul>

                </div>


            </div>

        </div>

    </div>

    <div class="b_content--line"></div>

</div>