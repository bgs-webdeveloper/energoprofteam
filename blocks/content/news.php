

<div class="b_content--box">

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Новости
        </div>
        <a class="link __arrow" href="#">Все новости</a>

    </div>

    <div class="block__news">

        <ul class="b_news--wrapper">

            <li class="b_news--item">
                <div class="b_news--body">
                    <span class="b_news--date">28 января 2019</span>
                    <a class="b_news--title" href="#">Новинки каталога генераторов EuroPower</a>
                    <p class="b_news--text">Пополнение в модельном ряду бензиновых генераторов Europower. Ознакомиться с ассортиментом и характеристиками новинок </p>
                </div>
                <a class="link __arrow" href="#">Подробнее</a>
            </li>

            <li class="b_news--item">
                <div class="b_news--body">
                    <span class="b_news--date">14 января 2019</span>
                    <a class="b_news--title" href="#">Акционные предложения на генераторы Fubag</a>
                    <p class="b_news--text">Только до 31 января вы можете приобрести генератор с АВР по цене генератора без АВР. Спешите - количество моделей ограничено!</p>
                </div>
                <a class="link __arrow" href="#">Подробнее</a>
            </li>

            <li class="b_news--item">
                <div class="b_news--body">
                    <span class="b_news--date">14 января 2019</span>
                    <a class="b_news--title" href="#">ЭнергоПроф — лидер продаж дизельных генераторов Yanmar по итогам 2018 года!</a>
                    <p class="b_news--text">По результатам 2018 года компания ГК ЭнергоПроф стала лидером продаж генераторов Yanmar.</p>
                </div>
                <a class="link __arrow" href="#">Подробнее</a>
            </li>

        </ul>

    </div>

</div>

<div class="b_content--line"></div>
