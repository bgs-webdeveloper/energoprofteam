<div class="b_slider--overlay">

    <div class="b_slider--layout __arrows">
        <a class="b_slider--button __left"></a>
        <a class="b_slider--button __right"></a>
    </div>

    <div class="b_slider--layout __frames">
        <ul class="b_slider--controller">
            <li class="b_slider--frame">
                <a class="b_slider--dot" href="#"></a>
            </li>
            <li class="b_slider--frame __active">
                <a class="b_slider--dot" href="#"></a>
            </li>
            <li class="b_slider--frame">
                <a class="b_slider--dot" href="#"></a>
            </li>
            <li class="b_slider--frame">
                <a class="b_slider--dot" href="#"></a>
            </li>
            <li class="b_slider--frame">
                <a class="b_slider--dot" href="#"></a>
            </li>
        </ul>
    </div>

</div>