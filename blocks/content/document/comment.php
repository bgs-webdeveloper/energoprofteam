<div class="block__document">

    <div class="b_document--wrapper">

        <div class="b_document--head">
            <div class="b_document--title">Отзывы</div>
            <a class="link __arrow" href="#">Все отзывы</a>
        </div>

        <div class="block__slider" id="slider_docs_rewards">

            <div class="b_slider--wrapper __aside">

                <div class="b_slider--overlay">

                    <div class="b_slider--layout __arrows">
                        <a class="b_slider--button __left"></a>
                        <a class="b_slider--button __right"></a>
                    </div>

                    <div class="b_slider--layout __frames">
                        <ul class="b_slider--controller">
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame __active">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                            <li class="b_slider--frame">
                                <a class="b_slider--dot" href="#"></a>
                            </li>
                        </ul>
                    </div>

                </div>

                <ul class="b_slider--list">

                    <li class="b_slider--item">

                        <div class="b_document--item">

                            <a class="b_document--layout" href="#">
                                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Награда">
                            </a>

                            <a class="b_document--name" href="#">Благодарность <br>от МГУ им. М.В. <br>Ломоносова</a>
                            <p class="b_document--text">2017</p>

                        </div>
                    </li>

                </ul>

            </div>


        </div>

    </div>
</div>