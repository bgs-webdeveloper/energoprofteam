<div class="block__document">

    <div class="b_document--wrapper">

        <div class="b_document--head">
            <div class="b_document--title">Буклет о компании</div>
        </div>

        <div class="b_document--item">

            <a class="b_document--layout" href="#">
                <img class="b_document--img" src="/img/preview/preview-booklet.jpg" alt="Буклет">
            </a>

            <a class="b_document--name" href="#">Скачать буклет <br>размером <br>6.3 МБ</a>

        </div>

    </div>

</div>