<div class="b_content--box __dynamic">

    <div class="block__question">

        <div class="b_question--wrapper">

            <div class="b_question--head">

                <img class="b_question--icon" alt="" src="/img/svg/icon-question.svg">

                <div class="b_question--title">Есть вопросы?</div>

                <p class="b_question--text">Оставьте свой номер телефона и наш специалист перезвонит Вам в течение 15 минут.</p>

            </div>

            <form class="b_question--body" action="/" method="post">

                <label class="b_question--label">
                    <input class="input" type="text" name="name" placeholder="Ваше имя">
                </label>

                <label class="b_question--label">
                    <input class="input" type="text" name="phone" placeholder="+7 (___) ___-__-__">
                </label>

                <label class="b_question--label">
                    <textarea class="textarea"  name="comment" placeholder="Комментарий"></textarea>
                </label>

                <button class="button __bold" type="submit">Отправить</button>

            </form>

        </div>

    </div>

</div>