<div class="b_content--box" >

    <div class="b_content--header">

        <div class="text-24_28 __header js-switch-header__put">
            Акции и распродажа
        </div>

        <a class="b_content--tab mod__visible-lg __inline js-switch-header__get" href="#">Лидеры продаж</a>

        <a class="link __arrow" href="#">Все предложения</a>

    </div>

    <div class="block__slider __active" id="slider_product_sales1">

        <div class="b_slider--wrapper">
            <div class="b_slider--list js-slider-product-sales1">

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="#">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>

                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="/product.php">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>
                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="#">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>


                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="#">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>

                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

    <div class="block__slider __active __hidden" id="slider_product_sales2">

        <div class="b_slider--wrapper">
            <div class="b_slider--list js-slider-product-sales2">

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="/product.php">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Генератор Geko 130014 ED-S/DEDA</a>
                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="/product.php">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Генератор Geko 130014 ED-S/DEDA</a>
                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="#">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>


                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

                <div class="b_slider--item">

                    <div class="block__product">

                        <div class="b_product--item">

                            <div class="b_product--head">

                                <div class="b_projects--preview">
                                    <span class="sticker">Распродажа</span>
                                    <a class="b_product--link __clear" href="#">
                                        <img class="b_product--img" alt="Товар" src="/img/products/generator.png">
                                    </a>
                                </div>

                                <a class="b_product--title">Дизельный генератор Geko 130014 ED-S/DEDA</a>

                            </div>

                            <div class="b_product--body">

                                <div class="block__params">

                                    <ul class="b_params--wrapper">

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Топливо</span>
                                                </dt>
                                                <dd>дизель</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Мощность</span>
                                                </dt>
                                                <dd>2 кВт</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Напряжение</span>
                                                </dt>
                                                <dd>230 В</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Наличие АВР</span>
                                                </dt>
                                                <dd>нет</dd>
                                            </dl>
                                        </li>

                                        <li class="b_params--item">
                                            <dl class="b_params--overlay">
                                                <dt class="b_params--layout">
                                                    <span>Исполнение</span>
                                                </dt>
                                                <dd>открытое</dd>
                                            </dl>
                                        </li>

                                    </ul>
                                </div>
                            </div>

                            <div class="b_product--footer">
                                <div class="b_product--price">124 520 руб.</div>
                                <div class="b_product--price __sale">124 520 руб.</div>
                                <a class="b_product--link mod__underline" href="/product.php">Купить</a>
                                <a class="b_product--button mod__underline" href="#">Получить скидку</a>
                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </div>

    </div>

</div>

<div class="b_content--line"></div>