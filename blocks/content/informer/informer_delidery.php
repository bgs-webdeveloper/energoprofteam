<div class="block__informer">

    <div class="b_informer--wrapper">

        <div class="b_informer--overlay __top">

            <div class="b_informer--title">Оперативная доставка </div>

            <p class="b_informer--text"> По Москве <br>и регионам <br>России </p>

            <img class="b_informer--icon" src="" alt="Иконка" />

        </div>

        <div class="b_informer--overlay __down">

            <span class="b_informer--layout __opacity"></span>

            <span class="b_informer--img" style="background-image: url('/img/informer/background_info_delivery.png')"></span>

        </div>

    </div>

</div>