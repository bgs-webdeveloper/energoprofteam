<div class="block__selection">

    <div class="b_selection--wrapper __catalog">

        <div class="b_selection--item __trio">

            <div class="b_selection--layout">

                <div class="b_selection--head" data-js="block.selection.toggle">

                    <div class="b_selection--header">

                        <div class="text-16_24 __header">
                            Быстрый подбор по типу
                        </div>
                        <i class="b_selection--ui mod__hidden-lg __inline"></i>

                    </div>

                </div>

                <div class="b_selection--body">

                    <div class="b_selection--column">
                        <a class="b_selection--link" href="#">Автономные генераторы</a>
                        <a class="b_selection--link" href="#">Синхронные генераторы</a>
                        <a class="b_selection--link" href="#">Инверторные генераторы</a>
                        <a class="b_selection--link" href="#">Промышленные генераторы</a>
                        <a class="b_selection--link" href="#">Резервные генераторы</a>
                        <a class="b_selection--link" href="#">Бесшумные генераторы</a>
                    </div>
                    <div class="b_selection--column">
                        <a class="b_selection--link" href="#">Однофазные генераторы</a>
                        <a class="b_selection--link" href="#">Асинхронные генераторы</a>
                        <a class="b_selection--link" href="#">Генератор с электростартером</a>
                        <a class="b_selection--link" href="#">Передвижные генераторы</a>
                        <a class="b_selection--link" href="#">Генераторы с автозапуском</a>
                        <a class="b_selection--link" href="#">Бытовые генераторы для дачи</a>
                    </div>
                    <div class="b_selection--column">
                        <a class="b_selection--link" href="#">Трехфазные генераторы</a>
                        <a class="b_selection--link" href="#">Генераторы с воздушным охлаждением</a>
                        <a class="b_selection--link" href="#">Бытовые генераторы</a>
                        <a class="b_selection--link" href="#">Стационарные генераторы</a>
                        <a class="b_selection--link" href="#">Переносные генераторы</a>
                        <a class="b_selection--link" href="#">Бытовые генераторы для дома</a>
                    </div>

                </div>

            </div>

        </div>

        <div class="b_selection--item __single">

            <div class="b_selection--layout">

                <div class="b_selection--head" data-js="block.selection.toggle">

                    <div class="b_selection--header">

                        <div class="text-16_24 __header">
                            Быстрый подбор по стране
                        </div>
                        <i class="b_selection--ui mod__hidden-lg __inline"></i>

                    </div>

                </div>

                <div class="b_selection--body">

                    <div class="b_selection--column">
                        <a class="b_selection--link" href="#">Контейнерные генераторы</a>
                        <a class="b_selection--link" href="#">Генераторы в кожухе</a>
                        <a class="b_selection--link" href="#">Открытые генераторы</a>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>