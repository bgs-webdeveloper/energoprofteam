

<div class="b_content--box" >

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Проекты
        </div>

        <a class="link __arrow" href="#">Все проекты</a>

    </div>

    <div class="block__projects">

        <div class="block__slider" id="slider_projects">

            <div class="b_slider--wrapper">

                <? /* inc_content("slider/overlay.php"); */ ?>

                <div class="b_slider--list js-slider-projects">

                    <div class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/vtb.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">Поставка дизельного<br> генератора для банка ВТБ</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __bank"></i>
                                    Банковская сфера
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/olymp.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">ДГУ в кожухе для олимпийского комплекса</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __olympic"></i>
                                    Олимпийский комплекс
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/olymp.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">ДГУ в кожухе для олимпийского комплекса</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __olympic"></i>
                                    Олимпийский комплекс
                                </div>

                            </div>

                        </div>

                    </div>

                    <div class="b_slider--item">

                        <div class="b_projects--item">

                            <a class="b_projects--img" href="#" style="background-image: url('/img/projects/olymp.jpg');"></a>

                            <div class="b_projects--overlay">

                                <a class="b_projects--title" href="#">ДГУ в кожухе для олимпийского комплекса</a>

                                <p class="b_projects--text">Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.</p>

                                <div class="b_projects--category">
                                    <i class="b_projects--icon __olympic"></i>
                                    Олимпийский комплекс
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>


        </div>
    </div>



</div>

<div class="b_content--line"></div>
