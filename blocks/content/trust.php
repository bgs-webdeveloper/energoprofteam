

<div class="b_content--box">

    <div class="b_content--header">

        <div class="text-24_28 __header">
            Нам доверяют
        </div>

        <a class="link __arrow" href="#">Все клиенты</a>

    </div>


    <div class="block__trust">

        <div class="block__slider __corp" id="slider_trust">

            <div class="b_slider--wrapper">

                <? /* inc_content("slider/overlay.php"); */ ?>

                <div class="b_slider--list js-slider-trust">

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/vtb.png')"></div>
                            <a class="b_trust--link" href="#">Банк ВТБ</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/mvd.png')"></div>
                            <a class="b_trust--link" href="#">МВД России</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/sochi.png')"></div>
                            <a class="b_trust--link" href="#">Ок в Сочи</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/lukoil.png')"></div>
                            <a class="b_trust--link" href="#">Лукойл</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/gazprom.png')"></div>
                            <a class="b_trust--link" href="#">Газпром Тула</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/vtb.png')"></div>
                            <a class="b_trust--link" href="#">Банк ВТБ</a>
                        </div>
                    </div>

                    <div class="b_slider--item">
                        <div class="b_trust--item">
                            <div class="b_trust--img" style="background-image: url('/img/clients/vtb.png')"></div>
                            <a class="b_trust--link" href="#">Банк ВТБ</a>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

<div class="b_content--line"></div>
