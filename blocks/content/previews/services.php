<div class="b_content--box" >

    <div class="block__preview">

        <div class="b_content--header __large">
            <b>Полный спектр</b> инженерных услуг
            <a class="link __arrow" href="#">Все услуги</a>
        </div>

        <div class="b_preview--wrapper">

            <div class="mod__grid __a5b4">

                <div class="m_grid--column">
                    <a class="b_preview--link __project" href="#" style="background-image: url('/img/services/progect-energo-system.png');">
                        <div class="b_preview--name">Проектирование систем <br>энергоснабжения</div>
                    </a>
                </div>

                <div class="m_grid--column">
                    <a class="b_preview--link" href="#" style="background-image: url('/img/services/buy-useles-generators.png');">
                        <div class="b_preview--name">Выкуп генераторов <br>с наработкой</div>
                    </a>
                </div>

            </div>

            <div class="mod__grid __a5b4">

                <div class="m_grid--column">
                    <a class="b_preview--link __setups" href="#" style="background-image: url('/img/services/startsetup-work-setup-generator.png');">
                        <div class="b_preview--name">Пусконаладочные работы и монтаж генераторов</div>
                    </a>
                </div>

                <div class="m_grid--column">
                    <a class="b_preview--link" href="#" style="background-image: url('/img/services/tech-services.png');">
                        <div class="b_preview--name">Техническое <br>обслуживание <br>электростанций</div>
                    </a>
                </div>

            </div>

        </div>

    </div>

    <? inc_content("previews/services-superiority.php"); ?>

</div>