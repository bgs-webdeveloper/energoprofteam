<div class="block__superiority">

    <div class="b_content--header __small js_superiority--toggle">
        <!-- Для раскрытия списка преимуществ click:js_superiority--toggle -->
        Преимущества нашего производства
    </div>

    <div class="b_superiority--wrapper __symbol">

        <div class="mod__grid __trio">

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">1</i>
                    <div class="b_superiority--title">Жесткий контроль качества</div>
                    <p class="b_superiority--text">
                        Собственный отдел качества обеспечивает стабильно высокий уровень выпускаемой продукции.
                    </p>
                </div>
            </div>

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">2</i>
                    <div class="b_superiority--title">Вся продукция сертифицирована</div>
                    <p class="b_superiority--text">
                        В наличии сертификаты соответствия, пожарные сертификаты и сертификаты CPO.
                    </p>
                </div>
            </div>

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">3</i>
                    <div class="b_superiority--title">Индивидуальные проекты</div>
                    <p class="b_superiority--text">
                        Богатый опыт работы по нестандартным задачам, созданию опытных образцов с переводом в серийное производство.
                    </p>
                </div>
            </div>

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">4</i>
                    <div class="b_superiority--title">Крупные производственные мощности</div>
                    <p class="b_superiority--text">
                        Предоставляем возможность посетить наши площадки, ознакомиться с примерами продукции и производственным процессом.
                    </p>
                </div>
            </div>

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">5</i>
                    <div class="b_superiority--title">Четкое соблюдение сроков</div>
                    <p class="b_superiority--text">
                        Ответственное отношение к согласованным срокам - важнейший принцип нашей компании.
                    </p>
                </div>
            </div>

            <div class="m_grid--column">
                <div class="b_superiority--item">
                    <i class="b_superiority--symbol">6</i>
                    <div class="b_superiority--title">Серийное производство</div>
                    <p class="b_superiority--text">
                        Ежегодно выпускаем не более 1000 единиц типовой продукции.
                    </p>
                </div>
            </div>

        </div>

    </div>

</div>

<div class="b_content--line"></div>

