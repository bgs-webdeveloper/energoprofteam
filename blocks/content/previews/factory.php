<div class="b_content--box mod__visible-lg">

    <div class="block__preview">

        <div class="b_content--header __large">
            <b>Сертифицированное</b> производство
        </div>

        <div class="b_preview--wrapper">

            <div class="mod__grid __a5b4">

                <div class="m_grid--column">
                    <a class="b_preview--link __large" href="#" style="background-image: url('/img/factory/kontainers.png')">
                        <div class="b_preview--name">
                            Блок-контейнеры
                        </div>
                    </a>
                </div>

                <div class="m_grid--column">
                    <a class="b_preview--link" href="#" style="background-image: url('/img/factory/avr.png')">
                        <div class="b_preview--name">
                            Блоки АВР
                            <sub>(автоматы ввода резерва)</sub>
                        </div>
                    </a>
                    <a class="b_preview--link" href="#" style="background-image: url('/img/factory/elektro-shitovoe.png')">
                        <div class="b_preview--name">
                            Электрощитовое
                            <br>оборудование
                        </div>
                    </a>
                </div>

            </div>

        </div>

    </div>

    <? inc_content("previews/factory-superiority.php"); ?>

</div>
