
<div class="block__superiority">

    <div class="b_superiority--wrapper">

        <div class="b_content--header __small js_superiority--toggle">
            <!-- Для раскрытия списка преимуществ click:js_superiority--toggle -->

            Преимущества наших услуг
            <i class="b_superiority--ui"></i>
        </div>

        <div class="b_superiority--layout __icons">

            <div class="mod__grid __dual">

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-pro.svg')" ></span>
                        <div class="b_superiority--title">
                            Профессионализм сотрудников
                        </div>
                        <p class="b_superiority--text">
                            Каждый из 30 инженеров обладает опытом в отрасли более 10 лет, необходимыми сертификатами и допусками, проходит обязательную ежегодную переаттестацию.
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-map.svg')" ></span>
                        <div class="b_superiority--title">
                            Работаем по всей России
                        </div>
                        <p class="b_superiority--text">
                            Гарантированная техническая поддержка в любом регионе от Калининграда на западе до Южно-Сахалинска на востоке, от Астаны на юге до северного Тикси.
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-doc.svg')" ></span>
                        <div class="b_superiority--title">
                            Гарантия на все виды работ <br>
                            от 12 месяцев
                        </div>
                        <p class="b_superiority--text">
                            Решение задач с наилучшим соотношением цена/качество.
                        </p>
                    </div>

                </div>

                <div class="m_grid--column">
                    <div class="b_superiority--item">
                        <span class="b_superiority--icons" style="background-image: url('/img/services/icon-any-work.svg')" ></span>
                        <div class="b_superiority--title">
                            Любая сложность проектов
                        </div>
                        <p class="b_superiority--text">
                            На сегодняшний день наш опыт позволяет эффективно решать задачи клиента любой степени сложности с полной ответственностью за конечный результат.
                        </p>
                    </div>

                </div>


            </div>

        </div>

    </div>
</div>

<div class="b_content--line"></div>
