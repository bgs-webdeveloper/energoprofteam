<div class="block__menu" id="menu">

    <div class="b_menu--wrapper">

        <div class="b_menu--layout mod__hidden-lg">
            <a class="link __white __back" data-js="block.mobile.toggle" href="#">Свернуть меню</a>
        </div>

        <div class="b_menu--layout mod__hidden-lg">
            <div class="b_menu--title js_menu--toggle">
                О компании
            </div>

            <ul class="b_menu--list">

                <li class="b_menu--item">

                    <a class="b_menu--link" href="/catalog.php">Подпункт 1</a>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Подпункт 2</a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Подпункт 3</a>
                </li>

            </ul>

        </div>

        <div class="b_menu--layout">

            <div class="b_menu--title js_menu--toggle">Каталог электростанций</div>

            <ul class="b_menu--list">

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Дизельные генераторы</a>

                    <? inc_block("block__manufacturers.php");?>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Газовые генераторы</a>

                    <? inc_block("block__manufacturers.php");?>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Бензиновые генераторы</a>

                    <? inc_block("block__manufacturers.php");?>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Сварочные генераторы</a>

                    <? inc_block("block__manufacturers.php");?>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">БУ генераторы с гарантией</a>
                </li>

                <li class="b_menu--item __glow">
                    <a class="b_menu--link" href="/catalog.php">Акции и распродажи</a>

                    <? inc_block("block__manufacturers.php");?>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Лидеры продаж</a>
                </li>
            </ul>

        </div>

        <div class="b_menu--layout">

            <div class="b_menu--title js_menu--toggle">Услуги</div>

            <ul class="b_menu--list">

                <li class="b_menu--item">

                    <a class="b_menu--link" href="/catalog.php">Предпроектное обследование</a>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">Проектирование систем электроснабжения</a>

                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Автоматизация станций
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Монтаж и пусконаладка
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Шеф-монтаж
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Техническое обслуживание
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Выкуп БУ генераторов
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Синхронизация генераторов
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Ремонт
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Аренда генераторов
                    </a>
                </li>

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Доставка и оплата
                    </a>
                </li>

            </ul>

        </div>

        <div class="b_menu--layout">

            <div class="b_menu--title js_menu--toggle">Оборудование</div>

            <ul class="b_menu--list">

                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Контейнеры
                    </a>

                    <? inc_block("block__manufacturers.php");?>

                </li>
                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Блоки автоматического ввода резерва (АВР)
                    </a>
                </li>
                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Электрощитовое оборудование
                    </a>
                </li>
                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Источники бесперебойного питания (ИБП)
                    </a>

                    <? inc_block("block__manufacturers.php");?>

                </li>
                <li class="b_menu--item">
                    <a class="b_menu--link" href="/catalog.php">
                        Стабилизаторы напряжения
                    </a>
                </li>

            </ul>

        </div>

        <div class="b_menu--layout mod__hidden-lg">
            <a class="b_menu--link">
                Проекты
            </a>
        </div>


        <div class="b_menu--layout mod__hidden-lg">
            <a class="b_menu--link">
                Информация
            </a>
        </div>

        <div class="b_menu--layout mod__hidden-lg">
            <a class="b_menu--link">
                Дилерам
            </a>
        </div>

        <div class="b_menu--layout mod__hidden-lg">
            <a class="b_menu--link">
                Контакты
            </a>
        </div>

    </div>

</div>