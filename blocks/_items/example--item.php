<?
$example_items = [];
$example_items[] = ['IST', 'ООО «Инновационные строительные технологии»'];
$example_items[] = ['magna', 'ЗАО «Магна Технопласт»'];
$example_items[] = ['veduchi', 'Горнолыжный курорт «Ведучи»'];

$example_items = $example_items[ rand(0, (count($example_items) - 1 )) ];
?>

<div class="block__example">

    <div class="b_example--wrapper">

        <div class="b_example--body">

            <a class="b_example--title" href="/_project.php" >
                <img class="b_example--logo" src="/img/projects/clients/<?=$example_items[0]?>.png" alt="Логотип клиента">
                <?=$example_items[1]?>
            </a>

            <div class="b_content--line __small"></div>

            <div class="b_example--product">Электростанции: ТСС АД-260С-Т400-1РМ5 с АВР — 4 шт, ТСС АД-160С-Т400-1РМ13 с АВР — 2 шт. Контейнер для генератора ПБК-6 — 6 шт, бак на 1000л — 6 шт.</div>

            <div class="b_example--text">
                <b>Регион поставки:</b> г. Усинск, республика Коми, г. Бавлы, республика Татарстан
            </div>

            <div class="b_example--text">
                <b>Дата завершения проекта:</b>  декабрь 2015 г.
            </div>

            <p class="b_example--text __m2 mod__hidden-sm">
                Осуществлена поставка 6 электростанций ТСС с АВР общей мощностью 1360 кВт для основного энергоснабжения строительных работ ООО Инновационные строительные технологии. АВР собственного производства "ЭнергоПроф".
            </p>

        </div>

        <div class="b_example--head">

            <div class="b_example--preview">
                <img class="b_example--pic" src="/img/projects/product.jpg" alt="Привью проекта" />
            </div>

            <div class="b_example--data">

                <a class="b_example--param __power">
                    1360 кВт
                    <? inc_block('block__hint.php'); ?>
                </a>

                <div class="b_example--param __category">Олимпийский комплекс</div>

                <a class="button" href="/_project.php">Подробнее</a>

            </div>

        </div>

    </div>

</div>