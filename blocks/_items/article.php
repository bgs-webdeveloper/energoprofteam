<article class="b_chronicle--item">

    <a class="b_chronicle--head" href="/article.php">
        <img class="b_chronicle--img" src="/img/chronicle/gen2.png" alt="Привью новости" />
    </a>

    <div class="b_chronicle--body">

        <span class="b_chronicle--data">
            11 февраля 2019
        </span>

        <a class="b_chronicle--title" href="/article.php">
            Российский бренд «ДИЗЕЛЬ» — новинка в каталоге ЭнергоПроф!
        </a>

        <p class="b_chronicle--text">
            В каталоге ЭнергоПроф представлены новые модели дизельных генераторов
            турецкого бренда Aksa мощностъю от 48 до 2200 кВт.
        </p>

    </div>

</article>