<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Просмотр Проекта</title>

        <? inc_block("head.php")?>

    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("services/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <div class="block__public">

                                <div class="b_content--line __blue mod__visible-sm"></div>

                                <h1 class="b_content--header">
                                    <span class="text-30_50 __header">
                                        Станции ТСС для ООО Инновационные строительные технологии
                                    </span>
                                </h1>

                                <div class="block__example">

                                    <a class="b_example--param __power">
                                        1360 кВт
                                    </a>

                                    <div class="b_example--param __category">Олимпийский комплекс</div>

                                    <a class="b_example--title" href="/_project.php" >
                                        <img class="b_example--logo" src="/img/projects/clients/IST.png" alt="Логотип клиента">
                                        ООО «Инновационные строительные технологии»
                                    </a>

                                </div>

                                <div class="b_public--wrapper">



                                    <div class="b_public--head">

                                        <div class="b_public--preview">
                                            <img class="b_public--pic" src="/img/projects/product.jpg" alt="привью Новости" />
                                        </div>

                                        <div class="b_public--img"></div>
                                        <div class="b_public--img"></div>
                                        <div class="b_public--img"></div>

                                    </div>

                                    <div class="b_public--body">

                                        <p class="b_public--text __blue">
                                            Электростанции:   с АВР — 4 шт, ТСС АД-160С-Т400-1РМ13 с АВР — 2 шт. Контейнер для генератора ПБК-6 — 6 шт, бак на 1000л — 6 шт.
                                        </p>

                                        <p class="b_public--text">
                                            <b>Регион поставки:</b> г. Усинск, республика Коми, г. Бавлы, республика Татарстан
                                            <br><b>Дата завершения проекта:</b> декабрь 2015 г.
                                        </p>

                                        <p class="b_public--text">
                                            <b>Профиль организации:</b>
                                            ООО Инновационные строительные технологии - компания, проводящая строительство крупных городских объектов в городе Киров.
                                        </p>

                                        <p class="b_public--text">
                                            <b>Характеристики электростанции ТСС АД-260С-Т400-1РМ5:</b>
                                            <br>Номинальная мощность станции: 260 кВт
                                            <br>Максимальная мощность станции: 286 кВт
                                            <br>Двигатель: TSS
                                            <br>Исполнение: открытое
                                            <br>Напряжение: 230/400 В
                                            <br>Страна происхождения: Россия-Китай
                                            <br>Гарантия производителя: 3 года
                                        </p>

                                        <p class="b_public--text">
                                            <b>Характеристики электростанции ТСС АД-260С-Т400-1РМ5:</b>
                                            <br>Номинальная мощность станции: 260 кВт
                                            <br>Максимальная мощность станции: 286 кВт
                                            <br>Двигатель: TSS
                                            <br>Исполнение: открытое
                                            <br>Напряжение: 230/400 В
                                            <br>Страна происхождения: Россия-Китай
                                            <br>Гарантия производителя: 3 года
                                        </p>

                                    </div>

                                </div>

                            </div>

                            <div class="block__portfolio">

                                <div class="b_portfolio--wrapper">


                                    <div class="b_portfolio--body __blue">

                                        <div class="b_portfolio--header">
                                            Похожие проекты
                                        </div>

                                        <? inc_block('_items/example--item.php')?>
                                        <? inc_block('_items/example--item.php')?>
                                    </div>

                                </div>

                            </div>

                            <? inc_content("questions-show.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>