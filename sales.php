<?php include "core/app.php";

    include root() . "core/brands.php";
?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Акции</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog" >

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">


                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("sales/block__breadcrumb.php"); ?>

                        <div class="block__content" id="content">

                            <h1 class="b_content--header __title">
                                <span class="text-30_50 __header">
                                    Акции и распродажи
                                </span>
                            </h1>

                            <div class="block__brands">

                                <div class="b_brands--wrapper">

                                    <div class="b_brands--head">

                                        <div class="b_brands--label">
                                            Фильтр по брендам
                                        </div>

                                    </div>

                                    <div class="b_brands--body">

                                        <? foreach ( $brands as $index => $item ) : ?>
                                            <? $class = ( $index == 1 ) ? '__active' : ''; ?>

                                            <a class="b_brands--link <?=$class?>" href="#">
                                                <span class="b_brands--img" style="background-image: url('/img/brands/<?=$item?>.png');"></span>
                                            </a>

                                        <? endforeach; ?>

                                    </div>

                                </div>

                            </div>

                            <? inc_page("sales/block__showcase.php"); ?>

                            <div class="b_content--line __small"></div>

                            <? inc_page("sales/block__superiority--services.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>