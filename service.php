<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Просмотр услуги</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog" >

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_block("block__banner.php"); ?>

                        <? inc_page("services/block__breadcrumb.php"); ?>

                        <h1 class="b_content--header">
                            <span class="text-30_50 __header">
                                Пусконаладочные работы и монтаж генераторов
                            </span>
                        </h1>

                        <div class="block__content" id="content">

                            <div class="block__service">

                                <p class="b_service--text">
                                    Срок службы приобретенной вами электростанции и безопасность её функционирования напрямую зависят от того, насколько грамотно проведен монтаж а и выполнены пусконаладочные работы.
                                </p>

                                <? inc_page("service/pic.php"); ?>

                                <div class="b_service--title">
                                    При установке генераторов важно:
                                </div>

                                <div class="block__list">

                                    <ul class="b_list--wrapper __block">

                                        <li class="b_list--item">
                                            учесть все условия, в которых предстоит функционировать технике;
                                        </li>

                                        <li class="b_list--item">
                                            грамотно подготовить площадку для установки на основании технических требований
                                            <br> производителей и правил эксплуатации электрогенерирующего оборудования;
                                        </li>

                                        <li class="b_list--item">
                                            соблюсти все пожарные и санитарные нормы.
                                        </li>

                                    </ul>

                                </div>

                                <p class="b_service--text">
                                    Ошибки, совершенные при попытке самостоятельно установить генератор,
                                    могут привести к поломке дорогостоящего оборудования или нарушению техники безопасности.
                                    Так же могут быть не учтены важные моменты, обеспечивающие исправную работу генератора:
                                    учет и контроль уровня шума, удобство технического обслуживания и ремонта и многое другое.
                                </p>

                                <? inc_page("service/rules.php"); ?>


                                <p class="b_service--text">
                                    Доверив компании «ЭнергоПроф» пусконаладку и монтаж электростанции, вы исключите потенциальные ошибки
                                    и сбои и обеспечите долгую и бесперебойную работу оборудования.
                                    Опыт наших инженеров, наличие специальных инструментов, проведение предпроектного обследования позволят
                                    быстро и качественно создать источник автономного бесперебойного электроснабжения на вашем объекте.
                                    Генератор будет работать в строгом соответствии с требованиями производителей
                                    и с учетом особенностей вашего объекта.
                                </p>

                                <div class="b_service--title">
                                    При заказе услуги по монтажу и пусконаладке в «ГК ЭнергоПроф» объект сдается « под ключ» и предоставляется гарантийный сертификат на срок от 1 года.
                                </div>

                                <div class="block__list">

                                    <ul class="b_list--wrapper __block">
                                        <li class="b_list--item __head">Организация источника резервного электроснабжения «под ключ» включает:</li>
                                        <li class="b_list--item">предпроектное обследование объекта;</li>
                                        <li class="b_list--item">выбор электростанции;</li>
                                        <li class="b_list--item">подготовку площадки для установки оборудования (прокладка кабельных трасс, строительство фундамента);</li>
                                        <li class="b_list--item">доставку техники;</li>
                                        <li class="b_list--item">подключение;</li>
                                        <li class="b_list--item">обеспечение условий для обслуживания агрегата (заправка маслом и топливом);</li>
                                        <li class="b_list--item">тестирование и запуск агрегата.</li>
                                    </ul>

                                </div>

                                <? inc_page("service/board.php"); ?>

                                <? inc_page("service/table.php"); ?>

                                <? inc_page("service/board2.php"); ?>

                                <p class="b_service--text">
                                    Кроме пусконаладки и установки электростанций "ГК ЭнергоПроф" предлагает также услугу по
                                    <a href="">монтажу и подключению дизель генераторов</a>, используемых там, где отсутствует центральная система электроснабжения..
                                </p>

                            </div>

                            <? inc_page("service/superiority.php"); ?>

                            <? inc_content("questions-show.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">


                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>
    </body>

</html>