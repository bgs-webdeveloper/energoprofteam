<?php include "core/app.php"; ?>

<!DOCTYPE html>

<html lang="en">

    <head>
        <title>Просмотр Новости</title>

        <? inc_block("head.php")?>
    </head>

    <body class="page __catalog">

        <div class="resources--start"></div>

        <div class="block__overlay" id="overlay">

            <div class="b_overlay--page" id="page">

                <div class="b_overlay--layout __left">

                    <a class="b_overlay--link mod__visible-lg" href="/">
                        <img class="b_overlay--img" src="/img/logo.svg" alt="Logo" title="Логотип">
                    </a>

                    <? inc_block("block__menu.php"); ?>

                    <div class="b_overlay--layout __widgets mod__visible-lg">

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_page("catalog/aside/block__trust.php"); ?>

                        <? inc_page("catalog/aside/slider--rewards.php"); ?>

                        <? inc_content("document/booklet.php"); ?>

                        <? inc_block("block__informer.php"); ?>

                    </div>

                </div>

                <main class="b_overlay--layout __right" role="main">

                    <div class="b_overlay--body" id="body">

                        <? inc_block("block__header.php"); ?>

                        <? inc_page("article/block__breadcrumb.php"); ?>


                        <div class="block__content" id="content">

                            <div class="b_content--line __small mod__hidden-lg"></div>

                            <div class="block__public">

                                <h1 class="b_public--header">
                                    <span class="text-30_50 __header">
                                        Пополнение в ассортименте ДГУ Амперос
                                    </span>
                                    <span class="b_public--date">11 февраля 2019</span>
                                </h1>

                                <div class="b_public--wrapper">

                                    <div class="b_public--head">

                                        <div class="b_public--preview">
                                            <img class="b_public--pic" src="/img/articles/1.jpg" alt="привью Новости" />

                                        </div>

                                        <div class="b_public--img"></div>
                                        <div class="b_public--img"></div>
                                        <div class="b_public--img"></div>

                                    </div>

                                    <div class="b_public--body">

                                        <p class="b_public--text">Ассортимент продукции Амперос пополнился серией ДГУ профессионального класса для интенсивной эксплуатации. Мы предлагаем модели жидкостного охлаждения мощностью 20-160 кВт в 6 вариантах комплектации. Генераторы каждой мощности выпускаются с блоком АВР и без в открытом исполнении, кожухе и контейнере.</p>

                                        <div class="text-18_24 __header __m1">Особенности профессиональных ДГУ :</div>

                                        <div class="block__list">
                                            <ul class="b_list--wrapper">
                                                <li class="b_list--item">повышенный ресурс двигателя;</li>
                                                <li class="b_list--item">интенсивная эксплуатация (от 8 часов в сутки);</li>
                                                <li class="b_list--item">микропроцессорное управление;</li>
                                                <li class="b_list--item">механический впрыск топлива;</li>
                                                <li class="b_list--item">работа в основном и аварийном режиме с повышенной нагрузкой</li>
                                            </ul>
                                        </div>

                                        <p class="b_public--text">Модели оснащены четырехтактными дизельными двигателями IVECO со среднеоборотной скоростью. Впрыск топлива осуществляется ротационным насосом. Управление производится с фирменной панели, на которой размещены сигнальные приборы, выключатель с ключом, индикаторы, сигнализация. Выбранная рабочая скорость поддерживается автоматически. Электроэнергия вырабатывается необслуживаемым синхронным альтернатором с медными обмотками.</p>
                                        <p class="b_public--text">ДГУ дополнительно комплектуются системами предварительного нагрева, удаленного мониторинга. Гарантия производителя составляет 12 месяцев.</p>
                                        <p class="b_public--text">Для заказа ДГУ позвоните нам по номеру +7 (495) 181-12-72.</p>
                                    </div>

                                </div>

                            </div>

                            <div class="b_content--line"></div>

                            <? inc_page("article/block__chronicle.php"); ?>

                        </div>

                    </div>

                </main>

            </div>

            <div class="b_overlay--footer">

                <? inc_content("questions.php"); ?>

                <? inc_block("block__footer.php"); ?>

            </div>

            <span class="b_overlay--mask"></span>

        </div>

        <div class="resources--end">
            <? addHTML($js)?>
        </div>

    </body>

</html>